<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "home";
$route['default_controller'] = "admin/authentication";

$route['admin'] = 'admin/authentication';
$route['admin/store'] = 'admin/home';
$route['admin/store/create'] = 'admin/home/create';
$route['admin/store/update'] = 'admin/home/update';
$route['admin/store/action_update'] = 'admin/home/action_update';
$route['admin/store/store_delete'] = 'admin/home/store_delete';
$route['admin/store/store_add_address'] = 'admin/home/store_add_address';
$route['admin/store/city'] = 'admin/home/city';
$route['admin/admin-management'] = "admin/admin_management/index";
$route['admin/changepassword'] = "admin/home/changepassword";
$route['admin/store/view_store'] = 'admin/home/view_store';

/* Frotn-devlopment routing start*/
$route['404_override'] = '';


// API path setting
$route['api/docs'] = 'api/api';

/* End of file routes.php */
/* Location: ./application/config/routes.php */