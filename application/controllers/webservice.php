<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Webservice extends CI_Controller {
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('webservice_model', '', TRUE);
		$this->load->library('upload'); 
		$this->load->library('image_lib');

	}   

	public function index()
	{
		$action = $this->input->get('action');
		if($action == 'getUserLogin'){
			$this->getUserLogin();
		}
		elseif($action == 'setUserRegistration'){
			$this->setUserRegistration();
		}
		elseif($action == 'getAppHomeSlider'){
			$this->getAppHomeSlider();
		}
		elseif($action == 'getStyleCategories'){
			$this->getStyleCategories();
		}
		elseif($action == 'getStyleByCategoryId'){
			$this->getStyleByCategoryId();
		}
		elseif($action == 'getGlobalSize'){
			$this->getGlobalSize();
		}
		elseif($action == 'getMoneySpendOn'){
			$this->getMoneySpendOn();
		}
		elseif($action == 'getProductCategory'){
			$this->getProductCategory();
		}
		elseif($action == 'getProductBrand'){
			$this->getProductBrand();
		}
		elseif($action == 'getProductSize'){
			$this->getProductSize();
		}
		elseif($action == 'getProductColor'){
			$this->getProductColor();
		}
		elseif($action == 'getStyleLooking'){
			$this->getStyleLooking();
		}
		elseif($action == 'getStore'){
			$this->getStore();
		}

	}

	
	function getUserLogin(){
		$vEmail = $this->input->post('vEmail');
		$vPassword = md5($this->input->post('vPassword'));
		$data = $this->webservice_model->checklogin($vEmail,$vPassword);
		if($data)
		{
			$data['success'] = 'User login succesfully';
			
		}else{
			$data['success'] = 'Error';
		}
		
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function setUserRegistration()
	{
		/*if(!$this->input->post()){
		echo '<html>
			<body>
			<form action="" method="post"
			enctype="multipart/form-data">
			<table border="1" cellpadding="5">
			
				<tr>
					<td>Email</td><td><input type="text" name="vEmail" value="" /></td>
				</tr>
				<tr>
					<td>Password</td><td><input type="password" name="vPassword" value="" /></td>
				</tr>
				<tr>
					<td>Gender</td>
						<td><input type="radio" name="eType" value="male">Male<br>
						<input type="radio" name="eType" value="female">Female</td>
				</tr>
				
				<tr>
					<td>Picture</td><td><input type="file" name="vImage" value="" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit" value="Submit"></td>
				</tr>
			</table>
			</form>
			
			</body>
			</html>';exit;
		}*/
		$data['vEmail'] = $this->input->post('vEmail');
		$data['vPassword'] = md5($this->input->post('vPassword'));
		$data['eType'] = $this->input->post('eType');
		
		$chackEmail = $this->webservice_model->ChackEmail($data['vEmail']);
		
		if(!empty($chackEmail)){
			
			$dataArry = array(
				'success' => 'Email Id Already Exists!'
			);
		}else{
			$iUserId = $this->webservice_model->userRegister($data);
			$img_uploaded = $this->do_upload($iUserId);
			$Data['vImage'] = $img_uploaded;
			$id = $this->webservice_model->updateUserRegister($iUserId,$Data);
			if($iUserId)
			{
				$success = 'User Register succesfully';
			}else{
				$success = 'Error';
			}
			$dataArry = array(
				'iUserId' => $iUserId,
				'success' => $success
			);
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($dataArry);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function do_upload($iUserId)
	{
		
		if(!is_dir('uploads/user/')){
			@mkdir('uploads/user/', 0777);
		}
		if(!is_dir('uploads/user/'.$iUserId)){
			@mkdir('uploads/user/'.$iUserId, 0777);
		}
		$config = array(
			'allowed_types' => "gif|GIF|JPG|jpg|JPEG|jpeg|PNG|png|TIF|tif",
			'upload_path' => 'uploads/user/'.$iUserId,
			'file_name' => str_replace(' ','',$_FILES['vImage']['name']),
			'max_size'=>5380334
		);
		
		$this->upload->initialize($config);
		$this->upload->do_upload('vImage'); //do upload
		$image_data = $this->upload->data(); //get image data
		
                $config1 = array(
			'source_image' => $image_data['full_path'], //get original image
			'new_image' => 'uploads/user/'.$iUserId.'/210x158_'.$image_data['file_name'], //save as new image //need to create thumbs first
			'maintain_ratio' => false,
			'width' => 210,
			'height' => 158
		      );
		$this->image_lib->initialize($config1);
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	function getAppHomeSlider(){
		//$eSliderType = $this->input->get('eSliderType');
		$data = $this->webservice_model->getSliderData();
		for($i=0;$i<count($data);$i++){
			$data[$i]['vImage']= $this->config->item('base_upload').'slider/'.$data[$i]['iSliderId'].'/'.$data[$i]['vImage'];
		}
		//if($data)
		//{
		//	$data['message'] = 'success';
		//}else{
		//	$data['message'] = 'Error';
		//}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function getStyleCategories(){
		$data = $this->webservice_model->getStyleCategories();
		//if($data)
		//{
		//	$data['success'] = 'User login succesfully';
		//}else{
		//	$data['success'] = 'Error';
		//}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function getStyleByCategoryId(){
		$iCategoryId = $this->input->get('iCategoryId');
		$data = $this->webservice_model->getStyleByCategoryId($iCategoryId);
		for($i=0;$i<count($data);$i++){
			$data[$i]['vImage']= $this->config->item('base_upload').'personalization/'.$data[$i]['iCategoryId'].'/'.$data[$i]['iPersonalId'].'/'.$data[$i]['vImage'];
		}
		//if($data)
		//{
		//	$data['message'] = 'success';
		//}else{
		//	$data['message'] = 'Error';
		//}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function getGlobalSize(){
		$eSizeType = $this->input->get('eSizeType');
		$data = $this->webservice_model->getGlobalSize($eSizeType);
		
		//if($data)
		//{
		//	$data['message'] = 'success';
		//}else{
		//	$data['message'] = 'Error';
		//}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function getMoneySpendOn(){
		
		$data = $this->webservice_model->getSpenOn();
		
		//if($data)
		//{
		//	$data['message'] = 'success';
		//}else{
		//	$data['message'] = 'Error';
		//}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}

	function getProductCategory(){
		$data = $this->webservice_model->getProductCategory();
		// echo "<pre>";print_r($data);exit;
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}

	function getProductBrand(){
		$data = $this->webservice_model->getProductBrand();
		// echo "<pre>";print_r($data);exit;
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}

	function getProductSize(){
		$data = $this->webservice_model->getProductSize();
		// echo "<pre>";print_r($data);exit;
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}

	function getProductColor(){
		$data = $this->webservice_model->getProductColor();
		// echo "<pre>";print_r($data);exit;
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}

	function getStyleLooking(){
		$data = $this->webservice_model->getStyleLooking();
		// echo "<pre>";print_r($data);exit;
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}

	function getStore(){
		$data = $this->webservice_model->getStore();

		for($i=0;$i<count($data);$i++){
			$file_url = $this->config->item('base_upload_url').'store/'.$data[$i]['iStoreId'];
			$filePath = $this->config->item('base_upload').'store/'.$data[$i]['iStoreId'];
			$mainImagePath = $filePath.'/MainImage'.'/'.$data[$i]['vMainImage'];
			if(file_exists($mainImagePath)){
				$data[$i]['vMainImage']= $file_url.'/MainImage'.'/'.$data[$i]['vMainImage'];
			}else{
				$data[$i]['vMainImage'] = "";
			}

			$smallImagePath = $filePath.'/SmalImage'.'/'.$data[$i]['vSmallIamge'];
			if(file_exists($smallImagePath)){
				$data[$i]['vSmallIamge']= $file_url.'/SmalImage'.'/'.$data[$i]['vSmallIamge'];
			}else{
				$data[$i]['vSmallIamge'] = "";
			}

			$shopImagePath = $filePath.'/ShopImage'.'/'.$data[$i]['vShopImage'];
			if(file_exists($shopImagePath)){
				$data[$i]['vShopImage']= $file_url.'/ShopImage'.'/'.$data[$i]['vShopImage'];
			}else{
				$data[$i]['vShopImage'] = "";
			}

		}
		
		if($data)
		{
			$data['message'] = 'success';
		}else{
			$data['message'] = 'Error';
		}
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($data);
		ob_clean();
		echo  $main;
		exit;
	}
	
}

/* End of file webservice.php */
/* Location: ./application/controllers/webservice.php */