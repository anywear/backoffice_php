<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product_brand extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/product_brand_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'product_brand';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/product_brand/view-product-brand.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_product_brand_listing(){
		$product_brand = $this->product_brand_model->get_all_product_brand_listing();
		if(count($product_brand) > 0){ 
			foreach ($product_brand as $key => $value){
				$alldata[$key]['iBrandId'] = '<input type="checkbox" name="iId[]" iBrandId="iId" value="'.$value['iBrandId'].'">';
				$alldata[$key]['vName'] = $value['vName'];
				//$alldata[$key]['tSynonyms'] = $value['tSynonyms'];
				if($value['eDataFrom'] == 'shopstyle'){
                	$alldata[$key]['eDataFrom'] = 'Shop Sense';
                }else if($value['eDataFrom'] == 'manual'){
                	$alldata[$key]['eDataFrom'] = 'Manually';
                }
				// $alldata[$key]['eDataFrom'] = $value['eDataFrom'];
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'product_brand/update/'.$value['iBrandId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'product_brand/product_brand_delete?iBrandId='.$value['iBrandId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'product_brand';
		$eDataFrom = field_enums('brands','eDataFrom');
		if($this->input->post()){
			$data = $this->input->post('data');
			
			$iBrandId = $this->product_brand_model->add_product_brand($data);
			if($iBrandId){
				$this->session->set_flashdata('message',"Product Categories added successfully");
				redirect($this->data['admin_url'].'product_brand');
			}else{
				$this->session->set_flashdata('message',"Product Categories successfully");
				redirect($this->data['admin_url'].'product_brand');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Product Categories', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/product_brand/create-product-brand.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eDataFrom', $eDataFrom);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'product_brand';
		$iBrandId = $this->uri->segment(4);
		$this->data['product_brand'] = $this->product_brand_model ->get_product_brand_details($iBrandId);
		$eDataFrom = field_enums('brands','eDataFrom');
		if($this->input->post()){
			$data = $this->input->post('data');
			$iBrandId = $this->product_brand_model->update($data['iBrandId'],$data);
			if($data['iBrandId']){
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'product_brand');
			}else{
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'product_brand');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/product_brand/edit-product-brand.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eDataFrom', $eDataFrom);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$iBrandIds = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='brands';
		$tableData['update_field']='iBrandId';
		if($action=='Delete'){
			$count=count($iBrandIds);
			foreach ($iBrandIds as $row){		            	
				$data= $this->product_brand_model->delete_brand_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_brand'); 
		}else if($action=='Active'){
			$count=count($iBrandIds);
			foreach ($iBrandIds as $row){		            	
				$data= $this->product_brand_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_brand'); 
		}else if($action=='Inactive'){
			$count=count($iBrandIds);
			foreach ($iBrandIds as $row){		            	
				$data= $this->product_brand_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_brand'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'product_brand'); 
		}
	}

	function product_brand_delete(){
		$iBrandIds = $this->input->get('iBrandId');
		$data= $this->product_brand_model->delete_record($iBrandIds);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'product_brand'); 
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
