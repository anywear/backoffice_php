<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product_color extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('admin/product_color_model', '', TRUE);
		$this->load->library('image_lib'); //load library
		$this->load->helper('url');
		$this->load->library('upload');
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){
			redirect($this->data['admin_url'].'authentication');
			exit ; 
		}		
		$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'product_color';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/product_color/view-product-color.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_product_color_listing(){
		$all_store=$this->product_color_model->get_all_color();
		for($i=0;$i<count($all_store);$i++){
		$record=$this->product_color_model->get_peoduct_color_details($all_store[$i]['iProductColorId']);
		$colorname=$this->product_color_model->get_peoduct_color_details($record['iParentId']);
            
		$child=$this->recursion($colorname['iParentId']);
		if($record['iParentId'] == 0){
		    $all_store[$i]['path']=$record['vColorName'];    
		}
		else{
		    if(count($child) > 1){
			$all_store[$i]['path']=$child['vColorName']." >> ".$colorname['vColorName']." >> ".$record['vColorName'];       
		    }
		    else{
			$all_store[$i]['path']=$colorname['vColorName']." >> ".$record['vColorName'];
		    }
		}
        }
        // echo "<pre>";print_r($all_store);exit;
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iProductColorId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iProductColorId'].'">';
				$alldata[$key]['iParentId'] = $value['path'];
				$alldata[$key]['vColorName'] = $value['vColorName'];
				$alldata[$key]['eStatus'] = $value['eStatus'];
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'product_color/update/'.$value['iProductColorId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'product_color/product_delete?iProductColorId='.$value['iProductColorId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'product_color';
		$ParentID = $this->product_color_model->get_parent_color_id('0','','0','1','0');
		if($this->input->post()){
			$color = $this->input->post('color');
			if($color['iParentId']==''){
				$color['iParentId']=0;
			}
			$iProductColorId = $this->product_color_model->add_color($color);
			if($iProductColorId){
				$this->session->set_flashdata('message',"color added successfully");
				redirect($this->data['admin_url'].'product_color');
			}else{
				$this->session->set_flashdata('message',"color added successfully");
				redirect($this->data['admin_url'].'product_color');
			}
			exit;
		}
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add color', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/product_color/create-product-color.tpl";   
		$this->smarty->assign('iParentId',$ParentID);
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'product_color';
		$iProductColorId = $this->uri->segment(4);
		$this->data['color'] = $this->product_color_model ->get_color_details($iProductColorId);
		$ParentID = $this->product_color_model->get_parent_color_id('0','','0','1','0');
		$eStatus = field_enums('product_colors','eStatus');
		
		if($this->input->post()){
			$data = $this->input->post('color');
			$data['iProductColorId'] = $this->input->post('iProductColorId');
			$id = $this->product_color_model->update($data['iProductColorId'],$data);
			if($data['iProductColorId']){
				$this->session->set_flashdata('message',"color added successfully");
				redirect($this->data['admin_url'] . 'product_color');
			}else{
				$this->session->set_flashdata('message',"color added successfully");
				redirect($this->data['admin_url'] . 'product_color');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/product_color/edit-product-color.tpl";
		$this->smarty->assign('iParentId',$ParentID);
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='product_colors';
		$tableData['update_field']='iProductColorId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_color_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_color'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_color_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_color'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_color_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_color'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'product_color'); 
		}
	}

	function product_delete(){
		$ids = $this->input->get('iProductColorId');
		$data= $this->product_color_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'product_color'); 
	}

	function recursion($id){
        $colorname=$this->product_color_model->getchild($id);
        if(count($colorname) > 1){
            $colorname=$this->product_color_model->getchild($id);
        }
        return $colorname;
    }
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


