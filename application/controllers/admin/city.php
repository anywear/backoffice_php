<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class city extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/city_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/city/view-city.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_city_listing(){
		$all_store=$this->city_model->get_all_city();
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iCityId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iCityId'].'">';
                $alldata[$key]['vCityName'] = $value['vCityName'];
                $alldata[$key]['eStatus'] = $value['eStatus'];
                $alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'city/update/'.$value['iCityId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'city/city_delete?iCityId='.$value['iCityId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		if($this->input->post()){
			$city = $this->input->post('city_detail'); 
			$iCityId = $this->city_model->add_city($city);
			if($iCityId){
				$this->session->set_flashdata('message',"City added successfully");
				redirect($this->data['admin_url'].'city');
			}else{
				$this->session->set_flashdata('message',"City added successfully");
				redirect($this->data['admin_url'].'city');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add City', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/city/create-city.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$iCityId = $this->uri->segment(4);
		$this->data['size'] = $this->city_model ->get_city_details($iCityId);
		$eStatus = field_enums('city','eStatus');
		
		if($this->input->post()){
			$data = $this->input->post('size_detail');
			$data['iCityId'] = $this->input->post('iCityId');
			$id = $this->city_model->update($data['iCityId'],$data);
			if($data['iCityId']){
				$this->session->set_flashdata('message',"City added successfully");
				redirect($this->data['admin_url'] . 'city');
			}else{
				$this->session->set_flashdata('message',"City added successfully");
				redirect($this->data['admin_url'] . 'city');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/city/edit-city.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='city';
		$tableData['update_field']='iCityId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->city_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'city'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->city_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'city'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->city_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'city'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'city'); 
		}
	}

	function city_delete(){
		$ids = $this->input->get('iCityId');
		$data= $this->city_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'city'); 
		
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


