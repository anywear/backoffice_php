<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class products extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/products_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['menuAction'] = 'store';
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/products/list-products.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_product_listing(){
		$iStoreId=$this->input->get('iStoreId');
		$all_products=$this->products_model->get_all_product($iStoreId);
		if(count($all_products) > 0){ 
			foreach ($all_products as $key => $value){
				$alldata[$key]['iProductId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iProductId'].'">';
				//$alldata[$key]['vName'] = '<a href="'.$this->data['base_url'].'admin/products/view_products?iProductId='.$value['iProductId'].'">'.$value['vName'].'</a>';
				$alldata[$key]['vName'] = $value['vName'];
				$alldata[$key]['dAddedDate'] = date('M d Y',strtotime($value['dAddedDate']));
			if($value['eDataFrom'] == 'shopstyle'){
				$alldata[$key]['eDataFrom'] = 'Shop Sense';
			}else if($value['eDataFrom'] == 'manual'){
				$alldata[$key]['eDataFrom'] = 'Manually';
			}
				$alldata[$key]['editlink'] = '<a href="'.$this->data['base_url'].'admin/products/update?iProductId='.$value['iProductId'].'" class="btn btn-default"><span class="fa fa-pencil"></span></a></a><a href="#" class="btn btn-default">Delete</a>';
				// $alldata[$key]['editlink'] = '<a href="#"class="btn btn-default"><span class="fa fa-pencil"></span></a></a><a href="#" class="btn btn-default">Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'store';
		$iStoreId=$this->input->get('iStoreId');

		$this->data['iBrandId'] = $this->products_model->get_all_brand_id();
		$this->data['color'] = $this->products_model->get_all_color();
		$this->data['size'] = $this->products_model->get_all_size();
		$this->data['product_cat'] = $this->products_model->get_all_product_cat();
		
		if($this->input->post()){
			$products = $this->input->post('products');
			$iStoreId= $this->input->post('iStoreId');
			$products['vImage'] = $_FILES['vImage']['name'];
			$products['dAddedDate'] =date('Y-m-d');
			$products['iStoreId'] = $iStoreId;
			
			$iProductId = $this->products_model->add_product($products);
			
			//$iProductId = 17;	
			$folder = 'products';
			$img_uploaded = $this->do_upload($iProductId,$folder,'vImage');
			$image['vImage']=$img_uploaded;
			
			$relation = $this->input->post('relation');
			
			
			if(!empty($relation['iProductColorId'])){
				for($i=0;$i<count($relation['iProductColorId']);$i++){
					$relation_color[$i]['iProductId'] = $iProductId;
					$relation_color[$i]['iProductColorId'] = $relation['iProductColorId'][$i];
					
					$iProductColorRelationId = $this->products_model->add_color_relation($relation_color[$i]);	
				}
				
			}
			
			if(!empty($relation['iProductSizeId'])){
				for($j=0;$j<count($relation['iProductSizeId']);$j++){
						$relation_size[$j]['iProductId'] = $iProductId;
						$relation_size[$j]['iProductSizeId'] = $relation['iProductSizeId'][$j];
						$iProductSizeRelationId= $this->products_model->add_size_relation($relation_size[$j]);
				}
			}
						
			if(!empty($relation['iProductCatId'])){
				for($k=0;$k<count($relation['iProductCatId']);$k++){
					$relation_category[$k]['iProductId'] = $iProductId;
					$relation_category[$k]['iProductCatId'] = $relation['iProductCatId'][$k];
					$iProductCategoryRelationId= $this->products_model->add_category_relation($relation_category[$k]);
				}
			}
			
			$this->session->set_flashdata('message',"Product updated successfully");
			redirect($this->data['admin_url'].'store');
			exit;
		}
		$this->data['function']='create';
		$this->data['tpl_name']= "admin/products/create-product.tpl";
		$this->data['iStoreId']= $iStoreId;
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'store';
		$iProductId = $this->input->get('iProductId');
		$eStatus = field_enums('products','eStatus');
		$this->data['iBrandId'] = $this->products_model->get_all_brand_id();
		$this->data['color'] = $this->products_model->get_all_color();
		$this->data['size'] = $this->products_model->get_all_size();
		$this->data['productColor'] = $this->products_model->get_all_product_color_by_id($iProductId);
		$this->data['productSize'] = $this->products_model->get_all_product_size_by_id($iProductId);
		$this->data['product_cat'] = $this->products_model->get_all_product_cat();
		$getAllproduct=$this->products_model->get_product_details($iProductId);

		if($this->input->post()){
			$product_detail = $this->input->post('products');
			$product_detail['vImage'] = $_FILES['vImage']['name'];
			$iStoreId= $this->input->post('iStoreId');
			$product_detail['iStoreId'] = $iStoreId;
			
			$product_detail['iProductId'] = $iProductId;
			$product_detail['dUpdatedDate'] =date('Y-m-d');
			// echo "<pre>";print_r($product_detail);exit;
			$ProductID = $this->products_model->edit_products($product_detail);
			$folder = 'products';
			
			$img_uploaded = $this->do_upload($iProductId,$folder,'vImage');
			$image['vImage']=$img_uploaded;

			$relation = $this->input->post('relation');
			
			
			if(!empty($relation['iProductColorId'])){

				for($i=0;$i<count($relation['iProductColorId']);$i++){
					$relation_color[$i]['iProductId'] = $iProductId;
					$relation_color[$i]['iProductColorId'] = $relation['iProductColorId'][$i];
					
					$iProductColorRelationId = $this->products_model->edit_color_relation($relation_color[$i]);	
				}
				
			}
			
			if(!empty($relation['iProductSizeId'])){
				for($j=0;$j<count($relation['iProductSizeId']);$j++){
						$relation_size[$j]['iProductId'] = $iProductId;
						$relation_size[$j]['iProductSizeId'] = $relation['iProductSizeId'][$j];
						$iProductSizeRelationId= $this->products_model->edit_size_relation($relation_size[$j]);
				}
			}
						
			if(!empty($relation['iProductCatId'])){
				for($k=0;$k<count($relation['iProductCatId']);$k++){
					$relation_category[$k]['iProductId'] = $iProductId;
					$relation_category[$k]['iProductCatId'] = $relation['iProductCatId'][$k];
					$iProductCategoryRelationId= $this->products_model->edit_category_relation($relation_category[$k]);
				}
			}

			$this->session->set_flashdata('message',"Product updated successfully");            
			redirect($this->data['admin_url'].'store');
			exit;
		}
		$this->data['function']='update';
		$this->data['products_detail'] = $getAllproduct;
		
		// echo "<pre>";print_r($this->data);exit;
		$this->data['tpl_name']= "admin/products/edit-product.tpl";
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	/*function action_update(){
		$this->data['menuAction'] = 'store';
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');

		$tableData['tablename']='products';
		$tableData['update_field']='iProductId';
		if($action=='Delete'){
		    $count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->products_model->delete_products_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'home'); 
		}elseif ($action=='Yes') {
			$Data['iStoreId']=$ids;
			$Data['eOnSale']=$action;
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->home_model->set_Store_onSale($Data);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Update successfully");
			redirect($this->data['admin_url'] . 'store'); 
		}
	}*/

	function product_delete(){
		$ids = $this->input->get('iProductId');
		$data= $this->products_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'store'); 
		
	}

	function view_products(){
		$this->data['menuAction'] = 'products';
		$iProductId = $this->input->get('iProductId');
		$getProductDetail = $this->products_model->get_product_details($iProductId);
		$this->data['getProductDetail'] = $getProductDetail;
		//echo "<pre>";print_r($this->data);exit;
		$this->data['tpl_name']= "admin/products/view-product-detail.tpl";
		$this->smarty->assign('data', $this->data); 
		$this->smarty->view('admin/admin_template.tpl');
	}

	function do_upload($iProductId,$folder,$image){

		if(!is_dir('uploads/'.$folder.'/'.$iProductId)){
			@mkdir('uploads/'.$folder.'/'.$iProductId, 0777);
		}
		if(!is_dir('uploads/'.$folder.'/'.$iProductId)){
			@mkdir('uploads/'.$folder.'/'.$iProductId, 0777);
		}

		$img=$this->data['base_path'].'uploads/'.$folder.'/'.$iProductId.'/'.$_FILES[$image]['name'];
		
		if (file_exists($img)){			
			$rnd1=$this->rand_number();			
			$name='copy_'.$rnd1.$_FILES[$image]['name'];
			$file_name=str_replace(' ','_',$name);
		}else {

			$file_name=str_replace(' ','_',$_FILES[$image]['name']);
		}
		
		$config = array(
			'allowed_types' => 'gif|GIF|JPG|jpg|JPEG|jpeg||PNG|png',
			'upload_path' => 'uploads/'.$folder.'/'.$iProductId,
			'file_name' => str_replace(' ','',$file_name),
			'max_size'=>5380334
		);
		
		$this->load->library('Upload', $config);          
		$this->upload->initialize($config);
		$this->upload->do_upload($image); //do upload
		$image_data = $this->upload->data(); //get icon data		
		
		$img_uploaded = $image_data['file_name'];   
		return $img_uploaded;
		
	}

	
    
    function city(){
    	//echo '<pre>';print_r($this->data['userdata']['anywear_admin_info']);
    	$this->data['iCityId']=$this->input->get('iCityId');
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/home/view-store-city.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');     	
    }
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


