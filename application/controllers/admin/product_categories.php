<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product_categories extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/product_categories_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'product_categories';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/product_categories/view-product-categories.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_product_categories_listing(){
		$product_categories = $this->product_categories_model->get_all_product_categories_listing();
		if(count($product_categories) > 0){ 
			foreach ($product_categories as $key => $value){
				$alldata[$key]['id'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['id'].'">';
				//$alldata[$key]['vProductCatId'] = $value['vProductCatId'];
				$alldata[$key]['vName'] = $value['vName'];
				$alldata[$key]['vShortName'] = $value['vShortName'];
				$alldata[$key]['vSizeTitle'] = $value['vSizeTitle'];
				$alldata[$key]['vLocalizedId'] = $value['vLocalizedId'];
				//$alldata[$key]['vParentId'] = $value['vParentId'];
				//$alldata[$key]['eHasHeelHeightFilter'] = $value['eHasHeelHeightFilter'];
				//$alldata[$key]['eHasColorFilter'] = $value['eHasColorFilter'];
				if($value['eDataFrom'] == 'shopstyle'){
                	$alldata[$key]['eDataFrom'] = 'Shop Sense';
                }else if($value['eDataFrom'] == 'manual'){
                	$alldata[$key]['eDataFrom'] = 'Manually';
                }
				// $alldata[$key]['eDataFrom'] = $value['eDataFrom'];
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'product_categories/update/'.$value['id'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'product_categories/store_delete?id='.$value['id'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'product_categories';
		$pranet_product_category = $this->product_categories_model->get_parent_product_category('clothes-shoes-and-jewelry','','clothes-shoes-and-jewelry','1','clothes-shoes-and-jewelry');
		
		if($this->input->post()){
			$data = $this->input->post('data');
			
			if($data['vParentId'] == ''){
				$data['vParentId'] = 'clothes-shoes-and-jewelry';
			}
			
			$id = $this->product_categories_model->add_product_categories($data);
			if($id){
				$this->session->set_flashdata('message',"Product Categories added successfully");
				redirect($this->data['admin_url'].'product_categories');
			}else{
				$this->session->set_flashdata('message',"Product Categories successfully");
				redirect($this->data['admin_url'].'product_categories');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Product Categories', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/product_categories/create-product-categories.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('pranet_product_category', $pranet_product_category);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'product_categories';
		$id = $this->uri->segment(4);
		$this->data['product_categories'] = $this->product_categories_model ->get_product_categories_details($id);
		$pranet_product_category = $this->product_categories_model->get_parent_product_category('clothes-shoes-and-jewelry','','clothes-shoes-and-jewelry','1','clothes-shoes-and-jewelry');
		if($this->input->post()){
			$data = $this->input->post('data');
			if($data['vParentId'] == ''){
				$data['vParentId'] = 'clothes-shoes-and-jewelry';
			}
			
			$id = $this->product_categories_model->update($data['id'],$data);
			if($data['id']){
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'product_categories');
			}else{
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'product_categories');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/product_categories/edit-product-categories.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('pranet_product_category', $pranet_product_category);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='product_categories';
		$tableData['update_field']='id';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_categories_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_categories'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_categories_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_categories'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_categories_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_categories'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'product_categories'); 
		}
	}

	function store_delete(){
		$ids = $this->input->get('id');
		$data= $this->product_categories_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'product_categories'); 
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


