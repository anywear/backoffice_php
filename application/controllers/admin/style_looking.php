<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class style_looking extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/style_looking_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		// echo "hi";exit;
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'store_style_looking';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/style_looking/view-style-looking.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_style_looking_listing(){
		$all_store=$this->style_looking_model->get_all_style_looking();
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iStyleLookingId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iStyleLookingId'].'">';
                $alldata[$key]['vStyleLookingName'] = $value['vStyleLookingName'];
                $alldata[$key]['eStatus'] = $value['eStatus'];
                $alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'style_looking/update/'.$value['iStyleLookingId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'style_looking/style_delete?iStyleLookingId='.$value['iStyleLookingId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'store_style_looking';
		if($this->input->post()){
			$style = $this->input->post('style_detail');
			$iStyleLookingId = $this->style_looking_model->add_style_looking($style);
			if($iStyleLookingId){
				$this->session->set_flashdata('message',"Price added successfully");
				redirect($this->data['admin_url'].'style_looking');
			}else{
				$this->session->set_flashdata('message',"Price added successfully");
				redirect($this->data['admin_url'].'style_looking');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Style', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/style_looking/create-style-looking.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'store_style_looking';
		$iStyleLookingId = $this->uri->segment(4);
		$this->data['size'] = $this->style_looking_model->get_style_looking_details($iStyleLookingId);
		$eStatus = field_enums('style_looking','eStatus');
		if($this->input->post()){
			$data = $this->input->post('style_detail');
			$data['iStyleLookingId'] = $this->input->post('iStyleLookingId');
			$id = $this->style_looking_model->update($data['iStyleLookingId'],$data);
			if($data['iStyleLookingId']){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'style_looking');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'style_looking');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/style_looking/edit-style-looking.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function store_delete(){
		$ids = $this->input->get('iStyleLookingId');
		$data= $this->style_looking_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'style_looking'); 
		
	}
	
	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='style_looking';
		$tableData['update_field']='iStyleLookingId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->style_looking_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'style_looking'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->style_looking_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'style_looking'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->style_looking_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'style_looking'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'style_looking'); 
		}
	}

	function style_delete(){
		$ids = $this->input->get('iStyleLookingId');
		$data= $this->style_looking_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'style_looking'); 
	}
	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


