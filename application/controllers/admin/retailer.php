<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class retailer extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/retailer_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'retailer';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/retailer/view-retailer.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_retailer_listing(){
		$retailer = $this->retailer_model->get_all_retailer_listing();
		if(count($retailer) > 0){ 
			foreach ($retailer as $key => $value){
				$alldata[$key]['iRetailerId'] = '<input type="checkbox" name="iId[]" iRetailerId="iId" value="'.$value['iRetailerId'].'">';
				$alldata[$key]['vName'] = $value['vName'];
				$alldata[$key]['eDeeplinkSupport'] = $value['eDeeplinkSupport'];
				$alldata[$key]['vHostDomain'] = $value['vHostDomain'];
				$alldata[$key]['eDataFrom'] = $value['eDataFrom'];
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'retailer/update/'.$value['iRetailerId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'retailer/retailer_delete?iRetailerId='.$value['iRetailerId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'retailer';
		$eDataFrom = field_enums('retailers','eDataFrom');
		$eDeeplinkSupport = field_enums('retailers','eDeeplinkSupport');
		if($this->input->post()){
			$data = $this->input->post('data');
			
			$iRetailerId = $this->retailer_model->add_retailer($data);
			if($iRetailerId){
				$this->session->set_flashdata('message',"Product Categories added successfully");
				redirect($this->data['admin_url'].'retailer');
			}else{
				$this->session->set_flashdata('message',"Product Categories successfully");
				redirect($this->data['admin_url'].'retailer');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Product Categories', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/retailer/create-retailer.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eDataFrom', $eDataFrom);
		$this->smarty->assign('eDeeplinkSupport', $eDeeplinkSupport);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'retailer';
		$iRetailerId = $this->uri->segment(4);
		$this->data['retailer'] = $this->retailer_model->get_retailer_details($iRetailerId);
		$eDataFrom = field_enums('retailers','eDataFrom');
		$eDeeplinkSupport = field_enums('retailers','eDeeplinkSupport');
		if($this->input->post()){
			$data = $this->input->post('data');
			$iRetailerId = $this->retailer_model->update($data['iRetailerId'],$data);
			if($data['iRetailerId']){
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'retailer');
			}else{
				$this->session->set_flashdata('message',"Product Categories Update successfully");
				redirect($this->data['admin_url'] . 'retailer');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/retailer/edit-retailer.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eDataFrom', $eDataFrom);
		$this->smarty->assign('eDeeplinkSupport', $eDeeplinkSupport);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		
		$iRetailerIds = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='retailers';
		$tableData['update_field']='iRetailerId';
		if($action=='Delete'){
			$count=count($iRetailerIds);
			foreach ($iRetailerIds as $row){		            	
				$data= $this->retailer_model->delete_retailers_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'retailer'); 
		}else if($action=='Active'){
			$count=count($iRetailerIds);
			foreach ($iRetailerIds as $row){		            	
				$data= $this->retailer_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'retailer'); 
		}else if($action=='Inactive'){
			$count=count($iRetailerIds);
			foreach ($iRetailerIds as $row){		            	
				$data= $this->retailer_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'retailer'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'retailer'); 
		}
	}

	function retailer_delete(){
		$iRetailerIds = $this->input->get('iRetailerId');
		$data= $this->retailer_model->delete_record($iRetailerIds);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'retailer'); 
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


