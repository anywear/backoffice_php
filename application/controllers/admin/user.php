<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class user extends Admin_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('admin/user_model', '', TRUE);
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){            
		   redirect($this->data['admin_url'].'authentication');
		   exit ; 
		}        
		$this->smarty->assign("data",$this->data);        
	} 

	function index(){
		$this->data['menuAction'] = 'user';
		$this->data['tpl_name']= "admin/user/view_user.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}

	function view_users(){
		$this->data['menuAction'] = 'user';
		$iUserId = $this->input->get('iUserId');
		$getUserDetails = $this->user_model->get_user_details($iUserId);

		$this->data['getUserDetails'] = $getUserDetails;
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/user/view_user_details.tpl');  
	}

	function get_user_listing(){
		$all_user = $this->user_model->get_user();
		if(count($all_user) > 0)
		{
			foreach ($all_user as $key => $value)
			{
				$alldata[$key]['iUserId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iUserId'].'">';
				$alldata[$key]['vFirstName'] = $value['vFirstName'];
				$alldata[$key]['vLastName'] = $value['vLastName'];
				$alldata[$key]['vEmail'] = $value['vEmail'];
			}        
			$aData['aaData'] =  $alldata;
		}else{
			$aData['aaData'] = '';
		}
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}
	
	function send_mail(){
		$iUserId = $this->input->get('iUserId');
		$getUseremail = $this->user_model->get_user_email($iUserId);
		$to = $getUseremail['vEmail'];
		
		// subject
		$subject = 'Contact:Testing';
		// message
		$message = '
			<html> 
			  <body bgcolor="#DCEEFC"> 
			    	<table border="1" cellpadding="5" cellspacing="5">
			    		Testing Mail
					</table>
			        <b></b> <br> 
			    </body> 
			</html> 
			';
			
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: demo2.testing2@gmail.com'."\r\n";
					'X-Mailer: PHP/' . phpversion();

		// Mail it
		mail($to, $subject, $message, $headers);
		$this->smarty->view('admin/user/view_user.tpl'); 
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
?>