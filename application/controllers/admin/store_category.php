<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class store_category extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/store_category_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		// echo "hi";exit;
		$this->data['menuAction'] = 'store_category';
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/store_category/view-store-category.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_store_category_listing(){
		$all_store=$this->store_category_model->get_all_store_category_looking();
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iStoreCategoryId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iStoreCategoryId'].'">';
                $alldata[$key]['vStoreCategoryName'] = $value['vStoreCategoryName'];
                $alldata[$key]['eStatus'] = $value['eStatus'];
                $alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'store_category/update/'.$value['iStoreCategoryId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'store_category/store_delete?iStoreCategoryId='.$value['iStoreCategoryId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		
		$this->data['menuAction'] = 'store_category';
		if($this->input->post()){
			$store = $this->input->post('store_detail');
			// echo "<pre>";print_r($store);exit;
			$iStyleLookingId = $this->store_category_model->add_style_looking($store);
			if($iStyleLookingId){
				$this->session->set_flashdata('message',"Store Category added successfully");
				redirect($this->data['admin_url'].'store_category');
			}else{
				$this->session->set_flashdata('message',"Store Category added successfully");
				redirect($this->data['admin_url'].'store_category');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Style', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/store_category/create-store-category.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'store_category';
		$iStoreCategoryId = $this->uri->segment(4);
		$this->data['size'] = $this->store_category_model->get_store_category_details($iStoreCategoryId);
		$eStatus = field_enums('style_looking','eStatus');
		if($this->input->post()){
			$data = $this->input->post('store_detail');
			$data['iStoreCategoryId'] = $this->input->post('iStoreCategoryId');
			$id = $this->store_category_model->update($data['iStoreCategoryId'],$data);
			if($data['iStoreCategoryId']){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'store_category');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'store_category');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/store_category/edit-store-category.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function store_delete(){
		$ids = $this->input->get('iStoreCategoryId');
		$data= $this->store_category_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'store_category'); 
		
	}
	
	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='store_category';
		$tableData['update_field']='iStoreCategoryId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->store_category_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'store_category'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->store_category_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'store_category'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->store_category_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'store_category'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'store_category'); 
		}
	}

	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


