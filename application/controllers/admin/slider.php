<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class slider extends Admin_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('admin/slider_model', '', TRUE);
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){            
		   redirect($this->data['admin_url'].'authentication');
		   exit ; 
		}        
		$this->smarty->assign("data",$this->data);        
	} 

	function index(){
		$this->data['menuAction'] = 'slider';
		$this->data['tpl_name']= "admin/slider/view_slider.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}

	function create(){
		$this->data['menuAction'] = 'slider';
		$eStatuses = field_enums('slider','eStatus');
		//$eSliderType = field_enums('slider','eSliderType');
		
		if($this->input->post()){
			$data = $this->input->post('data');

			$iSliderId = $this->slider_model->add_slider($data);
			$img_uploaded = $this->do_upload($iSliderId);
			$Data['vImage'] = $img_uploaded;
			$id = $this->slider_model->update($iSliderId,$Data);
			
			if($iSliderId){
				$this->session->set_flashdata('message',"Slider added successfully");
				redirect($this->data['admin_url'] . 'slider');
			}else{
				$this->session->set_flashdata('message',"Slider added successfully");
				redirect($this->data['admin_url'] . 'slider');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/slider/new_slider.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatuses', $eStatuses);
		//$this->smarty->assign('eSliderType', $eSliderType);
		$this->smarty->view('admin/admin_template.tpl');
		
	}
	
	function update(){
		$this->data['menuAction'] = 'slider';
		$iSliderId = $this->uri->segment(4);
		$this->data['slider'] = $this->slider_model ->get_slider_details($iSliderId);
		$eStatuses = field_enums('slider','eStatus');
		//$eSliderType = field_enums('slider','eSliderType');
		
		if($this->input->post()){
			$data = $this->input->post('data');
			$img_uploaded = $this->do_upload($data['iSliderId']);
			$data['vImage'] = $img_uploaded;
			$id = $this->slider_model->update($data['iSliderId'],$data);
			if($data['iSliderId']){
				$this->session->set_flashdata('message',"Slider added successfully");
				redirect($this->data['admin_url'] . 'slider');
			}else{
				$this->session->set_flashdata('message',"Slider added successfully");
				redirect($this->data['admin_url'] . 'slider');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/slider/edit_slider.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatuses', $eStatuses);
		//$this->smarty->assign('eSliderType', $eSliderType);
		$this->smarty->view('admin/admin_template.tpl');
		
	}

	function get_slider_listing(){
		$all_notifiaction = $this->slider_model->get_slider_listing();
		if(count($all_notifiaction) > 0)
		{
			foreach ($all_notifiaction as $key => $value)
			{
				$alldata[$key]['iSliderId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iSliderId'].'">';
				$alldata[$key]['vTitle'] = $value['vTitle'];
				//$alldata[$key]['eSliderType'] = $value['eSliderType'];
				$alldata[$key]['eStatus'] = $value['eStatus'];
				//$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'slider/update/'.$value['iSliderId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'slider/deleterecord/'.$value['iSliderId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'slider/update/'.$value['iSliderId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a>';
			}
			$aData['aaData'] =  $alldata;
		}else{
			$aData['aaData'] = '';
		}
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}
	
	
	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='slider';
		$tableData['update_field']='iSliderId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->slider_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'slider'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->slider_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'slider'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->slider_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'slider'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'slider'); 
		}
	}
	
	function do_upload($iSliderId)
	{
		if(!is_dir('uploads/slider/')){
			@mkdir('uploads/slider/', 0777);
		}
		if(!is_dir('uploads/slider/'.$iSliderId)){
			@mkdir('uploads/slider/'.$iSliderId, 0777);
		}
		$config = array(
			'allowed_types' => "gif|GIF|JPG|jpg|JPEG|jpeg|PNG|png|TIF|tif",
			'upload_path' => 'uploads/slider/'.$iSliderId,
			'file_name' => str_replace(' ','',$_FILES['vImage']['name']),
			'max_size'=>5380334
		);
		$this->upload->initialize($config);
		$this->upload->do_upload('vImage'); //do upload
		$image_data = $this->upload->data(); //get image data
                $config1 = array(
			'source_image' => $image_data['full_path'], //get original image
			'new_image' => 'uploads/slider/'.$iSliderId.'/210x158_'.$image_data['file_name'], //save as new image //need to create thumbs first
			'maintain_ratio' => false,
			'width' => 210,
			'height' => 158
		      );
		$this->image_lib->initialize($config1);
		$test1 = $this->image_lib->resize(); //do whatever specified in config
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	function deleteicon()
	{
		$upload_path = $this->config->item('base_upload');
		$iSliderId = $_REQUEST['id'];
		$data = $this->slider_model->get_slider_details($iSliderId);
		$var = unlink($upload_path.'slider/'.$iSliderId.'/'.$data['vImage']);
		$var = unlink($upload_path.'slider/'.$iSliderId.'/'.'210x158_'.$data['vImage']);
		if($var)
		{
			$var = $this->slider_model->delete_image($iSliderId);
		}
		redirect($this->data['admin_url'] . 'slider/update/'.$iSliderId); 
		exit;
	}
	
	function deleterecord(){
		$iSliderId = $this->uri->segment(4);
		$tableData['tablename']='slider';
		$tableData['update_field']='iSliderId';
		$data= $this->slider_model->delete_slider_record($iSliderId,$tableData);
		redirect($this->data['admin_url'] . 'slider'); 
	}

	
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
?>
