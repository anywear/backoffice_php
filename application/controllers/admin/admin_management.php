<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_management extends Admin_Controller{
	function __construct(){
		parent::__construct();        
		// echo "<pre>";print_r($this->data);exit;
		$this->load->model('admin/admin_management_model', '', TRUE); 
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);       
		$this->smarty->assign("data",$this->data);                              
		if(! isset($this->session->userdata['anywear_admin_info'])){            
			redirect($this->data['admin_url'].'authentication');
			exit;
		}
		
	}
	
	function index(){
		$this->data['tpl_name']= "admin/admin_management/view_admin.tpl";
		$this->data['message'] = $this->session->flashdata('message');
		$this->data['anywear_admin_info']=$this->session->userdata['anywear_admin_info'];        
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}
	
	function action_update(){
		$ids = $this->input->post('iId'); 
		$action=$this->input->post('action');
		$iAdminId = $this->input->get('iAdminId');      
		$tableData['tablename']='administrators';
		$tableData['update_field']='iAdminId';   
		//$count=$this->admin_management_model->delete_admin_record($iAdminId,$tableData);    
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){	
				$data=$this->admin_management_model->delete_admin_record($row,$tableData);
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record deleted successfully");
			redirect($this->data['admin_url'] . 'admin-management'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'admin-management');
		}           
	}
	
	function create(){
		if($this->input->post()){
			$admin = $this->input->post('admin_detail');
			$admin['vPassword'] = md5($admin['vPassword']);
			$iAdminId = $this->admin_management_model->add_admin($admin);
            if($iAdminId){
                $this->session->set_flashdata('message',"Admin details added successfully");
                redirect($this->data['admin_url'] . 'admin-management');
            }else{
                $this->session->set_flashdata('message',"Admin details added successfully");
                redirect($this->data['admin_url'] . 'admin-management');
            }
            exit;
        }   
        $this->data['function']='create';
        $this->smarty->assign('data', $this->data);
        $this->smarty->view('admin/admin_management/create_admin.tpl');
	}
	
	function update(){
		$iAdminId = $this->input->get('iAdminId');
		$this->data['admin_detail'] = $this->admin_management_model ->get_admin_details($iAdminId);
		if($this->input->post()){
			$admin_detail = $this->input->post('admin_detail');
			
			$adminDetail['vFirstName'] = $admin_detail['vFirstName'];
		    $adminDetail['vLastName'] = $admin_detail['vLastName'];
		    $adminDetail['vEmail'] = $admin_detail['vEmail'];
		    if($admin_detail['vPassword']){
		    	$adminDetail['vPassword'] = md5($admin_detail['vPassword']);
		    }
			$iAdminId= $this->input->post('iAdminId');
			$adminDetail['iAdminId'] = $iAdminId;
			
			$iAdminId = $this->admin_management_model->edit_admin($adminDetail);
			$this->session->set_flashdata('message',"Admin updated successfully");            
			redirect($this->data['admin_url'].'admin-management');
			exit;
		}
		$this->data['function']='update';
		$this->data['tpl_name']= "admin/admin_management/edit_admin.tpl";  
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_management/edit_admin.tpl'); 
	}
	
	
	function get_admin_listing(){
		$all_admins = $this->admin_management_model->get_all_admins();
		if(count($all_admins) > 0){
			foreach ($all_admins as $key => $value){
					$alldata[$key]['iAdminId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iAdminId'].'">';
					$alldata[$key]['vFirstName'] = $value['vFirstName'];
					$alldata[$key]['vLastName'] = $value['vLastName'];
					$alldata[$key]['vEmail'] = $value['vEmail'];
			}        
			$aData['aaData'] =  $alldata;
		}
		else{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}   


	function check_email_exist(){

		$vEmail = $this->input->post('id');
		echo $vEmail;exit();
		$checkexist = $this->admin_management_model->admin_exists($vEmail);
		if ($checkexist!=0) {
			echo "exitst";
		}else{
			echo "Not exitst";
		}
		exit;
    }


	function deleteicon(){
		$iAdminId  = $this->input->get('iAdminId');
		$deleteImage=$this->delete_image($iAdminId);  
		redirect($this->data['admin_url'].'admin_management/update/'.$iAdminId);
	}

}
?>
