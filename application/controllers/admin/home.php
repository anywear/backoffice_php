<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class home extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/home_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		//echo "<pre>";print_r($this->data);exit;
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['menuAction'] = 'store';
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/home/homes.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_store_listing(){
		
		$iCityId=$this->input->get('iCityId');
		if($iCityId){
	    	$cityData=$this->home_model->get_store_city($iCityId);
	        $iCityId = $cityData['vCityName'];
	        $all_store=$this->home_model->get_all_store_with_city($iCityId);
		}else{
			$all_store=$this->home_model->get_all_store();
		}
		//echo '<pre>';print_r($all_store);exit;
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$favVal = ($value['eFav'] == 'No')? 'Yes' : 'No' ;
				$onSaleVal = ($value['eOnSale'] == 'No')? 'Yes' : 'No' ;
				$alldata[$key]['iStoreId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iStoreId'].'">';
				$tempScript = "<script type='text/javascript'>
		                (function($) {
		 					$('#store_".$value['iStoreId']."').on('click',function(){
		 						var _thisObtId = $(this).attr('id');
		 						$.ajax({
									url: '".$this->data['admin_url']."home/hit_favorite?storeId=".$value['iStoreId']."',
									type: 'POST',
									//data: serializeFormData,
									success: function(response){
										if(response.replace(/\s/g,'') == 'Yes'	){
											$('#'+_thisObtId).removeClass('star_normal').addClass('star_yellow');
										}else{
											$('#'+_thisObtId).removeClass('star_yellow').addClass('star_normal');
										}
										return false;
									},
									error:function(){
										alert('Sorry some errror ocurs while send mail');
										return false;
									}   
								});
		    				});
		    			})(jQuery);
					</script>";
				$starClass = 'star_yellow';
				if($value['eFav'] == 'No'){
					$starClass = 'star_normal';
				}
                $alldata[$key]['eFav'] ="<a href='javascript:;' id='store_".$value['iStoreId']."' class='hit_favorite ".$starClass."' ><span class='fa fa-fw'></span></a>".$tempScript;
                
                $alldata[$key]['vStoreName'] = '<a href="'.$this->data['base_url'].'admin/store/view_store?iStoreId='.$value['iStoreId'].'">'.$value['vStoreName'].'</a>';
                $alldata[$key]['dCurrentdate'] = date('M d Y',strtotime($value['dCurrentdate']));
                if($value['eDataFrom'] == 'shopstyle'){
                	$alldata[$key]['eDataFrom'] = 'Shop Sense';
                }else if($value['eDataFrom'] == 'manual'){
                	$alldata[$key]['eDataFrom'] = 'Manually';
                }
                
                /*Start On Sale Script*/
                $onSaleScript = "<script type='text/javascript'>
		                (function($) {
		                	$('#hit_onSale_".$value['iStoreId']."').on('click',function(){
		 						var _thisObtId = $(this).attr('data-id');
		 						
		 						$.ajax({
									url: '".$this->data['admin_url']."home/hit_onSale?storeId=".$value['iStoreId']."',
									type: 'POST',
									//data: serializeFormData,
									success: function(response){
										if(response.replace(/\s/g,'') == 'Yes'	){
											$('#hit_onSale_'+_thisObtId).removeClass('on_normal').addClass('btn-success');
										}else{
											$('#hit_onSale_'+_thisObtId).removeClass('btn-success').addClass('on_normal');
										}
										return false;
									},
									error:function(){
										alert('Sorry some errror ocurs while send mail');
										return false;
									}   
								});
		    				});
		    			})(jQuery);
					</script>";	
                /*End On Sale Script*/
                $onSaleClass = 'btn-success';
				if($value['eOnSale'] == 'No'){
					$onSaleClass = 'on_normal';
				}
				
                $alldata[$key]['editlink'] = "<a href='javascript:;' id='hit_onSale_".$value['iStoreId']."' data-id='".$value['iStoreId']."' class='btn btn-default ".$onSaleClass."'>Sale</a>".$onSaleScript.'
                							<a href="'.$this->data['base_url'].'admin/store/update?iStoreId='.$value['iStoreId'].'" class="btn btn-default"><span class="fa fa-pencil"></span></a>
                							</a><a href="'.$this->data['base_url'].'admin/store/store_delete?iStoreId='.$value['iStoreId'].'" class="btn btn-default">Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function hit_onSale(){
		$data['iStoreId'] = $_REQUEST['storeId'];
		$data['eOnSale'] = $_REQUEST['storeFavVal'];
		$result = $this->home_model->set_onSale($data);
		echo $result;exit;
	}

	function hit_favorite(){
		$data['iStoreId'] = $_REQUEST['storeId'];
		$data['eFav'] = $_REQUEST['storeFavVal'];
		$result = $this->home_model->set_favorite($data);
		echo $result;exit;
	}

	function create(){
		
		$this->data['menuAction'] = 'store';
		$style = $this->home_model->get_all_style();
		$style_looking = $this->home_model->get_all_style_looking();
		$store_size = $this->home_model->get_all_store_size();
		$store_category = $this->home_model->get_all_store_category();
		$eSign = $this->home_model->get_all_money_spend();
		$eDeeplinkSupport = field_enums('store','eDeeplinkSupport');

		$style1 = $all_tag; 		
		$new_array=array();
		foreach($all_tag as $i=>$all_tag){
			$new_array[$i]='"'.$all_tag['vStyle'].'"';
		}
		$tag_array='['.implode(",",$new_array).']';

		// echo "<pre>";print_r($tag_array);exit;
		// echo "<pre>";print_r($style);exit;
		$uploaded_files = array();
		if($this->input->post()){
			
			$store = $this->input->post('store_detail');
			$store['vSize'] = $this->input->post('vSize');
			$store['vSize']=implode(",",$store['vSize']);									
			$store['vPriceAvr'] = $this->input->post('vPriceAvr');
			$store['vPriceAvr']=implode(",",$store['vPriceAvr']);									
			$store['vMainImage'] = $_FILES['vMainImage']['name'];
			$store['vSmallIamge'] =$_FILES['vSmallIamge']['name'];
			$store['vShopImage'] =$_FILES['vShopImage']['name'];
			//$store['vShopImage'] =implode(",",$store['vShopImage']);
			$store['dCurrentdate'] =date('Y-m-d');
			$store['eDataFrom'] ='manual';
			

			$iStoreId = $this->home_model->add_store($store);
			$folder = 'store';
			$MainImage = 'MainImage';
			$SmalImage = 'SmalImage';
			$ShopImage = 'ShopImage';
			$img_uploaded = $this->do_upload($iStoreId,$folder,$MainImage,'vMainImage');
			$image['vMainImage']=$img_uploaded;
			$img_uploaded1 = $this->do_upload_small_image($iStoreId,$folder,$SmalImage,'vSmallIamge');
			$image['vSmallIamge']=$img_uploaded1;
			$img_uploaded2 = $this->do_upload_shop_image($iStoreId,$folder,$ShopImage,'vShopImage');
			$image['vShopImage']=$img_uploaded2;
			/*for($i=0;$i<count($_FILES['vShopImage']['name']);$i++){	
				$uploaded_files[]=$_FILES['vShopImage']['name'][$i];
				$ShopImage = $this->do_upload_shop_image($iStoreId,$folder,$ShopImage,'vShopImage');
				$image['vShopImage']=$ShopImage;
				$iShopimageId = $this->home_model->add_shop_image($iStoreId,$_FILES['vShopImage']['name'][$i]);	
			}*/
			if($iStoreId){
				$this->session->set_flashdata('message',"Store details added successfully");
				redirect($this->data['admin_url'].'home');
			}else{
				$this->session->set_flashdata('message',"Store details added successfully");
				redirect($this->data['admin_url'].'home');
			}
			exit;
	    }
		//echo "<pre>";print_r($sizearray);exit;
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Store', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/home/create-store.tpl";
		$this->smarty->assign('style',$style);
		$this->smarty->assign("allTagGet",$tag_array);
		$this->smarty->assign('store_size', $store_size);
		$this->smarty->assign('style_looking', $style_looking);
		$this->smarty->assign('store_category', $store_category);
		$this->smarty->assign('eSign', $eSign);
		$this->smarty->assign('eDeeplinkSupport', $eDeeplinkSupport);
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function changepassword(){
		$this->breadcrumb->add('Dashboard', $this->data['admin_url']);
		$this->breadcrumb->add('Change Password', '');
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['anywear_admin_info']=$this->session->userdata['anywear_admin_info'];		
		$this->data['tpl_name']= "admin/home/changepassword.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
		if($this->input->post('vPassword')){
			$new_password=$this->input->post('vPassword');
			$user['vPassword'] = encrypt($this->input->post('vPassword'));
			$user['iAdminId']=$this->data['anywear_admin_info']['iAdminId'];
			$this->home_model->change_password($user);
			redirect($this->data['admin_url']);
		}
	}

	function update(){
		$this->data['menuAction'] = 'store';
		
		$selectedTab=$this->input->get('tab');
		if($selectedTab){
			$this->data['selectedTab'] = 'product';
		}else{
			$this->data['selectedTab'] = 'store';
		}
		
		$style = $this->home_model->get_all_style();
		$style_looking = $this->home_model->get_all_style_looking();
		$store_size = $this->home_model->get_all_store_size();
		$store_category = $this->home_model->get_all_store_category();
		$eSign = $this->home_model->get_all_money_spend();
		$eDeeplinkSupport = field_enums('store','eDeeplinkSupport');
		$iStoreId = $this->input->get('iStoreId');
		
		$getAllStore=$this->home_model->get_store_details($iStoreId);
		
		$vSize=$getAllStore['vSize'];
		$vSize=explode(",",$vSize);
		
		$new_array=array();
		foreach ($store_size as $i => $value) {
			$new_array[$i] = $value['vSizeTitle'];
			foreach ($new_array as $key => $value) {
				if(in_array($value, $vSize)){
				$size_checked[$key]='checked';
			}else{
				$size_checked[$key]='';
			}
			}
		}
		
		$vPriceAvr=$getAllStore['vPriceAvr'];
		$vPriceAvr=explode(",",$vPriceAvr);
		
		$sign_array = array();
		foreach ($eSign as $i => $value) {
			$sign_array[$i] = $value['eSign'];
			foreach ($sign_array as $key => $value) {
				if(in_array($value, $vPriceAvr)){
					$price_checked[$i]='checked';
				}else{
					$price_checked[$i]='';
				}
			}
			
		}
		$this->data['store_detail']=$getAllStore;
		$this->data['vSize']=$vSize;
		$this->data['vPriceAvr']=$vPriceAvr;
		if($this->input->post()){
			
			$store_detail = $this->input->post('store_detail');
			
			$store_detail['vSize'] = $this->input->post('vSize');
			$store_detail['vSize']=implode(",",$store_detail['vSize']);									
			$store_detail['vPriceAvr'] = $this->input->post('vPriceAvr');
			$store_detail['vPriceAvr']=implode(",",$store_detail['vPriceAvr']);									
			$store_detail['vMainImage'] = $_FILES['vMainImage']['name'];
			$store_detail['vSmallIamge'] =$_FILES['vSmallIamge']['name'];
			$store_detail['vShopImage'] =$_FILES['vShopImage']['name'];
			$store_detail['dUpdateDate'] =date('Y-m-d');
			
			// echo "<pre>";print_r($store_detail);exit;
			$iStoreId= $this->input->post('iStoreId');
			$store_detail['iStoreId'] = $iStoreId;
			$StoreId = $this->home_model->edit_store($store_detail);
			$folder = 'store';
			$MainImage = 'MainImage';
			$SmalImage = 'SmalImage';
			$ShopImage = 'ShopImage';


			$img_uploaded = $this->do_upload($iStoreId,$folder,$MainImage,'vMainImage');
			$image['vMainImage']=$img_uploaded;

			$img_uploaded1 = $this->do_upload_small_image($iStoreId,$folder,$SmalImage,'vSmallIamge');
			$image['vSmallIamge']=$img_uploaded1;

			$img_uploaded2 = $this->do_upload_shop_image($iStoreId,$folder,$ShopImage,'vShopImage');
			$image['vShopImage']=$img_uploaded2;	
			//echo "<pre>";print_r($store_detail);exit;
			
			
			$this->session->set_flashdata('message',"Store updated successfully");            
			redirect($this->data['admin_url'].'home');
			exit;
		}
		$this->data['function']='update';
		$this->data['iStoreId']=$iStoreId;
		$this->smarty->assign('style',$style);
		$this->smarty->assign("allTagGet",$tag_array);
		$this->smarty->assign('store_size', $store_size);
		$this->smarty->assign('style_looking', $style_looking);
		$this->smarty->assign('size_checked', $size_checked);
		$this->smarty->assign('price_checked', $price_checked);
		$this->smarty->assign('store_category', $store_category);
		$this->smarty->assign('eSign', $eSign);
		$this->smarty->assign('eDeeplinkSupport', $eDeeplinkSupport);
		/*$this->smarty->assign('sizearray', $sizearray);
		
		$this->smarty->assign('pricearray', $pricearray);
		*/
		$this->data['tpl_name']= "admin/home/edit-store.tpl";  
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$this->data['menuAction'] = 'store';
		
		
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');

		$tableData['tablename']='store';
		$tableData['update_field']='iStoreId';
		if($action=='Delete'){
		    $count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->home_model->delete_store_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'home'); 
		}elseif ($action=='Yes') {
			$Data['iStoreId']=$ids;
			$Data['eOnSale']=$action;
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->home_model->set_Store_onSale($Data);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Update successfully");
			redirect($this->data['admin_url'] . 'home'); 
		}
	}

	function store_delete(){
		$ids = $this->input->get('iStoreId');
		$data= $this->home_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'home'); 
		
	}

	function view_store(){
		$this->data['menuAction'] = 'store';
		$iStoreId = $this->input->get('iStoreId');
		$getStoreDetail = $this->home_model->get_store_details($iStoreId);
		$this->data['getStoreDetail'] = $getStoreDetail;
		//echo "<pre>";print_r($this->data);exit;
		$this->data['tpl_name']= "admin/home/view-store-detail.tpl";
		$this->smarty->assign('data', $this->data); 
		$this->smarty->view('admin/admin_template.tpl');
	}

	function store_add_address(){
		$this->data['menuAction'] = 'store';
		$sizearray[0]='S'; 
		$sizearray[1]='M';
		$sizearray[2]='L';
		$sizearray[3]='X';
		$sizearray[4]='XL';
		$sizearray[5]='XXL';
		$iStoreId = $this->input->get('iStoreId');
		
		$getAllStore=$this->home_model->get_store_details($iStoreId);
		
		$vSize=$getAllStore['vSize'];
		$vSize=explode(",",$vSize);

		foreach ($sizearray as $i => $value) {
			if(in_array($value, $vSize)){
				$size_checked[$i]='checked';
			}else{
				$size_checked[$i]='';
			}
		}

		
		$pricearray[0]='$';
		$pricearray[1]='$$';
		$pricearray[2]='$$$';
		$pricearray[3]='$$$$';
		$pricearray[4]='$$$$$';



		$vPriceAvr=$getAllStore['vPriceAvr'];
		$vPriceAvr=explode(",",$vPriceAvr);
		
		foreach ($pricearray as $i => $value) {
			if(in_array($value, $vPriceAvr)){
				$price_checked[$i]='checked';
			}else{
				$price_checked[$i]='';
			}
		}
		$this->data['store_detail']=$getAllStore;
		$this->data['vSize']=$vSize;
		$this->data['vPriceAvr']=$vPriceAvr;
		if($this->input->post()){
			
			$store_detail = $this->input->post('store_detail');
			
			$store_detail['vSize'] = $this->input->post('vSize');
			$store_detail['vSize']=implode(",",$store_detail['vSize']);									
			$store_detail['vPriceAvr'] = $this->input->post('vPriceAvr');
			$store_detail['vPriceAvr']=implode(",",$store_detail['vPriceAvr']);									
			$store_detail['vMainImage'] = $_FILES['vMainImage']['name'];
			$store_detail['vSmallIamge'] =$_FILES['vSmallIamge']['name'];
			$store_detail['dCurrentdate'] =date('Y-m-d');
			
			$iStoreId= $this->input->post('iStoreId');
			$store_detail['iStoreId'] = $iStoreId;
			$StoreId = $this->home_model->edit_store($store_detail);
			$folder = 'store';
			$MainImage = 'MainImage';
			$SmalImage = 'SmalImage';
			$ShopImage = 'ShopImage';

			$img_uploaded = $this->do_upload($iStoreId,$folder,$MainImage,'vMainImage');
			$image['vMainImage']=$img_uploaded;
			$img_uploaded1 = $this->do_upload_small_image($iStoreId,$folder,$SmalImage,'vSmallIamge');
			$image['vSmallIamge']=$img_uploaded1;		
			$img_uploaded2 = $this->do_upload_shop_image($iStoreId,$folder,$ShopImage,'vShopImage');
			$image['vShopImage']=$img_uploaded2;	
			//echo "<pre>";print_r($store_detail);exit;
			
			// echo "hi";exit;
			$this->session->set_flashdata('message',"Store updated successfully");            
			redirect($this->data['admin_url'].'home');
			exit;
		}
		$this->data['function']='update';
		$this->smarty->assign('sizearray', $sizearray);
		$this->smarty->assign('size_checked', $size_checked);
		$this->smarty->assign('pricearray', $pricearray);
		$this->smarty->assign('price_checked', $price_checked);
		$this->data['tpl_name']= "admin/home/add-store-address.tpl";  
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function do_upload($iStoreId,$folder,$MainImage,$image){

		if(!is_dir('uploads/'.$folder.'/'.$iStoreId)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId, 0777);
		}
		if(!is_dir('uploads/'.$folder.'/'.$iStoreId.'/'.$MainImage)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId.'/'.$MainImage, 0777);
		}

		$img=$this->data['base_path'].'uploads/'.$folder.'/'.$iStoreId.'/'.$MainImage.'/'.$_FILES[$image]['name'];
		
		if (file_exists($img)){			
			$rnd1=$this->rand_number();			
			$name='copy_'.$rnd1.$_FILES[$image]['name'];
			$file_name=str_replace(' ','_',$name);
		}else {

			$file_name=str_replace(' ','_',$_FILES[$image]['name']);
		}
		
		$config = array(
			'allowed_types' => 'gif|GIF|JPG|jpg|JPEG|jpeg||PNG|png',
			'upload_path' => 'uploads/'.$folder.'/'.$iStoreId.'/'.$MainImage,
			'file_name' => str_replace(' ','',$file_name),
			'max_size'=>5380334
		);
		
		$this->load->library('Upload', $config);          
		$this->upload->initialize($config);
		$this->upload->do_upload($image); //do upload
		$image_data = $this->upload->data(); //get icon data		
		
		$img_uploaded = $image_data['file_name'];   
		return $img_uploaded;
		
	}
    function do_upload_small_image($iStoreId,$folder,$SmalImage,$image){
    	if(!is_dir('uploads/'.$folder.'/'.$iStoreId)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId, 0777);
		}
		if(!is_dir('uploads/'.$folder.'/'.$iStoreId.'/'.$SmalImage)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId.'/'.$SmalImage, 0777);
		}
		$img=$this->data['base_path'].'uploads/'.$folder.'/'.$iStoreId.'/'.$SmalImage.'/'.$_FILES[$image]['name'];
		
		if (file_exists($img)){			
			$rnd1=$this->rand_number();			
			$name='copy_'.$rnd1.$_FILES[$image]['name'];
			$file_name=str_replace(' ','_',$name);
		}else {
			$file_name=str_replace(' ','_',$_FILES[$image]['name']);
		}
		$config = array(
			'allowed_types' => 'gif|GIF|JPG|jpg|JPEG|jpeg||PNG|png',
			'upload_path' => 'uploads/'.$folder.'/'.$iStoreId.'/'.$SmalImage,
			'file_name' => str_replace(' ','',$file_name),
			'max_size'=>5380334
		);
		
		$this->load->library('Upload', $config);          
		$this->upload->initialize($config);
		$this->upload->do_upload($image); //do upload
		$image_data = $this->upload->data(); //get icon data		
		
		$img_uploaded = $image_data['file_name'];          
		return $img_uploaded;
    }

    function do_upload_shop_image($iStoreId,$folder,$ShopImage,$image){
    	if(!is_dir('uploads/'.$folder.'/'.$iStoreId)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId, 0777);
		}
		if(!is_dir('uploads/'.$folder.'/'.$iStoreId.'/'.$ShopImage)){
			@mkdir('uploads/'.$folder.'/'.$iStoreId.'/'.$ShopImage, 0777);
		}
		$img=$this->data['base_path'].'uploads/'.$folder.'/'.$iStoreId.'/'.$ShopImage.'/'.$_FILES[$image]['name'];
		
		if (file_exists($img)){			
			$rnd1=$this->rand_number();			
			$name='copy_'.$rnd1.$_FILES[$image]['name'];
			$file_name=str_replace(' ','_',$name);
		}else {
			$file_name=str_replace(' ','_',$_FILES[$image]['name']);
		}
		$config = array(
			'allowed_types' => 'gif|GIF|JPG|jpg|JPEG|jpeg||PNG|png',
			'upload_path' => 'uploads/'.$folder.'/'.$iStoreId.'/'.$ShopImage,
			'file_name' => str_replace(' ','',$file_name),
			'max_size'=>5380334
		);
		
		$this->load->library('Upload', $config);          
		$this->upload->initialize($config);
		$this->upload->do_upload($image); //do upload
		$image_data = $this->upload->data(); //get icon data		
		
		$img_uploaded = $image_data['file_name'];          
		return $img_uploaded;
    }
	
    function city(){
    	//echo '<pre>';print_r($this->data['userdata']['anywear_admin_info']);
    	$this->data['iCityId']=$this->input->get('iCityId');
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/home/view-store-city.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl');     	
    }
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


