<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class notification extends Admin_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('admin/notification_model', '', TRUE);
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){            
		   redirect($this->data['admin_url'].'authentication');
		   exit ; 
		}        
		$this->smarty->assign("data",$this->data);        
	} 

	function index(){
		$this->data['menuAction'] = 'notification';
		$this->data['tpl_name']= "admin/notification/view_notification.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}

	function create(){
		$this->data['menuAction'] = 'notification';
		if($this->input->post()){
			$data = $this->input->post('data');
			$data['dCurrentDate'] = date('Y-m-d h:i:s');
			$subject = $data ['vTitle'];
			$massage = $data ['tDescription'];
			$all_user_email = $this->notification_model->get_all_user_email();
			
			for($i=0;$i<count($all_user_email);$i++){
			
				$headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
				$headers .= 'From:www.anywear.com<support@09php.com>' . "\r\n".
						'Reply-To:www.anywear.com<support@09php.com>'. "\r\n".
						'Return-Path: www.anywear.com<support@09php.com>' . "\r\n".
						'X-Mailer: PHP/' . phpversion();
				
				$subject = $subject ;
				$to = $all_user_email[$i]['vEmail'];
				$message = '
				<html> 
				  <body bgcolor="#DCEEFC"> 
					<table width="100%" border="0" cellpadding="5" cellspacing="5">
						'.$massage.'
						</table>
					<b></b> <br> 
				    </body> 
				</html> 
				';
		
			mail($to, $subject, $message, $headers);
			}
			$iNotificationId = $this->notification_model->add_notify($data);
			
			if($iNotificationId){
				$this->session->set_flashdata('message',"mail send successfully");
				redirect($this->data['admin_url'] . 'notification');
			}else{
				$this->session->set_flashdata('message',"mail send successfully");
				redirect($this->data['admin_url'] . 'notification');
			}
		exit;
		}   
        $this->data['function']='create';
        $this->smarty->assign('data', $this->data);
        $this->smarty->view('admin/notification/compose_notification.tpl');
	}

	function get_notification_listing(){
		$all_notifiaction = $this->notification_model->get_notification_listing();
		if(count($all_notifiaction) > 0)
		{
			foreach ($all_notifiaction as $key => $value)
			{
				//$alldata[$key]['iNotificationId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iNotificationId'].'">';
				$alldata[$key]['iNotificationId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iNotificationId'].'">';
				$alldata[$key]['tDescription'] = '<h4 style="width:100%;float:left;margin-top: 7px;">'.$value['vTitle'].'</h4><br />'.$value['tDescription'];
				$alldata[$key]['dCurrentDate'] = date('M d Y',strtotime($value['dCurrentDate']));
				
				/*$alldata[$key]['iNotificationId'] = '<div style="margin-left: 88%;">'.$value['dCurrentDate'].'</div><div style="width: 10px;margin-top: 22px;"><input type="checkbox" name="iId[]" id="iId" value="'.$value['iNotificationId'].'"></div><div style="margin-left: 29px;margin-top: -40px;color: black; "><h4 style="width:100%;float:left;margin-top: 7px;">'.$value['vTitle'].'</h4>'.$value['tDescription'].'</div>';
				$alldata[$key]['tDescription'] = '<div style="margin-left: 88%;">'.$value['dCurrentDate'].'</div><div style="width: 10px;margin-top: 22px;"><input type="checkbox" name="iId[]" id="iId" value="'.$value['iNotificationId'].'"></div><div style="margin-left: 29px;margin-top: -40px;color: black; "><h4 style="width:100%;float:left;margin-top: 7px;">'.$value['vTitle'].'</h4>'.$value['tDescription'].'</div>';
				$alldata[$key]['dCurrentDate'] = '<div style="margin-left: 88%;">'.$value['dCurrentDate'].'</div><div style="width: 10px;margin-top: 22px;"><input type="checkbox" name="iId[]" id="iId" value="'.$value['iNotificationId'].'"></div><div style="margin-left: 29px;margin-top: -40px;color: black; "><h4 style="width:100%;float:left;margin-top: 7px;">'.$value['vTitle'].'</h4>'.$value['tDescription'].'</div>';*/
			}        
			$aData['aaData'] =  $alldata;
		}else{
			$aData['aaData'] = '';
		}
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}
	
	
	function action_update(){
		$ids = $this->input->post('iId');
		
		$action=$this->input->post('action');
		$tableData['tablename']='notification';
		$tableData['update_field']='iNotificationId';
		
		if($action=='Delete'){
		    $count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->notification_model->delete_notification_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'notification'); 
		}else if($action=='Send'){
			for($k=0;$k<count($ids);$k++){
				$get_notification = $this->notification_model->get_notification($ids[$k]);
				$all_user_email = $this->notification_model->get_all_user_email();
				for($i=0;$i<count($all_user_email);$i++){			
					$headers = "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
					$headers .= 'From:www.anywear.com<support@09php.com>' . "\r\n".
						'Reply-To:www.anywear.com<support@09php.com>'. "\r\n".
						'Return-Path: www.anywear.com<support@09php.com>' . "\r\n".
						'X-Mailer: PHP/' . phpversion();
				
					$to = $all_user_email[$i]['vEmail'];
					$subject = $get_notification['vTitle'];
					$message = '
					<html> 
					  <body bgcolor="#DCEEFC"> 
						<table width="100%" border="0" cellpadding="5" cellspacing="5">
							'.$get_notification['tDescription'].'
							</table>
						<b></b> <br> 
					    </body> 
					</html> 
					';
				
					mail($to, $subject, $message, $headers);
				}
			}
			redirect($this->data['admin_url'] . 'notification'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'notification'); 
		}
	}

	
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
?>