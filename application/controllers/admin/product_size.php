<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product_size extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('admin/product_size_model', '', TRUE);
		$this->load->library('image_lib'); //load library
		$this->load->helper('url');
		$this->load->library('upload');
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){
			redirect($this->data['admin_url'].'authentication');
			exit ; 
		}		
		$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->breadcrumb->add('Dashboard', "");
		$this->data['menuAction'] = 'product_size';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/product_size/view-product-size.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_product_size_listing(){
		$all_store=$this->product_size_model->get_all_size();

		for($i=0;$i<count($all_store);$i++){
		$record=$this->product_size_model->get_peoduct_size_details($all_store[$i]['iProductSizeId']);
		$sizename=$this->product_size_model->get_peoduct_size_details($record['iParentId']);
            
		$child=$this->recursion($sizename['iParentId']);
		if($record['iParentId'] == 0){
		    $all_store[$i]['path']=$record['vSizeName'];    
		}else{
			if(count($child) > 1){
			    $all_store[$i]['path']=$child['vSizeName']." >> ".$sizename['vSizeName']." >> ".$record['vSizeName'];       
			}
			else{
			    $all_store[$i]['path']=$sizename['vSizeName']." >> ".$record['vSizeName'];
			}
		}
        }
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iProductSizeId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iProductSizeId'].'">';
				$alldata[$key]['iParentId'] = $value['path'];
				$alldata[$key]['vSizeName'] = $value['vSizeName'];
				$alldata[$key]['eStatus'] = $value['eStatus'];
				$alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'product_size/update/'.$value['iProductSizeId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'product_size/product_delete?iProductSizeId='.$value['iProductSizeId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'product_size';
		$ParentID = $this->product_size_model->get_parent_size_id('0','','0','1','0');
		if($this->input->post()){
			$size = $this->input->post('size');
			if($size['iParentId']==''){
				$size['iParentId']=0;
			}
			$iStoreSizeId = $this->product_size_model->add_size($size);
			if($iStoreSizeId){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'].'product_size');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'].'product_size');
			}
			exit;
		}
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Size', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/product_size/create-product-size.tpl";   
		$this->smarty->assign('iParentId',$ParentID);
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eSizeType', $eSizeType);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'product_size';
		$iProductSizeId = $this->uri->segment(4);
		$this->data['size'] = $this->product_size_model ->get_size_details($iProductSizeId);
		$ParentID = $this->product_size_model->get_parent_size_id('0','','0','1','0');
		$eStatus = field_enums('product_sizes','eStatus');
		if($this->input->post()){
			$data = $this->input->post('size');
			$data['iProductSizeId'] = $this->input->post('iProductSizeId');
			$id = $this->product_size_model->update($data['iProductSizeId'],$data);
			if($data['iProductSizeId']){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'product_size');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'product_size');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/product_size/edit-product-size.tpl";
		$this->smarty->assign('iParentId',$ParentID);
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='product_sizes';
		$tableData['update_field']='iProductSizeId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_size_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_size'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_size_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_size'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->product_size_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'product_size'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'product_size'); 
		}
	}

	function product_delete(){
		$ids = $this->input->get('iProductSizeId');
		$data= $this->product_size_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'product_size'); 
	}

	function recursion($id){
		$sizename=$this->product_size_model->getchild($id);
		if(count($sizename) > 1){
		    $sizename=$this->product_size_model->getchild($id);
		}
        return $sizename;
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */