<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class personalization extends Admin_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('admin/personalization_model', '', TRUE);
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
		if(! isset($this->session->userdata['anywear_admin_info'])){            
		   redirect($this->data['admin_url'].'authentication');
		   exit ; 
		}        
		$this->smarty->assign("data",$this->data);        
	} 

	function index(){
		$this->data['menuAction'] = 'style';
		$this->data['getCat'] = $this->personalization_model->get_category();
		$this->data['cat1PersonaData'] = $this->personalization_model->get_personalization($this->data['getCat'][0]['iCategoryId']);
		$this->data['cat2PersonaData'] = $this->personalization_model->get_personalization($this->data['getCat'][1]['iCategoryId']);
		
		//echo '<pre>';print_r($this->personalization_model->get_personalization());
		$this->data['tpl_name']= "admin/personalization/view_personalization.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}
	
	function add(){
		$this->data['menuAction'] = 'style';
		if($this->input->post()){
			$personaData = $this->input->post('data');
			$catName = $personaData['catName'];
			$vStyle = $personaData['per']['vStyle'];
			$files = $_FILES;
			
			foreach ($vStyle as $key => $value){
				$catArr['iCategoryId'] = $catName;
				$catArr['vStyle'] = $value;
				$iPersonalId = $this->personalization_model->save_personalize($catArr);
				
				$folderId = $catName;
				$image = $key;
				$imageName = $this->do_upload($folderId,$image,$iPersonalId);
				$imgData['iPersonalId'] = $iPersonalId;
				$imgData['vImage'] = $imageName;
				$this->personalization_model->updata_image_personalize($imgData);
			}
			redirect($this->data['admin_url'].'personalization');
		}
		
		$this->data['getCat'] = $this->personalization_model->get_category();
		$this->data['tpl_name']= "admin/personalization/add_personalization.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}
	
	function edit(){
		$this->data['menuAction'] = 'style';
		$iPersonalId = $this->input->get('iPersonalId');
		
		if($this->input->post()){
			$personaData = $this->input->post('data');
			// update
			$this->personalization_model->update_personalize($personaData);
			
			if($_FILES['vImage']['name']){
				// unlink
				$getOldImg = $this->personalization_model->get_personalization_by_id($iPersonalId);
				$oldImgName = $getOldImg['vImage'];
				$this->unlink_img($oldImgName,$iPersonalId,$getOldImg['iCategoryId']);
				//exit;
				$folderId = $personaData['iCategoryId'];
				$image = 'vImage';
				$imageName = $this->do_upload($folderId,$image,$iPersonalId);
				$imgData['iPersonalId'] = $iPersonalId;
				$imgData['vImage'] = $imageName;
				$this->personalization_model->updata_image_personalize($imgData);	
			}
			
			redirect($this->data['admin_url'].'personalization');
		}
		
		$this->data['getCat'] = $this->personalization_model->get_category();
		$this->data['personaData'] = $this->personalization_model->get_personalization_by_id($iPersonalId);
		$this->data['tpl_name']= "admin/personalization/edit_personalization.tpl";
		$this->data['message'] = $this->session->flashdata('message');  
		$this->smarty->assign('data', $this->data); 
	 	$this->smarty->view('admin/admin_template.tpl');  
	}
	
	function do_upload($iImageId,$image,$imgName)
	{
		if(!is_dir('uploads/personalization/')){
			@mkdir('uploads/personalization/', 0777);
		}
		if(!is_dir('uploads/personalization/'.$iImageId)){
			@mkdir('uploads/personalization/'.$iImageId, 0777);
		}
		if(!is_dir('uploads/personalization/'.$iImageId.'/'.$imgName)){
			@mkdir('uploads/personalization/'.$iImageId.'/'.$imgName, 0777);
		}
		
		$path = $_FILES[$image]['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		$newFileName = $imgName.'.'.$ext;
		$config = array(
			'allowed_types' => "gif|GIF|JPG|jpg|JPEG|jpeg|PNG|png|TIF|tif",
			'upload_path' => 'uploads/personalization/'.$iImageId.'/'.$imgName,
			'file_name' => str_replace(' ','',$newFileName),
			'max_size'=>5380334
		);
		$this->upload->initialize($config);
		$this->upload->do_upload($image); //do upload
		$image_data = $this->upload->data(); //get icon data
		
		$image_data = $this->upload->data(); //get image data
        $config1 = array(
			'source_image' => $image_data['full_path'], //get original image
			'new_image' => 'uploads/personalization/'.$iImageId.'/'.$imgName.'/104x354_'.$image_data['file_name'], //save as new image //need to create thumbs first
			'maintain_ratio' => false,
			'width' => 104,
			'height' => 354
		);
		
		$this->image_lib->initialize($config1);
		$this->image_lib->resize(); //do whatever specified in config
		
		$basePath = $this->data['base_upload'].'personalization/'.$iImageId.'/'.$imgName.'/'.$newFileName;
		
		$vals = @getimagesize($basePath);
		$imgWidth = $vals[0];
		$imgHeight = $vals[1];
		
		$config2 = array(
			'image_library' => 'ImageMagick',
			'library_path' => '/usr/bin/',
			'source_image' => $image_data['full_path'], //get original image
			'new_image' => 'uploads/personalization/'.$iImageId.'/'.$imgName.'/250x250_'.$image_data['file_name'], //save as new image //need to create thumbs first
			'maintain_ratio' => false,
			'width' => 250,
			'height' => 250,
			'x_axis' => 0, //(intval($imgWidth) / 2) - (250 / 2)
			'y_axis' => 0
		);
		
		$this->image_lib->initialize($config2);
		$this->image_lib->crop(); //do whatever specified in config
		
		$img_uploaded = $image_data['file_name'];
		return $img_uploaded;
	}
	
	function unlink_img($imgName,$iImageId,$folderId){
		unlink($this->data['base_upload'].'personalization/'.$folderId.'/'.$iImageId.'/250x250_'.$imgName);
		unlink($this->data['base_upload'].'personalization/'.$folderId.'/'.$iImageId.'/104x354_'.$imgName);
		return unlink($this->data['base_upload'].'personalization/'.$folderId.'/'.$iImageId.'/'.$imgName);
	}
	
	function get_personalization_listing(){
		$all_user = $this->user_model->get_user();
		if(count($all_user) > 0)
		{
			foreach ($all_user as $key => $value)
			{
				$alldata[$key]['iUserId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iUserId'].'">';
				$alldata[$key]['vFirstName'] = $value['vFirstName'];
				$alldata[$key]['vLastName'] = $value['vLastName'];
				$alldata[$key]['vEmail'] = $value['vEmail'];
			}        
			$aData['aaData'] =  $alldata;
		}else{
			$aData['aaData'] = '';
		}
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}
	
	
	/*function deleteicon()
	{
		$upload_path = $this->config->item('base_upload');
		$iPersonalId = $_REQUEST['id'];
		$data = $this->personalization_model->get_personalization_details($iPersonalId);
		$iCategoryId =$data['iCategoryId'];
		$var = unlink($upload_path.'personalization/'.$iCategoryId.'/'.$iPersonalId.'/250x250_'.$data['vImage']);
		$var = unlink($upload_path.'personalization/'.$iCategoryId.'/'.$iPersonalId.'/104x354_'.$data['vImage']);
		if($var)
		{

			$var = $this->personalization_model->delete_image($iPersonalId);
			
		}
		$var = $this->personalization_model->delete_record($iPersonalId);
		redirect($this->data['admin_url'] . 'personalization/edit?iPersonalId='.$iPersonalId); 
		// exit;
	}*/

	function action_update(){
			$this->data['menuAction'] = 'style';
			$iPersonalId = $_REQUEST['id'];
			$data = $this->personalization_model->get_personalization_details($iPersonalId);
			$iCategoryId =$data['iCategoryId'];
			$upload_path = $this->config->item('base_upload');
			
			$var = unlink($upload_path.'personalization/'.$iCategoryId.'/'.$iPersonalId.'/250x250_'.$data['vImage']);
			$var = unlink($upload_path.'personalization/'.$iCategoryId.'/'.$iPersonalId.'/104x354_'.$data['vImage']);
			if($var)
			{

				$var = $this->personalization_model->delete_record($iPersonalId);
			
			}
			redirect($this->data['admin_url'] . 'personalization'); 
			/*$tableData['tablename']='personalization';
			$tableData['update_field']='iPersonalId';
			if($action=='Delete'){
				$count=count($iPersonalId);
				foreach ($iPersonalId as $row){		            	
					$data= $this->personalization_model->delete_record($row,$tableData);		            	
				}
				$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
				redirect($this->data['admin_url'] . 'personalization'); 
			}else{
				$count=$count;
				$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
				redirect($this->data['admin_url'] . 'personalization'); 
			}*/
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
?>