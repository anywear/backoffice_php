<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class money_spend extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/money_spend_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->data['menuAction'] = 'money_spend';
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/money_spend/view-money-spend.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_money_spend_listing(){
		$all_store=$this->money_spend_model->get_all_money_spend();
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iSpendId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iSpendId'].'">';
                $alldata[$key]['vTitle'] = $value['vTitle'];
                $alldata[$key]['eSign'] = $value['eSign'];
                $alldata[$key]['iSorting'] = $value['iSorting'];
                $alldata[$key]['eStatus'] = $value['eStatus'];
                $alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'money_spend/update/'.$value['iSpendId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="#" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'money_spend';
		$eSign = field_enums('money_spend','eSign');
		$totalRec = $this->money_spend_model->count_all();
		$this->data['totalRec']=$totalRec['tot'];
		$this->data['initOrder']=1;
		if($this->input->post()){
			$price = $this->input->post('money_detail');
			$iSpendId = $this->money_spend_model->add_money_spend($price);
			if($iSpendId){
				$this->session->set_flashdata('message',"Price added successfully");
				redirect($this->data['admin_url'].'money_spend');
			}else{
				$this->session->set_flashdata('message',"Price added successfully");
				redirect($this->data['admin_url'].'money_spend');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Size', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/money_spend/create-money-spend.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eSign', $eSign);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'money_spend';
		$iSpendId = $this->uri->segment(4);
		$this->data['size'] = $this->money_spend_model ->get_size_details($iSpendId);
		$eStatus = field_enums('money_spend','eStatus');
		$eSign = field_enums('money_spend','eSign');
		$totalRec = $this->money_spend_model->count_all();
		$this->data['totalRec']=$totalRec['tot'];
		$this->data['initOrder']=1;
		// echo "<pre>";print_r($this->data);exit;
		if($this->input->post()){
			$data = $this->input->post('money_detail');
			$data['iSpendId'] = $this->input->post('iSpendId');
			$id = $this->money_spend_model->update($data['iSpendId'],$data);
			if($data['iSpendId']){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'money_spend');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'money_spend');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/money_spend/edit-money-spend.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->assign('eSign', $eSign);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='money_spend';
		$tableData['update_field']='iSizeId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->money_spend_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'money_spend'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->money_spend_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'money_spend'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->money_spend_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'money_spend'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'money_spend'); 
		}
	}

	
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


