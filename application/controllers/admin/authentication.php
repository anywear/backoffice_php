<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends Admin_Controller{
    
    function __construct(){
        parent::__construct();    
        $this->load->model('admin/authentication_model', '', TRUE);        
        $this->load->model('admin/loginlogs_model', '', TRUE);
        $this->load->model('common_model','', TRUE);
        
        $this->smarty->assign("data",$this->data);
        $this->smarty->assign("Name","Welcome To Flip Book Admin Panel");
    }    
    
    
    public function index(){
        
         $this->data['message'] = $this->session->flashdata('message');
        if(isset($this->session->userdata['anywear_admin_info'])){            
            redirect($this->data['base_url'] . 'admin/home');          
        }
        else if($this->input->post()){
            $vEmail = $this->input->post('vEmail');
            $vPassword=md5($this->input->post('vPassword'));             
            // echo "<pre>";print_r($vPassword);exit;
            $auth_exists = $this->authentication_model->check_auth($vEmail,$vPassword);
            if($auth_exists){
                $auth_exists['logged_in'] = TRUE;
                $datestring = "%Y-%m-%d  %h:%i:%s";                
                $time = time();                
                $dLoginDate = mdate($datestring, $time);                
                $logindata['iAdminId'] = $auth_exists['iAdminId'];
                $logindata['vFirstName'] = $auth_exists['vFirstName'];
                $logindata['vLastName'] = $auth_exists['vLastName'];
                $logindata['vEmail'] = $auth_exists['vEmail'];
                $logindata['vIP'] = $this->input->ip_address();
                $logindata['dLoginDate'] = $dLoginDate;    
                $iLoginLogId = $this->loginlogs_model->loginentry($logindata);                            
                $auth_exists['iLoginLogId'] = $iLoginLogId;                
                $this->session->set_userdata('anywear_admin_info', $auth_exists);
                redirect($this->data['base_url'] . 'admin/user');
                exit;
            }
            else{
                $this->session->set_flashdata('message',"Sorry , Username or Password is wrong !");
                redirect($this->data['admin_url'] . 'authentication');
                exit;
            }
        }
        else{            
            $this->data['tpl_name']= "admin/login.tpl";
            $this->smarty->assign('data', $this->data);        
            $this->smarty->view( 'admin/login.tpl');    
        }
    }
    
    function logout(){
        $datestring = "%Y-%m-%d  %h:%i:%s";
        $time = time();        
        $dLogoutDate = mdate($datestring, $time);
        $logindata['iLoginLogId'] = $this->data['anywear_admin_info']['iLoginLogId'];
        $logindata['dLogoutDate'] = $dLogoutDate;       
        $this->session->unset_userdata("anywear_admin_info");        
        redirect($this->data['admin_url'] . 'authentication');
        exit();
    }    
}

/* End of file authentication.php */
/* Location: ./application/controllers/authentication.php */


