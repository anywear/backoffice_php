<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class size extends Admin_Controller{
	
	function __construct(){
		parent::__construct();
			$this->load->model('admin/size_model', '', TRUE);
			$this->load->library('image_lib'); //load library
			$this->load->helper('url');
			$this->load->library('upload');
			$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iAdminId']);
			if(! isset($this->session->userdata['anywear_admin_info'])){
				redirect($this->data['admin_url'].'authentication');
				exit ; 
			}		
			$this->smarty->assign("data",$this->data);
	}
	
	function index(){
		$this->data['menuAction'] = 'size_master';
		$this->breadcrumb->add('Dashboard', "");
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['paging_message']  = 'No Records Found';
		$this->data['tpl_name']= "admin/size/view-size.tpl";
		$this->smarty->assign('data', $this->data);
		$this->smarty->view('admin/admin_template.tpl'); 
	}

	function all_size_listing(){
		$all_store=$this->size_model->get_all_size();
		if(count($all_store) > 0){ 
			foreach ($all_store as $key => $value){
				$alldata[$key]['iSizeId'] = '<input type="checkbox" name="iId[]" id="iId" value="'.$value['iSizeId'].'">';
                $alldata[$key]['vSizeTitle'] = $value['vSizeTitle'];
                $alldata[$key]['eSizeType'] = $value['eSizeType'];
                $alldata[$key]['iSorting'] = $value['iSorting'];
                $alldata[$key]['eStatus'] = $value['eStatus'];
                $alldata[$key]['editlink'] = '<a href="'.$this->data['admin_url'].'size/update/'.$value['iSizeId'].'" class="btn btn-default" title=Edit><span class="fa fa-pencil"></span></a><a href="'.$this->data['admin_url'].'size/size_delete?iSizeId='.$value['iSizeId'].'" class="btn btn-default" title=Delete style=margin-left:10px;>Delete</a>';
			}
			$aData['aaData'] =  $alldata;
		}
		else
		{
			$aData['aaData'] = '';
		}	
		$json_lang = json_encode($aData);
		echo $json_lang;exit;
	}

	function create(){
		$this->data['menuAction'] = 'size_master';
		$eSizeType = field_enums('global_size','eSizeType');
		$totalRec = $this->size_model->count_all();
		$this->data['totalRec']=$totalRec['tot'];
		$this->data['initOrder']=1;
		if($this->input->post()){
			$size = $this->input->post('size_detail'); 
			$iSizeId = $this->size_model->add_size($size);
			if($iStoreId){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'].'size');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'].'size');
			}
			exit;
	    }
		
		$this->breadcrumb->add('Home', $this->data['admin_url'].'home');
		$this->breadcrumb->add('Add Size', '');
		$this->data['function']='create';
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['tpl_name']= "admin/size/create-size.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eSizeType', $eSizeType);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function update(){
		$this->data['menuAction'] = 'size_master';
		$iSizeId = $this->uri->segment(4);
		$this->data['size'] = $this->size_model ->get_size_details($iSizeId);
		$eStatus = field_enums('global_size','eStatus');
		$eSizeType = field_enums('global_size','eSizeType');
		$totalRec = $this->size_model->count_all();
		$this->data['totalRec']=$totalRec['tot'];
		$this->data['initOrder']=1;
		// echo "<pre>";print_r($this->data);exit;
		if($this->input->post()){
			$data = $this->input->post('size_detail');
			$data['iSizeId'] = $this->input->post('iSizeId');
			$id = $this->size_model->update($data['iSizeId'],$data);
			if($data['iSizeId']){
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'size');
			}else{
				$this->session->set_flashdata('message',"Size added successfully");
				redirect($this->data['admin_url'] . 'size');
			}
			exit;
		}   
		$this->data['tpl_name']= "admin/size/edit-size.tpl";   
		$this->smarty->assign('data', $this->data);
		$this->smarty->assign('eStatus', $eStatus);
		$this->smarty->assign('eSizeType', $eSizeType);
		$this->smarty->view('admin/admin_template.tpl');
	}

	function action_update(){
		$this->data['menuAction'] = 'size_master';
		$ids = $this->input->post('iId');
		$action=$this->input->post('action');
		$tableData['tablename']='global_size';
		$tableData['update_field']='iSizeId';
		if($action=='Delete'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->size_model->delete_slider_record($row,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'size'); 
		}else if($action=='Active'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->size_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'size'); 
		}else if($action=='Inactive'){
			$count=count($ids);
			foreach ($ids as $row){		            	
				$data= $this->size_model->get_update_all($row,$action,$tableData);		            	
			}
			$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
			redirect($this->data['admin_url'] . 'size'); 
		}else{
			$count=$count;
			$this->session->set_flashdata('message',"Total  ($count)  Record updated successfully");
			redirect($this->data['admin_url'] . 'size'); 
		}
	}

	function size_delete(){
		$ids = $this->input->get('iSizeId');
		$data= $this->size_model->delete_record($ids);
		$this->session->set_flashdata('message',"Total  ($count)  Record Delete successfully");
		redirect($this->data['admin_url'] . 'size'); 
		
	}
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */


