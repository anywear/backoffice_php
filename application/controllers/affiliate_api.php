<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Affiliate_api extends Admin_Controller {
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('webservice_new_model', '', TRUE);
	}   

	public function index()
	{
		$action = $this->input->get('action');
		if($action == 'setRetailers'){
			$this->getRetailers();
		}else if($action == 'setBrands'){
			$this->getBrands();
		}else if($action == 'setCategories'){
			$this->categories();
		}else if($action == 'setProducts'){
			$this->getProducts();
		}else if($action == 'setShopSense'){
			$this->getRetailers();
			$this->getBrands();
			$this->categories();
		}
	}

	
	function getRetailers(){
		
		$url="http://api.shopstyle.com/api/v2/retailers?pid=uid4225-25554185-42";
		$data= $this->curl->simple_get($url);
		$result = json_decode($data,true);
		//echo '<pre>';print_r($result);exit;
		//$result['retailers'][351]['id'] = 1853;
		//$result['retailers'][351]['name'] = 'hardik';
		//$result['retailers'][351]['deeplinkSupport'] = 1;
		//$result['retailers'][351]['hostDomain'] = 'hardik.com';
		
		for($i=0;$i<count($result['retailers']);$i++){
			
			//$Data['iRetailerId']= $result['retailers'][$i]['id'];
			$Data['vStoreName']= $result['retailers'][$i]['name'];
			$Data['eDeeplinkSupport']= $result['retailers'][$i]['deeplinkSupport'];
			$Data['vHostDomain']= $result['retailers'][$i]['hostDomain'];
			$Data['eDataFrom']= 'shopstyle';
			$Data['dCurrentdate']= date('Y-m-d h:i:s');;
			
			$DatavName = $this->webservice_new_model->get_retailer_data($Data['vStoreName']);
			
			if($DatavName > 0){
				$message['success'] = 'Error';
			}else{
				$iRetailerId = $this->webservice_new_model->add_retailer($Data);	
			}
		}	
		
		if($iRetailerId)
		{
			$message['success'] = 'Shop data added succesfully';
			
		}else{
			$message['success'] = 'Shop data updated succesfully';
		}
		
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($message);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	
	function getBrands(){
		
		$url="http://api.shopstyle.com/api/v2/brands?pid=uid4225-25554185-42";
		$data= $this->curl->simple_get($url);
		$result = json_decode($data,true);
		//echo '<pre>';print_r($result);exit;
		for($i=0;$i<count($result['brands']);$i++){
			
			//$Data['iBrandId']= $result['brands'][$i]['id'];
			$Data['vName']= $result['brands'][$i]['name'];
			$Data['tSynonyms']= implode(",", $result['brands'][$i]['synonyms']);
			$Data['eDataFrom']= 'shopstyle';
			
			$DatavName = $this->webservice_new_model->get_brands_data($Data['vName']);
			
			if($DatavName > 0){
				$message['success'] = 'Error';
			}else{
				$iBrandId = $this->webservice_new_model->add_brands($Data);	
			}
		}	
		
		if($iBrandId)
		{
			$message['success'] = 'Brand data added succesfully';
			
		}else{
			$message['success'] = 'Brand data updated succesfully';
		}
		
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($message);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function categories(){
		
		$url="http://api.shopstyle.com/api/v2/categories?pid=uid4225-25554185-42";
		$data= $this->curl->simple_get($url);
		$result = json_decode($data,true);
		for($i=0;$i<count($result['categories']);$i++){
			
			$Data['vProductCatId'] = $result['categories'][$i]['id'];
			$Data['vName'] = $result['categories'][$i]['name'];
			$Data['vShortName'] = $result['categories'][$i]['shortName'];
			$Data['vLocalizedId'] = $result['categories'][$i]['localizedId'];
			$Data['vParentId'] = $result['categories'][$i]['parentId'];
			$Data['eHasHeelHeightFilter'] = $result['categories'][$i]['hasHeelHeightFilter'];
			$Data['eHasColorFilter'] = $result['categories'][$i]['hasSizeFilter'];
			$Data['eDataFrom'] = 'shopstyle';
			
			$DatavName = $this->webservice_new_model->get_categories_data($Data['vName']);
			
			if($DatavName > 0){
				$message['success'] = 'Error';
			}else{
				$vProductCatId = $this->webservice_new_model->add_categories($Data);	
			}
		}	
		
		if($vProductCatId)
		{
			$message['success'] = 'Product categories added succesfully';
			
		}else{
			$message['success'] = 'Product categories updated succesfully';
		}
		
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($message);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function getProducts(){
		
		$url="http://api.shopstyle.com/api/v2/products?pid=uid4225-25554185-42";
		
		$productData= $this->curl->simple_get($url);
		$result = json_decode($productData,true);
		$products = $result['products'];
		echo '<pre>';print_r($result['products']);exit;
		foreach ($products as $key => $value){
			// temp solution
			/*$value = array();
			$value = $products;*/
			// temp solution
			
			$iStoreId = $this->webservice_new_model->getStoreId($value['retailer']['name']);
			$iBrandId = $this->webservice_new_model->getBrandId($value['brand']['name']);
			$data['iStoreId'] = ($iStoreId['iStoreId'])? $iStoreId['iStoreId'] : 0 ;
			$data['iBrandId'] = ($iBrandId['iBrandId'])? $iBrandId['iBrandId'] : 0 ;
			
			$data['vName'] = $value['name'];
			$data['vBrandedName'] = $value['brandedName'];
			$data['vUnbrandedName'] = $value['unbrandedName'];
			$data['vCurrency'] = $value['currency'];
			$data['fPrice'] = $value['price'];
			$data['iInStock'] = $value['inStock'];
			$data['vLocale'] = $value['locale'];
			$data['tDescription'] = $value['description'];
			$data['dExtractDate'] = $value['extractDate'];
			$data['dAddedDate'] = date('Y-m-d H:i:s');
			$data['eStatus'] = 'Active';
			$data['eDataFrom'] = 'shopstyle';
			
			// check If Product exist
			$ifProductExist = $this->webservice_new_model->check_product($data['vName']);
			//echo '<pre>';print_r($ifProductExist);exit;
			if(count($ifProductExist) <= 0){
				// products entry
				$iProductId = $this->webservice_new_model->setProductData($data);
				
				// upload main product image
				$vImages = $value['image']['sizes'];
				foreach ($vImages as $imageKey => $imageValue){
					$url = $imageValue['url'];
					$newFileName = end(explode("/",strtolower(basename($url))));
					$fileurl = $this->data['base_upload'].'products/'.$iProductId.'/'.$imageValue['sizeName'].'_'.$newFileName;
			   	 	$resultFile = $this->upload_image($iProductId,$url,$fileurl);
			   	 	
			   	 	// save original image
			   	 	if($imageValue['sizeName'] == 'Original'){
			   	 		$vImage['vImage'] = $imageValue['sizeName'].'_'.$newFileName;
			   	 		$this->webservice_new_model->updateImage($iProductId,$vImage);
			   	 	}
				}
				
				$alternateImages = $value['alternateImages'];
				foreach ($alternateImages as $altImageMainKey => $altImageMainVallue){
					//upload_gallery_image
					$vAltImages = $altImageMainVallue['sizes'];
					$altImageData['iProductId'] = $iProductId;
					$iProductGalleryId = $this->webservice_new_model->saveAltImage($altImageData);
					foreach ($vAltImages as $altImageKey => $altImageValue){
						$url = $altImageValue['url'];
						$newFileName = end(explode("/",strtolower(basename($url))));
						$fileurl = $this->data['base_upload'].'products/image_gallery/'.$iProductGalleryId.'/'.$altImageValue['sizeName'].'_'.$newFileName;
				   	 	$resultFile = $this->upload_gallery_image($iProductGalleryId,$url,$fileurl);
					}
				}
				
				// Product category relation entry
				$productCategories = $value['categories'];
				foreach ($productCategories as $catKey => $catValue){
					$catId = $this->webservice_new_model->getCatId($catValue['id']);
					$productCategoryData['iProductCatId'] = $catId['id'];
					$productCategoryData['iProductId'] = $iProductId;
					$this->webservice_new_model->setProductCategoryData($productCategoryData);
				}
				
				// colors entry
				$colorsData = $value['colors'];
				if(!empty($colorsData)){
					foreach ($colorsData as $colorKey => $colorValue ){
						$insetColorParentData['iParentId'] = 0;
						$insetColorParentData['vColorName'] = $colorValue['name'];
						
						$ifColorExist = $this->webservice_new_model->check_colors($colorValue['name']);
						if(count($ifColorExist) <= 0){
							$iColorParentId = $this->webservice_new_model->setColorData($insetColorParentData);
						}else{
							$iColorParentId = $ifColorExist['iProductColorId'];
						}
						
						// canonical Colors (sub colors entry)
						$canonicalColors = $colorValue['canonicalColors'];
						if(!empty($canonicalColors)){
							foreach ($canonicalColors as $subColorKey => $subColorValue ){
								$insetSubColorParentData['iParentId'] = $iColorParentId;
								$insetSubColorParentData['vColorName'] = $subColorValue['name'];
								
								$ifSubColorExist = $this->webservice_new_model->check_colors($subColorValue['name']);
								
								if(count($ifSubColorExist) <= 0){
									$iSubColorParentId = $this->webservice_new_model->setColorData($insetSubColorParentData);
								}
							}
						}
						
						if($iColorParentId){
							$insertColorRelationData['iProductId'] = $iProductId;
							$insertColorRelationData['iProductColorId'] = $iColorParentId;
							$iColorRelationId = $this->webservice_new_model->setColorRelationData($insertColorRelationData);
						}
					}
				}
				
				// sizes entry
				$sizesData = $value['sizes'];
				if(!empty($sizesData)){
					foreach ($sizesData as $sizeKey => $sizeValue ){
						$insetSizeParentData['iParentId'] = 0;
						$insetSizeParentData['vSizeName'] = $sizeValue['name'];
						
						$ifSizeExist = $this->webservice_new_model->check_sizes($sizeValue['name']);
						if(count($ifSizeExist) <= 0){
							$iSizeParentId = $this->webservice_new_model->setSizeData($insetSizeParentData);
						}else{
							$iSizeParentId = $ifSizeExist['iProductSizeId'];
						}
						
						// canonical Sizes (sub sizes entry)
						$canonicalSizes = $sizeValue['canonicalSize'];
						if(isset($sizeValue['canonicalSize'])){
							$insetSubSizeData['iParentId'] = $iSizeParentId;
							$insetSubSizeData['vSizeName'] = $sizeValue['canonicalSize']['name'];
							
							$ifSubSizeExist = $this->webservice_new_model->check_sizes( $sizeValue['canonicalSize']['name']);
							
							if(count($ifSubSizeExist) <= 0){
								$iSubSizeParentId = $this->webservice_new_model->setSizeData($insetSubSizeData);
							}
						}
						
						if($iSizeParentId){
							$insertSizeRelationData['iProductId'] = $iProductId;
							$insertSizeRelationData['iProductSizeId'] = $iSizeParentId;
							$iSizeRelationId = $this->webservice_new_model->setSizeRelationData($insertSizeRelationData);
						}
					}
				}
			}else{
				// update
				//$this->webservice_new_model->update_product($ifProductExist['iProductId'],$data);
				//$iProductId = $ifProductExist['iProductId'];
			}
		}
		
		if($iProductId)
		{
			$message['success'] = 'Products added succesfully';
			
		}else{
			$message['success'] = 'Products updated succesfully';
		}
		
		header('Content-type: application/json');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		$main = json_encode($message);
		ob_clean();
		echo $callback . '('.$main.');';
		exit;
	}
	
	function upload_image($iProductId,$url,$fileurl){
		
		if(!is_dir('uploads/products/')){
			@mkdir('uploads/products/', 0777);
		}
		if(!is_dir('uploads/products/'.$iProductId)){
			@mkdir('uploads/products/'.$iProductId, 0777);
		}
		
		
	    $ch = curl_init($url);
		$fp = fopen($fileurl, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_exec($ch);
		curl_close($ch);
		$newfile = fclose($fp);
		return $newfile;
	}
	
	function upload_gallery_image($iProductGalleryId,$url,$fileurl){
		
		if(!is_dir('uploads/products/image_gallery/')){
			@mkdir('uploads/products/image_gallery/', 0777);
		}
		if(!is_dir('uploads/products/image_gallery/'.$iProductGalleryId)){
			@mkdir('uploads/products/image_gallery/'.$iProductGalleryId, 0777);
		}
		
	    $ch = curl_init($url);
		$fp = fopen($fileurl, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_exec($ch);
		curl_close($ch);
		$newfile = fclose($fp);
		return $newfile;
	}
	
}
/* End of file webservice.php */
/* Location: ./application/controllers/webservice.php */