<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// Use for extends COMMON CONTROLLER IN FRONT SIDE and ADMIN SIDE
class Common_Libraries extends CI_Controller {
	
	function __construct(){
		
    	parent::__construct();
	
	$this->load->helper('cookie');	
        $this->load->library('session');
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('common_func');
        $this->load->helper('global_variable');
        $this->load->database();
        $this->load->library('curl');
        $this->load->library('email');
        $this->load->helper('file');	   
        $this->load->library('upload'); 
        $this->load->library('image_lib'); //load library
        $this->load->model('common_model');
	   
        getGeneralVar();
        $this->data=$GLOBALS['Configration_value'];		
        $this->data['base_CK']  = $this->config->item('base_CK');
        $this->data['base_url']  = $this->config->item('base_url');
        $this->data['fancy']  = $this->config->item('front_fancybox');
        $this->data['base_upload']  = $this->config->item('base_upload');
        $this->data['base_path']    = $this->config->item('base_path');
        $this->data['base_lib']     = $this->config->item('base_lib');
        $this->data['userdata'] = $this->session->all_userdata();
    }

    function rand_number(){
        $rnd1 = rand(0,99);
        return $rnd1 ;
    }

}
 

// Use common controller in admin side
class Admin_Controller extends Common_Libraries {
 	function __construct(){
		parent::__construct();
		$this->load->library('Breadcrumb');
		$this->data['admin_url']= $this->config->item('admin_url');     
		$this->data['admin_css_path']=$this->config->item('admin_css_path');        
		$this->data['admin_image_path']=$this->config->item('admin_image_path');
		$this->data['admin_js_path']=$this->config->item('admin_js_path');
		$this->data['admin_boostrap']=$this->config->item('admin_boostrap');
		$this->data['anywear_admin_info']=$this->session->userdata['anywear_admin_info'];
		$this->data['admin_base_url']= $this->config->item('base_url');
		$this->data['base_url']     = $this->config->item('base_url');
		$this->data['admindetail'] = $this->common_model->get_admin_details($this->data['anywear_admin_info']['iUserId']);
		$this->data['vCityName'] = $this->common_model->getcity();
        // echo "<pre>";print_r($this->data);exit;
        $this->smarty->assign('data', $this->data);
	}
}
?>