<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 23:12:24
         compiled from "application/views/templates/admin/home/view-store-detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152895243953faca34a5cf95-76110289%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ed9979ab64ee6a4c2fe3ca8c49da8e3732fa0c7b' => 
    array (
      0 => 'application/views/templates/admin/home/view-store-detail.tpl',
      1 => 1409328741,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152895243953faca34a5cf95-76110289',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53faca34c80697_98280525',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53faca34c80697_98280525')) {function content_53faca34c80697_98280525($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
list_store.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Store List</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Store DataTables Advanced <?php if ($_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['eOnSale']=='Yes'){?>
                <button type="button" class="btn btn-success" style="margin-left:30px;">On Sale</button><?php }?>
                <div class="pull-right">
                    <!-- <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="span12">
                                <div class="block">
                                    <div class="block-content collapse in">
                                        <!--  -->
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Name of Store </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vStoreName'];?>

                                            </div>
                                        </div>
                                         <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Style </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vStyle'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Description </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['tDescription'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Opening Hour </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vOpeningHour'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Phone Number </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vPhoneNumber'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Address </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['tAddress'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Area </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vArea'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Sizes </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vSize'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">PriceAvr </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vPriceAvr'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Looking for </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vLookingFor'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Want to Buy </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vWantToBuy'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">FB Datasource </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vFBDataSource'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Instagram Datasource </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vInstagramSource'];?>

                                            </div>
                                        </div>

                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Main Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vMainImage']!=''){?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['iStoreId'];?>
/MainImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vMainImage'];?>
" width="90%;" style="margin:0 0 0 0px;"/>
                                            <?php }?>
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Small Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vSmallIamge']!=''){?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['iStoreId'];?>
/SmalImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vSmallIamge'];?>
" width="90%;" style="margin:0 0 0 0px;" />
                                            <?php }?>
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Shop Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vShopImage']!=''){?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['iStoreId'];?>
/ShopImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['vShopImage'];?>
" width="90%;" style="margin:0 0 0 0px;"/>
                                            <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
<?php }} ?>