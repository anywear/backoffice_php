<?php /* Smarty version Smarty-3.1.11, created on 2014-09-03 12:56:27
         compiled from "application/views/templates/admin/left.tpl" */ ?>
<?php /*%%SmartyHeaderCode:105767598153f8ac8450d315-48041289%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18abc6e9a7a22a10e7af0270890cc9e369caa1fd' => 
    array (
      0 => 'application/views/templates/admin/left.tpl',
      1 => 1409723783,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '105767598153f8ac8450d315-48041289',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ac845174d2_84328576',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ac845174d2_84328576')) {function content_53f8ac845174d2_84328576($_smarty_tpl) {?><div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
        	<li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='user'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
user">Users</a>
            </li>
            <li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='slider'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
slider">App Home Slider</a>
            </li>
            <li class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='store'||$_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_categories'||$_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_brand'||$_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_size'||$_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_color'||$_smarty_tpl->tpl_vars['data']->value['menuAction']=='store_style_looking'){?>active<?php }?>">
            	<a href="#" class="">Stores<span class="fa arrow"></span></a>
            	<ul class="nav nav-second-level">
                    <!--<li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='store_category'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store_category">Category</a>
                    </li>-->
            		<li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='store'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store">Store</a>
                    </li>
                    <li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_categories'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_categories">Category</a>
                    </li>
                    <li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_brand'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_brand">Brand</a>
                    </li>
                    
                    <li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_size'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_size">Sizes</a>
                    </li>
                    <li>
                        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='product_color'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_color">Color</a>
                    </li>
                    <li>
				        <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='store_style_looking'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
style_looking">Style Looking For</a>
				    </li>
                </ul>
                <!--<a class="active" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
home"><i class="fa fa-dashboard fa-fw"></i> Stores</a>-->
            </li>
            
            <li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='style'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization">Personalization</a>
            </li>
            <li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='size_master'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size">Size Master</a>
            </li>
            <li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='money_spend'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
money_spend">Money Spend On</a>
            </li>
            <li>
                <a class="<?php if ($_smarty_tpl->tpl_vars['data']->value['menuAction']=='notification'){?>active<?php }?>" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
notification">Notification</a>
            </li>
            <!--<li>
                <a class="" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
style_looking"><i class="fa fa-sliders fa-fw"></i>Style Looking</a>
            </li>-->
            <!--<li>
                <a class="" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store_category"><i class="fa fa-sliders fa-fw"></i>Store Category</a>
            </li>-->
            <!--<li>
                <a class="" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store_size"><i class="fa fa-sliders fa-fw"></i>Size</a>
            </li>-->
        </ul>
    </div>
</div><?php }} ?>