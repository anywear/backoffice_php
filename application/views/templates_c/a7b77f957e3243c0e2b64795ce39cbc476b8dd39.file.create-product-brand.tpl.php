<?php /* Smarty version Smarty-3.1.11, created on 2014-09-08 18:34:10
         compiled from "application/views/templates/admin/product_brand/create-product-brand.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1213706645540038ee82ed15-19701929%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7b77f957e3243c0e2b64795ce39cbc476b8dd39' => 
    array (
      0 => 'application/views/templates/admin/product_brand/create-product-brand.tpl',
      1 => 1410176042,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1213706645540038ee82ed15-19701929',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_540038ee8aa298_21195751',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540038ee8aa298_21195751')) {function content_540038ee8aa298_21195751($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
product-brand.js"></script>
<div class="row">
    <div class="navbar">
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Brand
                <div class="pull-right">
                    <div class="btn-group"></div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_brand/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
				<fieldset>
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Name</label>
					<input type="text" class="form-control size-style-dropdawn" id="vName" name="data[vName]" value="" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Synonyms</label>
					<input type="text" class="form-control size-style-dropdawn" id="tSynonyms" name="data[tSynonyms]" value="" required>
				    </div>
				    
				    <input type="hidden" name="data[eDataFrom]" value="manual">
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
<?php }} ?>