<?php /* Smarty version Smarty-3.1.11, created on 2014-09-08 18:14:50
         compiled from "application/views/templates/admin/product_color/create-product-color.tpl" */ ?>
<?php /*%%SmartyHeaderCode:759465885406b13216e387-37178350%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47c4c976fe8b700be74e8067bea6902691c1b109' => 
    array (
      0 => 'application/views/templates/admin/product_color/create-product-color.tpl',
      1 => 1410174502,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '759465885406b13216e387-37178350',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5406b132212ec0_60117023',
  'variables' => 
  array (
    'data' => 0,
    'iParentId' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5406b132212ec0_60117023')) {function content_5406b132212ec0_60117023($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
product_color.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Color
                <div class="pull-right">
                    <div class="btn-group">
                        
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_color/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
				<fieldset>
				    
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                    <label class="control-label" for="typeahead">Select Color</label>
                                        <div class="btn-group size-style-dropdawn">
					    <select name="color[iParentId]" class="form-control" data-toggle="dropdown" >
						<option value="">Select Color</option>
					    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['iParentId']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['iParentId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductColorId'];?>
" <?php if ($_smarty_tpl->tpl_vars['iParentId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['color']['iParentId']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['iParentId']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColorName'];?>
</option>
					    <?php endfor; endif; ?>
					    </select>
                                        </div>
                                    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Name of Color</label>
					    <input type="text" class="form-control " id="vColorName" name="color[vColorName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['color_detail']['vColorName'];?>
" required>
				    </div>

				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	    </div>
	</div>
</div>

<script type="text/javascript">

</script>

<?php }} ?>