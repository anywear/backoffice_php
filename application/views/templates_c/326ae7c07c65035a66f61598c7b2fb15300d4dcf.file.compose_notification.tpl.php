<?php /* Smarty version Smarty-3.1.11, created on 2014-08-23 22:12:20
         compiled from "application/views/templates/admin/notification/compose_notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:171635112253f8af542a9f98-71295621%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '326ae7c07c65035a66f61598c7b2fb15300d4dcf' => 
    array (
      0 => 'application/views/templates/admin/notification/compose_notification.tpl',
      1 => 1408796539,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '171635112253f8af542a9f98-71295621',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8af542dc1e3_49052401',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8af542dc1e3_49052401')) {function content_53f8af542dc1e3_49052401($_smarty_tpl) {?><div class="row" style="width:550px;">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
				<form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
notification/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
					<fieldset>
					<legend><?php echo $_smarty_tpl->tpl_vars['data']->value['label'];?>
 Compose</legend>
						<!--<input type="hidden" name="iAdminId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['iAdminId'];?>
'>-->
						<div class="form-group">
							<div class="controls">
								<input type="text" style="width:85%;margin-left:15px;" class="admin-form" id="vTitle" name="data[vTitle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vTitle'];?>
" placeholder="Title" required>
							</div>
						</div>

						<div class="form-group">
							<div class="controls">
								<textarea class="form-control" style="width:85%;margin-left:15px;" rows="3" id="tDescription" name="data[tDescription]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['tDescription'];?>
" placeholder="One Fine body" required></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-left:5px;">
							<button type="button" class="btn bottom-buffer" onclick="returnme();">Close</button>
							<button type="Submit" value="Send" id="btn-save" class="btn btn-primary" onclick="validate();">Send</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
function validate(){
    if($( "#vFirstName" ).val() ==''){
    	$("#firstnameinput").html( "<p style='margin:5px 0 0 191px;'>Please Enter First Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#firstnameinput").hide();
    }
    if($( "#vLastName" ).val() ==''){
        $("#lastnameinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Last Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#lastnameinput").hide();}
    if($( "#vEmail" ).val() ==''){
        $("#emailinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Email!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#emailinput").hide();}
    if(IsEmail($( "#vEmail" ).val())==false){
        $("#properemail").html( "<p style='margin:5px 0 0 191px;>Please Enter Proper Email Address!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#properemail").hide();}
    if($("#vPassword" ).val() ==''){
        $("#passwordinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Password!</p>" );
        $("#myalert").modal('show');
        return false;
    }
    else{
    	$("#passwordinput").hide();
        $("#frmadmin").submit();
    }
}
function returnme(){
    $.fancybox.close();
}
</script>
<?php }} ?>