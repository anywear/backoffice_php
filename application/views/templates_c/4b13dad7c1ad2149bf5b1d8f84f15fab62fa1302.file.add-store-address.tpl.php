<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 20:15:44
         compiled from "application/views/templates/admin/home/add-store-address.tpl" */ ?>
<?php /*%%SmartyHeaderCode:212211045953fc384b03ea57-16800743%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4b13dad7c1ad2149bf5b1d8f84f15fab62fa1302' => 
    array (
      0 => 'application/views/templates/admin/home/add-store-address.tpl',
      1 => 1409154963,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '212211045953fc384b03ea57-16800743',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fc384b22b530_41250687',
  'variables' => 
  array (
    'data' => 0,
    'sizearray' => 0,
    'size_checked' => 0,
    'pricearray' => 0,
    'price_checked' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc384b22b530_41250687')) {function content_53fc384b22b530_41250687($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
list_store.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Store Data
                <div class="pull-right">
                    <div class="btn-group">
                        <!--<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
home/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
?iStoreId=<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iStoreId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
'>
									<div class="form-group">
										<div class="store-label">
											<label class="control-label" for="typeahead">Name of Store</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vStoreName" name="store_detail[vStoreName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStoreName'];?>
" required>
										</div>
									</div>
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Style</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group style-dropdawn">
												<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" data-all="Vintage,Modern,Slipee,Banana,Evening,Wow">				<option value="">Menu</option>
													<option value="Vintage">Vintage</option>
													<option value="Modern">Modern</option>
													<option value="Slipee">Slipee</option>
													<option value="Banana">Banana</option>
													<option value="Evening">Evening</option>
													<option value="Wow">Wow</option>
													<option class="divider"></option>
													<option value="All" >All</option>
												</select>
											</div> -->
											<input class="form-control addtextstyle" id="vStyle" name="store_detail[vStyle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStyle'];?>
" type="text" required>
										</div>
									</div>
  
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Description</label>
										</div>
										<div class="controls">
											<textarea class="form-control textcss" rows="3" id="tDescription" name="store_detail[tDescription]" value="" required><?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tDescription'];?>
</textarea>
										</div>
									</div>

									<div class="form-group">
									<div class="store-label">
										<label class="control-label" for="typeahead">Opening Hour</label>
									</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vOpeningHour" name="store_detail[vOpeningHour]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vOpeningHour'];?>
"required>
										</div>
									</div>
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Phone Number</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vPhoneNumber" name="store_detail[vPhoneNumber]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vPhoneNumber'];?>
" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Address</label>
										</div>
										<div class="controls">
											<textarea class="form-control textcss" rows="1" id="tAddress" name="store_detail[tAddress]" value="" required><?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tAddress'];?>
</textarea>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Area</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vArea" name="store_detail[vArea]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vArea'];?>
" required>
										</div>
									</div>
									
									
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Sizes</label>
										</div>
										<div class="controls check-label">
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['sizearray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['sizearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" name="vSize[]" value="<?php echo $_smarty_tpl->tpl_vars['sizearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"  <?php echo $_smarty_tpl->tpl_vars['size_checked']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
>
											   <label for="size-<?php echo $_smarty_tpl->tpl_vars['sizearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['sizearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</label>											
											<?php endfor; endif; ?>
										</div>
									</div>
									
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Price Avr.</label>
										</div>
										<div class="controls check-label">
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['pricearray']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['pricearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" name="vPriceAvr[]" value="<?php echo $_smarty_tpl->tpl_vars['pricearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"  <?php echo $_smarty_tpl->tpl_vars['price_checked']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
>
											   <label for="size-<?php echo $_smarty_tpl->tpl_vars['pricearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['pricearray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</label>											
											<?php endfor; endif; ?>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Looking for</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group ">
												<select class="form-control" id="wearvalue" data-toggle="dropdown" data-all="Work Wear,Weekend Wear,Evening Wear,Special Event Wear,Vintage Wear">	
													<option value="">Menu</option>
													<option value="Work Wear">Work Wear</option>
													<option value="Weekend Wear">Weekend Wear</option>
													<option value="Evening Wear">Evening Wear</option>
													<option value="Special Event Wear">Special Event Wear</option>
													<option value="Vintage Wear">Vintage Wear</option>
													<option class="divider"></option>
													<option value="All">All</option>
												</select>
											</div> -->
											<input class="form-control addtextlooking" id="vLookingFor" name="store_detail[vLookingFor]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vLookingFor'];?>
" type="text" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Want to Buy</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group style-dropdawn">
												<select class="form-control" id="wantBuyValue" data-toggle="dropdown" data-all="Clothing,Lingerie,Coats,Bags,Shoes">	
													<option value="">Menu</option>
													<option value="Clothing">Clothing</option>
													<option value="Lingerie">Lingerie</option>
													<option value="Coats">Coats</option>
													<option value="Bags">Bags</option>
													<option value="Shoes">Shoes</option>
													<option class="divider"></option>
													<option value="All">All</option>
												</select>
											</div> -->
											<input class="form-control addtextbuy" id="vWantToBuy" name="store_detail[vWantToBuy]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vWantToBuy'];?>
" type="text" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">FB datasource</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vFBDataSource" name="store_detail[vFBDataSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vFBDataSource'];?>
" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Instagram datasource</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vInstagramSource" name="store_detail[vInstagramSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vInstagramSource'];?>
" required>
										</div>
									</div>

									<div class="form-group">
											<div class="store-label">
                                            <label class="control-label" for="typeahead">Main Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage']!=''){?>
                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/MainImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage'];?>
" width="90%;" style="margin:0 0 0 2px;"/>
                                            <?php }?>
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vMainImage" name="vMainImage" data-icon="false"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage'];?>
">
                                            </div>
                                    </div>

                                    <div class="form-group">
                                    		<div class="store-label">
                                            <label class="control-label" for="typeahead">Small Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge']!=''){?>
                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/SmalImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge'];?>
" width="90%;" style="margin:0 0 0 2px;"/>
                                            <?php }?>
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vSmallIamge" name="vSmallIamge" data-icon="false"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge'];?>
">
                                            </div>
                                    </div>

                                    <div class="form-group">
                                    		<div class="store-label">
                                            <label class="control-label" for="typeahead">Shop Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage']!=''){?>
                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/ShopImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage'];?>
" style="margin:0 0 0 2px;"/>
                                            <?php }?>
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vShopImage" name="vShopImage" data-icon="false"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage'];?>
">
                                            </div>
                                    </div>

									<div class="form-group" style="float:right;margin-right:10px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});
	});
</script>

<?php }} ?>