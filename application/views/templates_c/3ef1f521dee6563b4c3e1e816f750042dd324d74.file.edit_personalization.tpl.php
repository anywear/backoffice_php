<?php /* Smarty version Smarty-3.1.11, created on 2014-08-30 14:23:16
         compiled from "application/views/templates/admin/personalization/edit_personalization.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15983810053f8b020a6ce07-77969502%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ef1f521dee6563b4c3e1e816f750042dd324d74' => 
    array (
      0 => 'application/views/templates/admin/personalization/edit_personalization.tpl',
      1 => 1409383394,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15983810053f8b020a6ce07-77969502',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8b020ab7e44_57118999',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8b020ab7e44_57118999')) {function content_53f8b020ab7e44_57118999($_smarty_tpl) {?>
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class=""></h1>
    </div>
</div>

<div class="row">
<div class="btn-group" style="float:right;margin:0 20px 10px 0;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization/add" class="btn btn-primary">Add Style</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Style
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body edit_personalization">
            	<div class="row">
            		<div class="col-lg-8">
	            		 <form role="form" action="" method="POST" enctype="multipart/form-data">
                         <input type="hidden" name="action" id="action">
                         <div class="form-group col-md-10" style="padding-left:15px;">
	            			<label class="">Category</label>
                            <!-- <div class="form-group col-md-10"> -->
	            		 	<select name="data[iCategoryId]" class="form-control add-per-cat" required>
                                    <option value="">Select category</option>
                                    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['getCat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                                    	<option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['getCat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['personaData']['iCategoryId']==$_smarty_tpl->tpl_vars['data']->value['getCat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['getCat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCategoryName'];?>
</option>
                                    <?php endfor; endif; ?>
                                </select>
                            </div>
	                    
		            		<div class="img-list cat1">
	            				<div class="col-md-4 personal-image">
	            					<!-- <label class="col-md-2 personal-label"></label> -->
									<div class="col-xs-6 thumb">
						                <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['data']->value['base_url'];?>
uploads/personalization/<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['iCategoryId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['iPersonalId'];?>
/104x354_<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['vImage'];?>
" alt="">
						            </div>
						            <div style="clear:both;"></div>
						            <div class="col-xs-5 thumb">
						            	<input type="hidden" name="data[iPersonalId]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['iPersonalId'];?>
" />
                                                        
						                <!-- <input type="file" class="forn_input" name="vImage" /> -->
                                        
						            </div>
                                </div>
                                <div class="form-group col-md-10" style="padding-left:15px;">
                                    <!-- <label class="">Add Photo</label> -->
                                    <!-- <div class="input-group ">
                                        <input id="upfile" type="text" class="form-control" readonly="" name="vImage" onchange="sub(this)">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                Browse <input id="upfile" type="file" name="vImage" onchange="sub(this)">
                                            </span>
                                        </span>
                                    </div> -->
                                    <div class="form-group col-md-10">
                                        <div class="col-xs-5 thumb">
                                        <div id="yourBtn" class="btn btn-primary" onclick="getFile()">Add Photo</div>
                                        <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" class="forn_input" name="vImage" value="upload" onchange="sub(this)"/></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-10" style="padding-left:15px;">
                                    <!-- <label class="personal-label">Style Title</label> -->
                                    <input type="text" class="form-control upload_inputfile_text" name="data[vStyle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['vStyle'];?>
" required/>
                                </div>
                                
                            </div>
					        
					        <div class="form-group col-md-10" style="padding-left:15px;">
					        	<input type="submit" value="Save changes" class="btn btn-primary"/>
                                <a data-toggle="modal" data-target="#myModal" class="btn btn-default" title="Delete">Delete</a>
					        	<button type="button" class="btn btn-default" onclick="returnme();">Cancel</button>
					        </div>
				         </form>
		            </div>
		        </div>
        	</div>
    	</div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete</h4>
        </div>
        <div class="modal-body">
        Are you sure to delete the Image?
        </div>
        <div class="modal-footer">
        <a class="btn btn-default" data-dismiss="modal">No</a>
        <a class="btn btn-default" style="float:right;" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization/action_update?id=<?php echo $_smarty_tpl->tpl_vars['data']->value['personaData']['iPersonalId'];?>
">Yes</a>
        </div>
    </div>
    </div>
</div>

	<script type="text/javascript">
    
		function returnme(){
			window.location.href = base_url+'personalization';
		}

        function getFile(){
   document.getElementById("upfile").click();
 }
 function sub(obj){
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("yourBtn").innerHTML = fileName[fileName.length-1];
    document.myForm.submit();
    event.preventDefault();
  }
 

$(document).ready( function() {
    /*$('.photo-delete').on('click',function(){
        $("#action").val("Delete");
            $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
        });*/
});
	</script>
<?php }} ?>