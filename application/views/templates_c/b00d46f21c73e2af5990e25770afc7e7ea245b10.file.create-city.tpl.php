<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 12:53:36
         compiled from "application/views/templates/admin/city/create-city.tpl" */ ?>
<?php /*%%SmartyHeaderCode:213511618253fefdf6534d92-21263335%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b00d46f21c73e2af5990e25770afc7e7ea245b10' => 
    array (
      0 => 'application/views/templates/admin/city/create-city.tpl',
      1 => 1409291615,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '213511618253fefdf6534d92-21263335',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fefdf6567446_55874682',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fefdf6567446_55874682')) {function content_53fefdf6567446_55874682($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
city.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
city/create" class="store-edit btn btn-primary">Add City</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add City
                <div class="pull-right">
                    
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
city/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
								<fieldset>
									<!-- <input type="hidden" name="iSizeId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['size_detail']['iSizeId'];?>
'> -->
									<div class="form-group col-md-10" style="padding-left:15px;">
                                        <label for="typeahead">Name of City</label>
                                        <input type="text" class="form-control" id="vCityName" name="city_detail[vCityName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['size_detail']['vCityName'];?>
" required>
									</div>
									
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>

<?php }} ?>