<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 21:47:11
         compiled from "application/views/templates/admin/personalization/view_personalization.tpl" */ ?>
<?php /*%%SmartyHeaderCode:197751598053f8ad948eee44-89733829%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '67e506e03b3fdef9e76b55ff7b803afa4d4ba8fc' => 
    array (
      0 => 'application/views/templates/admin/personalization/view_personalization.tpl',
      1 => 1409323627,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '197751598053f8ad948eee44-89733829',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ad9490a0f4_61816736',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ad9490a0f4_61816736')) {function content_53f8ad9490a0f4_61816736($_smarty_tpl) {?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Personalization</h1>
    </div>
</div>

<div class="row" >
    <div class="btn-group" style="float:right;margin:0 20px 10px 0;">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization/add" class="btn btn-primary">Add Style</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Style List
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body img-list">
            	<div class="row">
            		<h4>Name: <small><?php echo $_smarty_tpl->tpl_vars['data']->value['getCat'][0]['vCategoryName'];?>
</small></h4>
            		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['cat1PersonaData']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			            	<a class="thumbnail" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization/edit?iPersonalId=<?php echo $_smarty_tpl->tpl_vars['data']->value['cat1PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPersonalId'];?>
">
			                    <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['data']->value['base_url'];?>
uploads/personalization/<?php echo $_smarty_tpl->tpl_vars['data']->value['cat1PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['cat1PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPersonalId'];?>
/250x250_<?php echo $_smarty_tpl->tpl_vars['data']->value['cat1PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="">
			                </a>
			            </div>
		            <?php endfor; endif; ?>
		        </div>
		        
		        <hr />
		        
		        <div class="row">
            		<h4>Name: <small><?php echo $_smarty_tpl->tpl_vars['data']->value['getCat'][1]['vCategoryName'];?>
</small></h4>
            		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['cat2PersonaData']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			            	<a class="thumbnail" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
personalization/edit?iPersonalId=<?php echo $_smarty_tpl->tpl_vars['data']->value['cat2PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPersonalId'];?>
">
			                    <img class="img-responsive" src="<?php echo $_smarty_tpl->tpl_vars['data']->value['base_url'];?>
uploads/personalization/<?php echo $_smarty_tpl->tpl_vars['data']->value['cat2PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCategoryId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['cat2PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iPersonalId'];?>
/250x250_<?php echo $_smarty_tpl->tpl_vars['data']->value['cat2PersonaData'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vImage'];?>
" alt="">
			                </a>
			            </div>
		            <?php endfor; endif; ?>
		        </div>
        	</div>
    	</div>
    </div>
</div>        <?php }} ?>