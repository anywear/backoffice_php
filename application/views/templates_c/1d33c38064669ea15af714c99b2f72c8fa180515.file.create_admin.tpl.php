<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 18:53:38
         compiled from "application/views/templates/admin/admin_management/create_admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:165892284453f8ae82cc1ca2-51100953%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d33c38064669ea15af714c99b2f72c8fa180515' => 
    array (
      0 => 'application/views/templates/admin/admin_management/create_admin.tpl',
      1 => 1409290881,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '165892284453f8ae82cc1ca2-51100953',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ae82ceff59_24034102',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ae82ceff59_24034102')) {function content_53f8ae82ceff59_24034102($_smarty_tpl) {?><div class="row" style="width:550px;">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
				<form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
					<fieldset>
					<legend><?php echo $_smarty_tpl->tpl_vars['data']->value['label'];?>
 New Admin</legend>
						<input type="hidden" name="iAdminId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['iAdminId'];?>
'>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">First Name</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vFirstName" name="admin_detail[vFirstName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vFirstName'];?>
">
							</div>
							<span id="firstnameinput"></span>
						</div>

						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Last Name</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vLastName" name="admin_detail[vLastName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vLastName'];?>
">
							</div>
							<span id="lastnameinput"></span>
						</div>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Email</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vEmail" name="admin_detail[vEmail]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vEmail'];?>
">
							</div>
							<span id="emailinput"></span>
							<span id="properemail"></span>
						</div>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Password</label>
							</div>
							<div class="controls">
								<input type="password" class="admin-form" id="vPassword" name="admin_detail[vPassword]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vPassword'];?>
">
							</div>
							<span id="passwordinput"></span>
						</div>
						<div class="form-group" style="margin-left:5px;">
							<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
							<button type="button" id="btn-save" class="btn btn-primary" onclick="validate();">Create</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	function validate(){
    if($( "#vFirstName" ).val() ==''){
    	$("#firstnameinput").html( "<p style='margin:5px 0 0 191px;'>Please Enter First Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#firstnameinput").hide();
    }
    if($( "#vLastName" ).val() ==''){
        $("#lastnameinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Last Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#lastnameinput").hide();}
    if($( "#vEmail" ).val() ==''){
        $("#emailinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Email!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#emailinput").hide();}
    if(IsEmail($( "#vEmail" ).val())==false){
        $("#properemail").html( "<p style='margin:5px 0 0 191px;>Please Enter Proper Email Address!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#properemail").hide();}
    if($("#vPassword" ).val() ==''){
        $("#passwordinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Password!</p>" );
        $("#myalert").modal('show');
        return false;
    }
    else{
    	$("#passwordinput").hide();
        $("#frmadmin").submit();
    }
}
</script>
<?php }} ?>