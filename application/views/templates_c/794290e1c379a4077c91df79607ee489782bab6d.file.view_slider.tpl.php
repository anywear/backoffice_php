<?php /* Smarty version Smarty-3.1.11, created on 2014-08-28 22:54:14
         compiled from "application/views/templates/admin/slider/view_slider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:192713519353fc4af24c0e51-37378331%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '794290e1c379a4077c91df79607ee489782bab6d' => 
    array (
      0 => 'application/views/templates/admin/slider/view_slider.tpl',
      1 => 1409241252,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '192713519353fc4af24c0e51-37378331',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fc4af24d9044_49939231',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc4af24d9044_49939231')) {function content_53fc4af24d9044_49939231($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
slider.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
slider/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">App Home Slider</h1>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
slider/create" class="notify-create btn btn-primary">Add New Slider</a>
                        <a href="" class="make-active btn btn-success">Make Active</a>
                        <a href="" class="make-inactive btn btn-info">Make Inactive</a>
                        <a href="" class="notify-delete btn btn-danger">Delete</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Slider Listing
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="sliderid">
                                <thead>
                                    <tr>
                                        <th width="3%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId" value="1"></th>
                                        <th width="25%">Title</th>
					<!--<th width="25%">Type</th>-->
					<th width="15%">Status</th>
					<th width="15%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php }} ?>