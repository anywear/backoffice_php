<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 17:43:46
         compiled from "application/views/templates/admin/store_size/create-store-size.tpl" */ ?>
<?php /*%%SmartyHeaderCode:121156398353fdee775213b5-33802778%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '840e9a144d5a6344848b19fad9b35a2c960ec08b' => 
    array (
      0 => 'application/views/templates/admin/store_size/create-store-size.tpl',
      1 => 1409309025,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '121156398353fdee775213b5-33802778',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fdee77575586_89342024',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fdee77575586_89342024')) {function content_53fdee77575586_89342024($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
store_size.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Size
                <div class="pull-right">
                    <div class="btn-group">
                        
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store_size/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
								<fieldset>
									<!-- <input type="hidden" name="iSizeId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['size_detail']['iSizeId'];?>
'> -->
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Name of Size</label>
										<input type="text" class="form-control " id="vSizeTitle" name="size_detail[vSizeTitle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['style_detail']['vSizeTitle'];?>
" required>
										
									</div>
									
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Sorting</label>
										<div class="btn-group size-style-dropdawn">
											<select id="iSorting" class="form-control" data-toggle="dropdown" name="size_detail[iSorting]" title="order no" lang="*">
											 	<option value=''>--Select Order--</option>
                                                <?php while (($_smarty_tpl->tpl_vars['data']->value['totalRec']+1)>=$_smarty_tpl->tpl_vars['data']->value['initOrder']){?>
                                                <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder'];?>
" ><?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder']++;?>
</option>
                                                <?php }?>
											</select>
											</div>
										
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>

<?php }} ?>