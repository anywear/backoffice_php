<?php /* Smarty version Smarty-3.1.11, created on 2014-09-08 20:32:42
         compiled from "application/views/templates/admin/products/view-product-detail.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13914922475405920629fb80-92700118%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d59a4261c23ec55d1eac8de2f33a82bc07c1348' => 
    array (
      0 => 'application/views/templates/admin/products/view-product-detail.tpl',
      1 => 1410177606,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13914922475405920629fb80-92700118',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_540592063267b3_69187306',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540592063267b3_69187306')) {function content_540592063267b3_69187306($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
list_store.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Store List</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Store DataTables Advanced <?php if ($_smarty_tpl->tpl_vars['data']->value['getStoreDetail']['eOnSale']=='Yes'){?>
                <button type="button" class="btn btn-success" style="margin-left:30px;">On Sale</button><?php }?>
                <div class="pull-right">
                    <!-- <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="span12">
                                <div class="block">
                                    <div class="block-content collapse in">
                                        <!--  -->
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Name of Product </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vName'];?>

                                            </div>
                                        </div>
                                         <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Brand </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vBrandedName'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Unbrand Name </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vUnbrandedName'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Currency </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vCurrency'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Price </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['fPrice'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vImage']!=''){?>
                                                <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/products/<?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['iProductId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vImage'];?>
" width="90%;" style="margin:0 0 0 0px;"/>
                                            <?php }?>
                                            </div>
                                        </div>

                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Stock </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['iInStock'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Locale </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['vLocale'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Description </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['tDescription'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Extract Date </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['dExtractDate'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Status </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['eStatus'];?>

                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Data from </label>
                                            </div>
                                            <div class="store-details">
                                                <?php echo $_smarty_tpl->tpl_vars['data']->value['getProductDetail']['eDataFrom'];?>

                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

</script>
<?php }} ?>