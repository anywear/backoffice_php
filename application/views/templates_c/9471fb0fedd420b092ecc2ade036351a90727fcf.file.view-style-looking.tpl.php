<?php /* Smarty version Smarty-3.1.11, created on 2014-08-28 22:49:08
         compiled from "application/views/templates/admin/style_looking/view-style-looking.tpl" */ ?>
<?php /*%%SmartyHeaderCode:118333181253fdcfa77752c1-84200831%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9471fb0fedd420b092ecc2ade036351a90727fcf' => 
    array (
      0 => 'application/views/templates/admin/style_looking/view-style-looking.tpl',
      1 => 1409240945,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '118333181253fdcfa77752c1-84200831',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fdcfa77a5b25_93626607',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fdcfa77a5b25_93626607')) {function content_53fdcfa77a5b25_93626607($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
style_looking.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
style_looking/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Style List</h1>
    </div>
</div>
<!-- <div style="margin:0 0 0 33px;">
    <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
style_looking/create">Add Size +</a>
</div> -->
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
style_looking/create" class="btn btn-primary">Add Style</a>
                        <a href="" class="btn btn-success make-active">Make Active</a>
                        <a href="" class="btn btn-info make-inactive">Make InActive</a>
                        <a href="" class="btn btn-danger notify-delete">Delete Style</a>
                        <!-- <button type="button" class="btn btn-default" >Add Style</button>
                        <button type="button" class="btn btn-default" >Make Active</button>
                        <button type="button" class="btn btn-default" >Make InActive</button>
                        <button type="button" class="btn btn-default" >Delete Style</button> -->
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Style DataTables
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="styletable">
                                <thead>
                                    <tr>
                                        <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
                                        <th width="20%">Name of Style</th>
                                        <th width="20%">Status</th>
                                        <th width="20%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('.notify-delete').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
        $("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
            }
        return false;
        });
    
    
    $(".make-active").click(function() {
        $("#action").val("Active");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    
    $(".make-inactive").click(function() {
        $("#action").val("Inactive");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    });
    
    function submitform() {
    $("#frmlist").submit();
    }
</script>
<?php }} ?>