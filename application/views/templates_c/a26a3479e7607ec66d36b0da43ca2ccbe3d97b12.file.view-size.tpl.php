<?php /* Smarty version Smarty-3.1.11, created on 2014-08-28 22:54:38
         compiled from "application/views/templates/admin/size/view-size.tpl" */ ?>
<?php /*%%SmartyHeaderCode:102802462653fc5d015138f3-54616448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a26a3479e7607ec66d36b0da43ca2ccbe3d97b12' => 
    array (
      0 => 'application/views/templates/admin/size/view-size.tpl',
      1 => 1409241161,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '102802462653fc5d015138f3-54616448',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fc5d0152b9c2_38195186',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc5d0152b9c2_38195186')) {function content_53fc5d0152b9c2_38195186($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
size.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Size List</h1>
    </div>
</div>
<!--<div style="margin:0 0 0 33px;">
    <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size/create">Add Size +</a>
</div>-->
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size/create" class="store-edit btn btn-primary">Add Size</a>
                        <a href="" class="make-active btn btn-success">Make Active</a>
                        <a href="" class="make-inactive btn btn-info">Make InActive</a>
                        <a href="" class="notify-delete btn btn-danger">Delete Size</a>
                        
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Size DataTables
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="sizetable">
                                <thead>
                                    <tr>
                                        <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
                                        <th width="20%">Name of Size</th>
                                        <th width="15%">Size Type</th>
                                        <th width="20%">Sorting</th>
                                        <th width="20%">Status</th>
                                        <th width="20%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('.notify-delete').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
        $("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
            }
        return false;
        });
    
    
    $(".make-active").click(function() {
        $("#action").val("Active");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    
    $(".make-inactive").click(function() {
        $("#action").val("Inactive");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    });
    
    function submitform() {
    $("#frmlist").submit();
    }
</script>
<?php }} ?>