<?php /* Smarty version Smarty-3.1.11, created on 2014-08-23 22:12:59
         compiled from "application/views/templates/admin/user/view_user_details.tpl" */ ?>
<?php /*%%SmartyHeaderCode:72241216453f8af7b6fdda0-28793800%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10904b1e593521918787d0494437ddf317a2fd62' => 
    array (
      0 => 'application/views/templates/admin/user/view_user_details.tpl',
      1 => 1408705357,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '72241216453f8af7b6fdda0-28793800',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8af7b74c2d9_39735671',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8af7b74c2d9_39735671')) {function content_53f8af7b74c2d9_39735671($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/php/anywear/system/libs/smarty/plugins/modifier.date_format.php';
?><div class="row" style="width:600px;">
    <div class="col-lg-12">
        <h3 class="user-profile-header">User Profile</h3>
    </div>
</div>
<div class="row">
<div class="row">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
            	<div class="form-group">
					<!-- <label class="control-label" for="typeahead">Profile Image :</label> -->
					<div class="user-image">
						<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
/uploads/user/<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['iUserId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vImage'];?>
" width="90%;" />
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">First Name </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vFirstName'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Last Name </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vLastName'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Email </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vEmail'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Password </label>
					</div>
					<div>
						****************
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Date of birth </label>
					</div>
					<div>
						<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['data']->value['getUserDetails']['dDateOfBirth'],"%m/%d/%Y");?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Type </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['eType'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I Live in </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vLeaveIn'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">My Style </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vMyStyle'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Size </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vSize'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">How Much usualy I Spend </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vSpendMoney'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I'm Looking for </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vLookingFor'];?>

					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I Want to Buy </label>
					</div>
					<div>
						<?php echo $_smarty_tpl->tpl_vars['data']->value['getUserDetails']['vWantBuy'];?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php }} ?>