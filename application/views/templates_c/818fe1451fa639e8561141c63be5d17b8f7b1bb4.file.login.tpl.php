<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 20:36:51
         compiled from "application/views/templates/admin/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:136642982653f8adfe0d6939-82375819%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '818fe1451fa639e8561141c63be5d17b8f7b1bb4' => 
    array (
      0 => 'application/views/templates/admin/login.tpl',
      1 => 1409319410,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '136642982653f8adfe0d6939-82375819',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8adfe10a602_59413278',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8adfe10a602_59413278')) {function content_53f8adfe10a602_59413278($_smarty_tpl) {?><!DOCTYPE html>
<html class="no-js">
    <head>
        <title>::: AnyWear :::</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
        <!-- Bootstrap -->
        <link href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
css/login_bootstrap.min.css" rel="stylesheet">
        <meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!--<link href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
css/bootstrap.css" rel="stylesheet" media="screen">-->
        <!--<link href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">-->
        
        <style>
			.modal-footer {   border-top: 0px; float: left; width: 100%; }
			.modal-content { float: left; width: 100%; }
		</style>
        
        
    </head>
    <body>
		<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
		  <div class="modal-content">
		      <div class="modal-header">
		          <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
		          <h1 class="text-center">Login</h1>
		      </div>
		      <div class="modal-body">
		      		<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1!=''){?>
		  				<div class="alert alert-danger">
		                    <?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>

		                </div>
	                <?php }?>
		          <form class="form col-md-12 center-block" id="frmsignin" role="form" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
authentication" method="post">
		            <div class="form-group">
		              <input type="text" class="form-control input-lg" placeholder="Email" name="vEmail" id="vEmail">
		            </div>
		            <div class="form-group">
		              <input type="password" class="form-control input-lg" placeholder="Password" name="vPassword" id="vPassword">
		            </div>
		            <div class="form-group">
		              <button class="btn btn-primary btn-lg btn-block">Sign In</button>
		            </div>
		          </form>
		      </div>
		      <div class="modal-footer">
		          <div class="col-md-12">
		          	<!--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>-->
				  </div>
		      </div>
		  </div>
		  </div>
		</div>
        <!--<a style="background:none; border:none; box-shadow:none;margin-top: 10px;width:97%;" class="brand-logo bottom-buffer" href="#"></a>
        <div class="container">
            <div class="row">
                <?php if ($_smarty_tpl->tpl_vars['data']->value['message']!=''){?>
                <div class="span4" style="margin:0px auto;float:none;">
                    <div class="alert alert-info">
                        <?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>

                    </div>
                </div>
                <?php }?>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title form-signin-heading">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
            
                        <form  role="form" class="form-signin" id="frmsignin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
authentication" method="post">
                            
                            <div class="form-group">
                                <input type="text" class="input-block-level" placeholder="Email" maxlength="255" name="vEmail" id="vEmail">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-block-level" placeholder="Password" maxlength="255" name="vPassword" id="vPassword">
                            </div>
                            <div class="checkbox">
                                <label class="checkbox" style="width:69%; float:left;">

                                    <input type="checkbox" value="remember-me"> Remember me
                                </label>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>-->
        <script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
js/jquery-1.11.0.js"></script>
        <script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
js/login_bootstrap.min.js"></script>
    </body>
</html>



<?php }} ?>