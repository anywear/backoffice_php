<?php /* Smarty version Smarty-3.1.11, created on 2014-08-30 12:33:51
         compiled from "application/views/templates/admin/notification/view_notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:194423800853f8ad976365b1-36499965%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c16773aa6b95e8feb38edcb831b07149ebb1b829' => 
    array (
      0 => 'application/views/templates/admin/notification/view_notification.tpl',
      1 => 1409373292,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '194423800853f8ad976365b1-36499965',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ad97657890_03253113',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ad97657890_03253113')) {function content_53f8ad97657890_03253113($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
notification.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
notification/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Notification</h1>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
    <div class="" style="float:right;margin:0 20px 10px 0;">
        <a href="" class="notify-create btn btn-primary">Compose</a>
        <a href="" class="notify-edit btn btn-success">Send</a>
        <a href="" class="notify-delete btn btn-danger" data-id="Delete">Delete</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Message List
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="notificationid">
                                <thead>
                                    <tr>
                                        <th width="5%"></th>
                                        <th width="80%"></th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script>
    $(document).ready(function() {
        $('.notify-create').on('click',function(){
        	$.ajax({
            type     : "POST",
            cache    : false,
            url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
notification/create",
            success: function(data) {
                $.fancybox({
                    'width': 800,
                    'height': 400,
                    'enableEscapeButton' : false,
                    'overlayShow' : true,
                    'overlayOpacity' : 0,
                    'hideOnOverlayClick' : false,
                    'content' : data
                });
                return false;
            },
            error: function(response){
                alert('Error: Please try later.');
                return false;
            }
        });
        return false;
        });


        $('.notify-delete').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
		$("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><button type="button" class="btn bottom-buffer" onclick="submitform();">Delete</button><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn bottom-buffer" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
            }
        return false;
        });
	
	
	$('.notify-edit').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
		$("#action").val("Send");
		$("#frmlist").submit();
                return false;
            }
        return false;
        });
	
    });
    
    function submitform() {
	$("#frmlist").submit();
    }
	
</script>
<?php }} ?>