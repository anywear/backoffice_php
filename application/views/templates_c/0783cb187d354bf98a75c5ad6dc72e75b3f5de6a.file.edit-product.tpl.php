<?php /* Smarty version Smarty-3.1.11, created on 2014-09-10 19:03:30
         compiled from "application/views/templates/admin/products/edit-product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:443969977540573fbbd2a78-40399002%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0783cb187d354bf98a75c5ad6dc72e75b3f5de6a' => 
    array (
      0 => 'application/views/templates/admin/products/edit-product.tpl',
      1 => 1410350530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '443969977540573fbbd2a78-40399002',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_540573fbd8b954_55884288',
  'variables' => 
  array (
    'data' => 0,
    'eStatus' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_540573fbd8b954_55884288')) {function content_540573fbd8b954_55884288($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
products.js"></script>
<input type="hidden" id="iStoreId" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['iStoreId'];?>
" />
<div class="row">
<div class="navbar"></div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit product
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
products/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
?iProductId=<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['iProductId'];?>
" method="post" enctype="multipart/form-data">
								<fieldset>
								    <input type="hidden" name="iStoreId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['iStoreId'];?>
'>
								    <input type="hidden" name="iProductId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['iProductId'];?>
'>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Brand</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="products[iBrandId]">		
										    <option>Select brand</option>
										    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['iBrandId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
										    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['iBrandId'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBrandId'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['iBrandId'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iBrandId']==$_smarty_tpl->tpl_vars['data']->value['products_detail']['iBrandId']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['iBrandId'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
</option>
										    <?php endfor; endif; ?>
										</select>
										</div>
									    </div>
									</div>
									
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Color</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductColorId][]" multiple>		
										    <!--<option>Select Color</option>-->
										    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['color']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
										    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['color'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductColorId'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['data']->value['color'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductColorId'],$_smarty_tpl->tpl_vars['data']->value['productColor'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductColorId'])){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['color'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vColorName'];?>
</option>
										    <?php endfor; endif; ?>
										</select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Size</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductSizeId][]" multiple>		
										    <!--<option>Select Size</option>-->
										    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['size']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
										    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['size'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductSizeId'];?>
" <?php if (in_array($_smarty_tpl->tpl_vars['data']->value['size'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iProductSizeId']==$_smarty_tpl->tpl_vars['data']->value['productSize']['iProductSizeId'])){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['size'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeName'];?>
</option>
										    <?php endfor; endif; ?>
										</select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Product Category</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductCatId][]" multiple>		
										    <!--<option>Select Product Category</option>-->
										    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['product_cat']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
										    <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['product_cat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['product_cat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['id']==$_smarty_tpl->tpl_vars['data']->value['products_detail']['id']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['product_cat'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
</option>
										    <?php endfor; endif; ?>
										</select>
										</div>
									    </div>
									</div>
										
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Name</label>	
									    <input class="form-control " rows="1" id="vName" name="products[vName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['vName'];?>
" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Brand Name</label>	
									    <input class="form-control " rows="1" id="vBrandedName" name="products[vBrandedName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['vBrandedName'];?>
" required>
									</div>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Unbrand Name</label>	
									    <input class="form-control " rows="1" id="vUnbrandedName" name="products[vUnbrandedName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['vUnbrandedName'];?>
" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Currency</label>	
									    <input class="form-control " rows="1" id="vCurrency" name="products[vCurrency]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['vCurrency'];?>
" required>
									</div>
									
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Price</label>
									    <input type="text" class="form-control " id="fPrice" name="products[fPrice]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['fPrice'];?>
" required>
									</div>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Product Image</label>
									    <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage']!=''){?>
										    <img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/ShopImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage'];?>
" style="margin:0 0 0 2px;"/>
									    <?php }else{ ?>
										    <div class="input-group">
										    <input type="text" class="form-control" readonly="" name="vImage">					
										    <span class="input-group-btn">
											    <span class="btn btn-primary btn-file">
												    Browse <input type="file" name="vImage">
											    </span>
										    </span>
										    </div>
									    <?php }?>
									</div>
					
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Stock</label>	
									    <input class="form-control " rows="1" id="iInStock" name="products[iInStock]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['iInStock'];?>
" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Locale</label>	
									    <input class="form-control " rows="1" id="vLocale" name="products[vLocale]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['vLocale'];?>
" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Description</label>	
									    <input class="form-control " rows="1" id="tDescription" name="products[tDescription]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['tDescription'];?>
" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Extract Date</label>	
									    <input class="form-control " rows="1" id="dExtractDate" name="products[dExtractDate]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['products_detail']['dExtractDate'];?>
" required>
									</div>

									<div class="form-group col-md-10" style="padding-left:15px;">
									    <label class="control-label" for="typeahead">Status</label>
										<div class="btn-group size-style-dropdawn">
										    <select name="products[eStatus]" class="form-control" data-toggle="dropdown" required>
										    <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eStatus']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<option value="<?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['products']['eStatus']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
										    <?php endfor; endif; ?>
										    </select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" id="cancel">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
									</div>
									
								</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).on('change', '.btn-file :file', function() {
	  var input = $(this),
	      numFiles = input.get(0).files ? input.get(0).files.length : 1,
	      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	  input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready( function() {
	    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
	        
	        var input = $(this).parents('.input-group').find(':text'),
	            log = numFiles > 1 ? numFiles + ' files selected' : label;
	        
	        if( input.length ) {
	            input.val(log);
	        } else {
	            if( log ) alert(log);
	        }
	        
	    });
	});
</script>

<?php }} ?>