<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 17:45:27
         compiled from "application/views/templates/admin/store_size/edit-store-size.tpl" */ ?>
<?php /*%%SmartyHeaderCode:167539761453fdf144b476e8-78269832%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e5c933b8a1786daf7f87987f73944dc7c66309a8' => 
    array (
      0 => 'application/views/templates/admin/store_size/edit-store-size.tpl',
      1 => 1409309123,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '167539761453fdf144b476e8-78269832',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fdf144d5cd18_71589569',
  'variables' => 
  array (
    'data' => 0,
    'eStatus' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fdf144d5cd18_71589569')) {function content_53fdf144d5cd18_71589569($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
store_size.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Size
                <div class="pull-right">
                    <div class="btn-group">
                        
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store_size/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iStoreSizeId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['size']['iStoreSizeId'];?>
'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Name of Size</label>
										<input type="text" class="form-control size-style-dropdawn" id="vSizeTitle" name="size_detail[vSizeTitle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['size']['vSizeTitle'];?>
" required>
										
									</div>
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Sorting</label>
                                            <div class="btn-group size-style-dropdawn">
                                            <select id="iSorting" class="form-control" data-toggle="dropdown" name="size_detail[iSorting]" title="order no" lang="*">
                                                <option value=''>--Select Order--</option>
                                            <?php while (($_smarty_tpl->tpl_vars['data']->value['totalRec'])>=$_smarty_tpl->tpl_vars['data']->value['initOrder']){?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder'];?>
" <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['size']['iSorting'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1==$_smarty_tpl->tpl_vars['data']->value['initOrder']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder']++;?>
</option>
                                            <?php }?>
                                            </select>
                                            </div>
                                        
                                    </div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Status</label>
										<div class="btn-group size-style-dropdawn">
												<select name="size_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eStatus']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												    <option value="<?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['size']['eStatus']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
												<?php endfor; endif; ?>
												</select>
											</div>
									    
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">

</script>

<?php }} ?>