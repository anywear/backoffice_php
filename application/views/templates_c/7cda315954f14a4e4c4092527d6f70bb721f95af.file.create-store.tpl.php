<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 20:59:33
         compiled from "application/views/templates/admin/home/create-store.tpl" */ ?>
<?php /*%%SmartyHeaderCode:134688610753f8ac86ab2891-59964337%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cda315954f14a4e4c4092527d6f70bb721f95af' => 
    array (
      0 => 'application/views/templates/admin/home/create-store.tpl',
      1 => 1409320729,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134688610753f8ac86ab2891-59964337',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ac86b0af28_32587249',
  'variables' => 
  array (
    'data' => 0,
    'style' => 0,
    'store_size' => 0,
    'eSign' => 0,
    'style_looking' => 0,
    'store_category' => 0,
    'eDeeplinkSupport' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ac86b0af28_32587249')) {function content_53f8ac86b0af28_32587249($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
list_store.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Stores Data
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iAdminId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['iAdminId'];?>
'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label for="typeahead">Name of Store</label>
										<input type="text" class="form-control " id="vStoreName" name="store_detail[vStoreName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStoreName'];?>
" required>
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label  for="typeahead">Style</label>
										<div class="input-prepend">
											<div class="btn-group style-dropdawn">
												<select class="form-control " row="1" data-toggle="dropdown" id="stylevalue" data-all="Vintage,Modern,Slipee,Banana,Evening,Wow">		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['style']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					                                <option value="<?php echo $_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle'];?>
" <?php if ($_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle'];?>
</option>
					                                <?php endfor; endif; ?>
												</select>
											</div>
											<input class="form-control textdropdown" id="vStyle" name="store_detail[vStyle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStyle'];?>
" type="text" required>
										</div>
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
  										<label for="typeahead">Description</label>
										<textarea class="form-control " rows="3" id="tDescription" name="store_detail[tDescription]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tDescription'];?>
" required></textarea>
										
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Opening Hour</label>
										<input type="text" class="form-control " id="vOpeningHour" name="store_detail[vOpeningHour]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vOpeningHour'];?>
"required>
										
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Phone Number</label>
										<input type="text" class="form-control " id="vPhoneNumber" name="store_detail[vPhoneNumber]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vPhoneNumber'];?>
" required>
										
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Address</label>	
											<textarea class="form-control " rows="1" id="tAddress" name="store_detail[tAddress]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tAddress'];?>
" required></textarea>
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label  for="typeahead">Area</label>
											<input type="text" class="form-control " id="vArea" name="store_detail[vArea]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vArea'];?>
" required>
									</div>
									<!-- <div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Sizes</label>
										<div class="check-label">
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['store_size']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
" name="vSize[]" value="<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
">
											   <label for="size-<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
"><?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
</label>											
											<?php endfor; endif; ?>
										</div>	
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Price Avr.</label>
										<div class="check-label">
											<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eSign']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
											<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
" name="vPriceAvr[]" value="<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
">
											   <label for="size-<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
"><?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
</label>											
											<?php endfor; endif; ?>
										</div>
									</div> -->
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Looking for</label>
										<div class="input-prepend">
											<div class="btn-group looking-style-dropdawn">
												<select class="form-control" id="wearvalue" data-toggle="dropdown" data-all="Work Wear,Weekend Wear,Evening Wear,Special Event Wear,Vintage Wear">	
													<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['style_looking']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					                                <option value="<?php echo $_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName'];?>
" <?php if ($_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName'];?>
</option>
					                                <?php endfor; endif; ?>
												</select>
											</div>
										
											<input class="form-control textdropdowntest" id="vLookingFor" name="store_detail[vLookingFor]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vLookingFor'];?>
" type="text" required>
											</div>
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Want to Buy</label>
										<div class="input-prepend">
											<div class="btn-group want-style-dropdawn">
												<select class="form-control" id="wantBuyValue" data-toggle="dropdown" data-all="Clothing,Lingerie,Coats,Bags,Shoes">	
													<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['store_category']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					                                <option value="<?php echo $_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName'];?>
" <?php if ($_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName'];?>
</option>
					                                <?php endfor; endif; ?>
												</select>
											</div>

											<input class="form-control wanttextdropdowntest" id="vWantToBuy" name="store_detail[vWantToBuy]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vWantToBuy'];?>
" type="text" required>
											</div>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label for="typeahead">Deeplink Support</label>
										<select name="store_detail[eDeeplinkSupport]" class="form-control" data-toggle="dropdown" required>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eDeeplinkSupport']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												    <option value="<?php echo $_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['store_detail']['eDeeplinkSupport']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
												<?php endfor; endif; ?>
												</select>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label for="typeahead">Host Domain</label>
										<input type="text" class="form-control " id="vHostDomain" name="store_detail[vHostDomain]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vHostDomain'];?>
" required>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label for="typeahead">FB datasource</label>
										<input type="text" class="form-control" id="vFBDataSource" name="store_detail[vFBDataSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vFBDataSource'];?>
" required>
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Instagram datasource</label>
										<input type="text" class="form-control" id="vInstagramSource" name="store_detail[vInstagramSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vInstagramSource'];?>
" required>
										
									</div>
									<div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Main Image</label>
										<?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage']==''){?>
											<div class="input-group">
											<input type="text" class="form-control" readonly="" name="vMainImage">					
											<span class="input-group-btn">
												<span class="btn btn-primary btn-file">
													Browse <input type="file" name="vMainImage">
												</span>
											</span>
											</div>
										<?php }?>
											<!-- <div class="btn btn-primary btn-file">Browse
										}
										<input type="file" <?php if ($_smarty_tpl->tpl_vars['data']->value['user']['vMainImage']==''){?>id="vMainImage"<?php }?> name="vMainImage"   value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vMainImage'];?>
">
                                       </div>  -->
                                    </div>
                                    <div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Small Image</label>
										<?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge']==''){?>
											<div class="input-group">
											<input type="text" class="form-control" readonly="" name="vSmallIamge">					
											<span class="input-group-btn">
												<span class="btn btn-primary btn-file">
													Browse <input type="file" name="vSmallIamge">
												</span>
											</span>
											</div>
										<?php }?>
                                    	<!-- <input type="file" class="filestyle" <?php if ($_smarty_tpl->tpl_vars['data']->value['user']['vSmallIamge']==''){?>id="vSmallIamge"<?php }?> name="vSmallIamge" data-icon="false"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vSmallIamge'];?>
" onchange="CheckValidFile(this.value,this.name)"> -->
                                        
                                    </div>
                                    <div class="form-group col-md-10"style="padding-left:15px;">
										<label for="typeahead">Shop Image</label>
										<?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage']==''){?>
											<div class="input-group ">
											<input type="text" class="form-control" readonly="" name="vShopImage">					
											<span class="input-group-btn">
												<span class="btn btn-primary btn-file">
													Browse <input type="file" name="vShopImage">
												</span>
											</span>
											</div>
										<?php }?>
                                    	<!-- <input type="file" class="filestyle" <?php if ($_smarty_tpl->tpl_vars['data']->value['user']['vShopImage']==''){?>id="vShopImage"<?php }?> name="vShopImage" data-icon="false"  value="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_detail']['vShopImage'];?>
" onchange="CheckValidFile(this.value,this.name)"> -->
                                        
                                    </div>

									<div class="form-group col-md-10" style="padding-left:15px;">
										
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});

			
	});
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>

<?php }} ?>