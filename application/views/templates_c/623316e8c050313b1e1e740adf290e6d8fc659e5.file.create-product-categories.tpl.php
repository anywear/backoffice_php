<?php /* Smarty version Smarty-3.1.11, created on 2014-09-08 17:03:27
         compiled from "application/views/templates/admin/product_categories/create-product-categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1087628817540015018a3b13-60244508%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '623316e8c050313b1e1e740adf290e6d8fc659e5' => 
    array (
      0 => 'application/views/templates/admin/product_categories/create-product-categories.tpl',
      1 => 1410170600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1087628817540015018a3b13-60244508',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_5400150197c032_99184426',
  'variables' => 
  array (
    'data' => 0,
    'pranet_product_category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5400150197c032_99184426')) {function content_5400150197c032_99184426($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
product-categories.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Product Categories
                <div class="pull-right">
                    <div class="btn-group"></div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
product_categories/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
" method="post" enctype="multipart/form-data">
				<fieldset>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Product Category</label>
                                        <div class="btn-group size-style-dropdawn">
					    <select name="data[vParentId]" class="form-control" data-toggle="dropdown">
						<option value="">Select Category</option>
						<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['pranet_product_category']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['pranet_product_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vProductCatId'];?>
"><?php echo $_smarty_tpl->tpl_vars['pranet_product_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vName'];?>
</option>
						<?php endfor; endif; ?>
					    </select>
                                        </div>
                                    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
						<label class="control-label" for="typeahead">Category</label>
						<input type="text" class="form-control size-style-dropdawn" id="vProductCatId" name="data[vProductCatId]" value="" required>
					</div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Name</label>
					    <input type="text" class="form-control size-style-dropdawn" id="vName" name="data[vName]" value="" required>
				    </div>
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Short Name</label>
					    <input type="text" class="form-control size-style-dropdawn" id="vShortName" name="data[vShortName]" value="" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Localized</label>
					    <input type="text" class="form-control size-style-dropdawn" id="vLocalizedId" name="data[vLocalizedId]" value="" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Has Heel Height Filter</label>
					    <select name="data[eHasHeelHeightFilter]" class="form-control size-style-dropdawn" required>
					    <option value="true">True</option>
					    <option value="false">False</option>
					    </select>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Has Color Filter</label>
					    <select name="data[eHasColorFilter]" class="form-control size-style-dropdawn" required>
					    <option value="true">True</option>
					    <option value="false">False</option>
					    </select>
					</div>
				    <input type="hidden" name="data[eDataFrom]" value="manual">
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
<?php }} ?>