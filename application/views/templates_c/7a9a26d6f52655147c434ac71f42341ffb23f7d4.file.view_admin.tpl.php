<?php /* Smarty version Smarty-3.1.11, created on 2014-08-28 23:15:42
         compiled from "application/views/templates/admin/admin_management/view_admin.tpl" */ ?>
<?php /*%%SmartyHeaderCode:182356535553f8ae1492d378-78422278%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a9a26d6f52655147c434ac71f42341ffb23f7d4' => 
    array (
      0 => 'application/views/templates/admin/admin_management/view_admin.tpl',
      1 => 1409242535,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '182356535553f8ae1492d378-78422278',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ae14959377_72066125',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ae14959377_72066125')) {function content_53f8ae14959377_72066125($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
admin_management.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Settings</h1>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
    <div class="" style="float:right;margin:0 20px 10px 0;">
        <a href="" class="admin-create btn btn-primary">Create New</a>
        <a href="" class="admin-edit btn btn-success">Edit</a>
        <a href="" class="admin-delete btn btn-danger" data-id="Delete" id="btn-delete">Delete</a>
    </div>
    <div class="col-lg-8">
                
        <div class="panel panel-default">

            <div class="panel-heading">
                <i class="fa-fw"></i> Admin List
                <div class="pull-right">
                    <div class="">
                        <!-- <a href="" class="admin-create btn btn-primary">Create New</a>
                        <a href="" class="admin-edit btn btn-success">Edit</a>
                        <a href="" class="admin-delete btn btn-danger" data-id="Delete" id="btn-delete">Delete</a> -->
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="admintable">
                                <thead>
                                    <tr>
                                        <th width="3%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId" value="1"></th>
                                        <!-- <th>Item</th> -->
                                        <th width="15%">First Name</th>
                                        <th width="15%">Last Name</th>
                                        <th width="15%">Email</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                    <input id="lefile" type="file" style="display:none">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script>
    $(document).ready(function() {
        $('.admin-create').on('click',function(){
            $.ajax({
            type     : "POST",
            cache    : false,
            url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management/create",
            success: function(data) {
                $.fancybox({
                    'width': 800,
                    'height': 400,
                    'enableEscapeButton' : false,
                    'overlayShow' : true,
                    'overlayOpacity' : 0,
                    'hideOnOverlayClick' : false,
                    'content' : data
                });
                return false;
            },
            error: function(response){
                alert('Error: Please try later.');
                return false;
            }
        });
        return false;
        });


        $('.admin-edit').on('click',function(){
            var n = $('input[name="iId[]"]:checked').length;
            if (n == 1) {
                var _getAdminId = $('input[name="iId[]"]:checked').val();
                // alert(_getAdminId);return false;
                    $.ajax({
                    type     : "POST",
                    cache    : false,
                    url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management/update?iAdminId="+_getAdminId,
                    success: function(data) {
                        $.fancybox({
                            'width': 800,
                            'height': 400,
                            'enableEscapeButton' : false,
                            'overlayShow' : true,
                            'overlayOpacity' : 0,
                            'hideOnOverlayClick' : false,
                            'content' : data
                        });
                        return false;
                    },
                    error: function(response){
                        alert('Error: Please try later.');
                        return false;
                    }
                });
            return false;
            }else{
                alert('You can not edit multiple records at a time. Please select only 1.');return false;
            }
        });


        $('.admin-delete').on('click',function(){
            // var _getAction = $(this).attr('data-id');
             var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
                $("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><button type="button" id="btn-delete" class="btn bottom-buffer" onclick="submitform();">Delete</button><div class="delete_user" style="margin-left:10px"><a  class="delete_btnimg_large" data-dismiss="modal">Cancel</a></div></div></div>').modal();
                return false;
            }
        return false;
            $.ajax({
            type     : "POST",
            cache    : false,
            url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management/action_update?iAdminId="+_getAdminId,
            success: function(data) {
                // alert('hi');return false;
                // alert(data);return false;
                window.location.reload("{$data.admin_url}admin_management");
            },
            error: function(response){
                alert('Error: Please try later.');
                return false;
            }
        });
        return false;
        });
    });

    function submitform() {
        $("#frmlist").submit();
    }
    function close_model() {
        $.modal.close();
        
    }
</script>
<?php }} ?>