<?php /* Smarty version Smarty-3.1.11, created on 2014-08-30 13:41:41
         compiled from "application/views/templates/admin/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152766759653f8ac84501102-30528803%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c5f3220bc959dcdb0f8227dba3175dd7f8fdd89' => 
    array (
      0 => 'application/views/templates/admin/header.tpl',
      1 => 1409380899,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152766759653f8ac84501102-30528803',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ac8450bed9_13551884',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ac8450bed9_13551884')) {function content_53f8ac8450bed9_13551884($_smarty_tpl) {?><nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_boostrap'];?>
img/logo-anywear.png" alt=""></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-location-arrow fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages ">
                        <li class="scrollable-menu">
                            <!-- <select name="store_detail[eDeeplinkSupport]" class="form-control"  required> -->
                            <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['data']->value['vCityName']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
                                <!-- <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCityId'];?>
" <?php if ($_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCityName']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCityName'];?>
</option> -->
                                <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
store/city?iCityId=<?php echo $_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['iCityId'];?>
" class="text-center" ><strong><?php echo $_smarty_tpl->tpl_vars['data']->value['vCityName'][$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vCityName'];?>
</strong></a>
                            <?php endfor; endif; ?>
                            </select>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
city">
                                <strong>Add City</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <?php echo $_smarty_tpl->tpl_vars['data']->value['userdata']['anywear_admin_info']['vFirstName'];?>
 <?php echo $_smarty_tpl->tpl_vars['data']->value['userdata']['anywear_admin_info']['vLastName'];?>
 <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
admin_management"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
authentication/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>
         
            <?php echo $_smarty_tpl->getSubTemplate ("admin/left.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

</nav>


<script type="text/javascript">
    function changecity(iCityId)
    {
        if(iCityId=="")
        {
            
        }
        else if(iCityId=="addnew")
        {
            
            var url=base_url+"home/create";        
            window.location=url;
        }
        else
        {
            var url=base_url+"home/city?";
            var pars="iCityId="+iCityId;
            // alert(pars);return false;
            window.location.href =url+pars;
            /*$.post(url+pars,function(data){                                                             
                // alert(data);return false;
                window.location.reload();
            });*/
        }
}
</script>
<?php }} ?>