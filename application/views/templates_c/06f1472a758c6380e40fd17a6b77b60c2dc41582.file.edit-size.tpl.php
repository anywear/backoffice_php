<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 17:34:49
         compiled from "application/views/templates/admin/size/edit-size.tpl" */ ?>
<?php /*%%SmartyHeaderCode:135513477753fc7a6040a734-09725746%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '06f1472a758c6380e40fd17a6b77b60c2dc41582' => 
    array (
      0 => 'application/views/templates/admin/size/edit-size.tpl',
      1 => 1409308487,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '135513477753fc7a6040a734-09725746',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fc7a604df8d4_47808611',
  'variables' => 
  array (
    'data' => 0,
    'eSizeType' => 0,
    'eStatus' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc7a604df8d4_47808611')) {function content_53fc7a604df8d4_47808611($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
size.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
	<div class="" style="float:right;margin:0 20px 10px 0;">
    	<a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size/create" class="store-edit btn btn-primary">Add Size</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Size
                <div class="pull-right">
                    
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
size/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iSizeId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['size']['iSizeId'];?>
'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Name of Size</label>
										<input type="text" class="form-control size-style-dropdawn" id="vSizeTitle" name="size_detail[vSizeTitle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['size']['vSizeTitle'];?>
" required>
										
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Size Type</label>
										<div class="input-prepend">
											<div class="btn-group size-style-dropdawn">
												<select class="form-control" data-toggle="dropdown" name="size_detail[eSizeType]" id="eSizeType">
					                                <option value=" ">-- Select Status --</option>
					                                <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eSizeType']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					                                <option value="<?php echo $_smarty_tpl->tpl_vars['eSizeType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eSizeType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['size']['eSizeType']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eSizeType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
					                                <?php endfor; endif; ?>
					                            </select>
											</div>
										</div>
									</div>
  
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Sorting</label>
										<div class="btn-group size-style-dropdawn">
											<select id="iSorting" class="form-control" data-toggle="dropdown" name="size_detail[iSorting]" title="order no" lang="*">
											 	<option value=''>--Select Order--</option>
                                            <?php while (($_smarty_tpl->tpl_vars['data']->value['totalRec'])>=$_smarty_tpl->tpl_vars['data']->value['initOrder']){?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder'];?>
" <?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['data']->value['size']['iSorting'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1==$_smarty_tpl->tpl_vars['data']->value['initOrder']){?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['data']->value['initOrder']++;?>
</option>
                                            <?php }?>
											</select>
										</div>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Status</label>
											<div class="btn-group size-style-dropdawn">
												<select name="size_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eStatus']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												    <option value="<?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['size']['eStatus']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eStatus']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
												<?php endfor; endif; ?>
												</select>
											</div>
									    </div>
									</div>
									<div class="form-group col-md-10" style="padding-left:0px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});

			
	});
</script>

<?php }} ?>