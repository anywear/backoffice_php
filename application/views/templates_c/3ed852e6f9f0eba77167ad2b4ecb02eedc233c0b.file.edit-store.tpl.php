<?php /* Smarty version Smarty-3.1.11, created on 2014-09-02 16:35:37
         compiled from "application/views/templates/admin/home/edit-store.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201087885553fb12a4a23707-06732494%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ed852e6f9f0eba77167ad2b4ecb02eedc233c0b' => 
    array (
      0 => 'application/views/templates/admin/home/edit-store.tpl',
      1 => 1409649277,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201087885553fb12a4a23707-06732494',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fb12a4a93d23_71279932',
  'variables' => 
  array (
    'data' => 0,
    'style' => 0,
    'store_size' => 0,
    'size_checked' => 0,
    'eSign' => 0,
    'price_checked' => 0,
    'style_looking' => 0,
    'store_category' => 0,
    'eDeeplinkSupport' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fb12a4a93d23_71279932')) {function content_53fb12a4a93d23_71279932($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
list_store.js"></script>
<script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
products.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            <?php echo $_smarty_tpl->tpl_vars['data']->value['breadcrumb'];?>

        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Store Data
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
			<div class="panel-body">
				<!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="<?php if ($_smarty_tpl->tpl_vars['data']->value['selectedTab']=='store'){?>active<?php }?>">
                    	<a href="#store" data-toggle="tab">Store</a>
                    </li>
                    <li class="<?php if ($_smarty_tpl->tpl_vars['data']->value['selectedTab']=='product'){?>active<?php }?>">
                    	<a href="#product" data-toggle="tab">Products</a>
                    </li>
                    
                    </li>
                </ul>
                <div class="tab-content">
               <!-- <div class="row">-->
               		<div class="tab-pane fade in <?php if ($_smarty_tpl->tpl_vars['data']->value['selectedTab']=='store'){?>active<?php }?>" id="store">
	                    <div class="col-lg-4" style="margin-top:10px;">
	                        <div class="table-responsive">
	                           <form class="form-horizontal" id="frmadmin" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
home/<?php echo $_smarty_tpl->tpl_vars['data']->value['function'];?>
?iStoreId=<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
" method="post" enctype="multipart/form-data">
									<fieldset>
										<input type="hidden" name="iStoreId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
'>
										<div class="form-group col-md-10" style="padding-left:15px;">
											<label for="typeahead">Name of Store</label>
											<input type="text" class="form-control" id="vStoreName" name="store_detail[vStoreName]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStoreName'];?>
" required>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Style</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" data-all="Vintage,Modern,Slipee,Banana,Evening,Wow">		<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['style']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						                                <option value="<?php echo $_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle'];?>
" <?php if ($_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['style']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyle'];?>
</option>
						                                <?php endfor; endif; ?>
													</select>
												</div>
												<input class="form-control textdropdown" id="vStyle" name="store_detail[vStyle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vStyle'];?>
" type="text" required>
											</div>
										</div>
	
										<div class="form-group col-md-10"style="padding-left:15px;">
	  										<label for="typeahead">Description</label>	
											<textarea class="form-control" rows="3" id="tDescription" name="store_detail[tDescription]" value="" required><?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tDescription'];?>
</textarea>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Opening Hour</label>
											<input type="text" class="form-control" id="vOpeningHour" name="store_detail[vOpeningHour]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vOpeningHour'];?>
"required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Phone Number</label>
											<input type="text" class="form-control" id="vPhoneNumber" name="store_detail[vPhoneNumber]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vPhoneNumber'];?>
" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Address</label>	
											<textarea class="form-control" rows="1" id="tAddress" name="store_detail[tAddress]" value="" required><?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['tAddress'];?>
</textarea>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Area</label>
											<input type="text" class="form-control" id="vArea" name="store_detail[vArea]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vArea'];?>
" required>
											
										</div>
										<!-- <div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Sizes</label>
											<div class="controls check-label">
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['store_size']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
" name="vSize[]" value="<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
" <?php echo $_smarty_tpl->tpl_vars['size_checked']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
>
												   <label for="size-<?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
"><?php echo $_smarty_tpl->tpl_vars['store_size']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vSizeTitle'];?>
</label>											
												<?php endfor; endif; ?>
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Price Avr.</label>
											<div class="controls check-label">
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eSign']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												<input type="checkbox" id="size-<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
" name="vPriceAvr[]" value="<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
" <?php echo $_smarty_tpl->tpl_vars['price_checked']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
>
												   <label for="size-<?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
"><?php echo $_smarty_tpl->tpl_vars['eSign']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['eSign'];?>
</label>											
												<?php endfor; endif; ?>
											</div>
										</div> -->
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Looking for</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" id="wearvalue" data-toggle="dropdown" data-all="Work Wear,Weekend Wear,Evening Wear,Special Event Wear,Vintage Wear">	
														<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['style_looking']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						                                <option value="<?php echo $_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName'];?>
" <?php if ($_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['style_looking']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStyleLookingName'];?>
</option>
						                                <?php endfor; endif; ?>
													</select>
												</div>
												<input class="form-control textdropdown" id="vLookingFor" name="store_detail[vLookingFor]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vLookingFor'];?>
" type="text" required>
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Want to Buy</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" id="wantBuyValue" data-toggle="dropdown" data-all="Clothing,Lingerie,Coats,Bags,Shoes">	
														<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['store_category']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
						                                <option value="<?php echo $_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName'];?>
" <?php if ($_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName']==$_smarty_tpl->tpl_vars['data']->value['size_detail']['vStyle']){?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['store_category']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]['vStoreCategoryName'];?>
</option>
						                                <?php endfor; endif; ?>
													</select>
												</div>
												<input class="form-control textdropdown" id="vWantToBuy" name="store_detail[vWantToBuy]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vWantToBuy'];?>
" type="text" required>
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Deeplink Support</label>
											<select name="store_detail[eDeeplinkSupport]" class="form-control" data-toggle="dropdown" required>
												<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eDeeplinkSupport']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
												    <option value="<?php echo $_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['store_detail']['eDeeplinkSupport']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eDeeplinkSupport']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
												<?php endfor; endif; ?>
											</select>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Host Domain</label>
											<input type="text" class="form-control" id="vHostDomain" name="store_detail[vHostDomain]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vHostDomain'];?>
" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">FB datasource</label>
											<input type="text" class="form-control" id="vFBDataSource" name="store_detail[vFBDataSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vFBDataSource'];?>
" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Instagram datasource</label>
											<input type="text" class="form-control" id="vInstagramSource" name="store_detail[vInstagramSource]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vInstagramSource'];?>
" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Main Image</label>
											<div class="store-image">
	                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage']!=''){?>
	                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/MainImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vMainImage'];?>
" width="90%;" style="margin:0 0 0 2px;"/>
	                                            <?php }else{ ?>
												<div class="input-group">
												<input type="text" class="form-control store-control" readonly="" name="vMainImage">					
												<span class="input-group-btn">
													<span class="btn btn-primary btn-file">
														Browse <input type="file" name="vMainImage">
													</span>
												</span>
												</div>
											<?php }?>
											 </div>
	                                    </div>
	                                    <div class="form-group col-md-10"style="padding-left:15px;">
	                                    	<label for="typeahead">Small Image</label>
	                                    	<div class="store-image">
	                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge']!=''){?>
	                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/SmalImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vSmallIamge'];?>
" width="90%;" style="margin:0 0 0 2px;"/>
	                                            <?php }else{ ?>
												<div class="input-group">
												<input type="text" class="form-control" readonly="" name="vSmallIamge">					
												<span class="input-group-btn">
													<span class="btn btn-primary btn-file">
														Browse <input type="file" name="vSmallIamge">
													</span>
												</span>
												</div>
											<?php }?>
											</div>
	                                    </div>
	                                    <div class="form-group col-md-10"style="padding-left:15px;">
	                                    	<label for="typeahead">Shop Image</label>
	                                    	<div class="store-image">
	                                            <?php if ($_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage']!=''){?>
	                                            	<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/store/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['iStoreId'];?>
/ShopImage/<?php echo $_smarty_tpl->tpl_vars['data']->value['store_detail']['vShopImage'];?>
" style="margin:0 0 0 2px;"/>
	                                            <?php }else{ ?>
	                                            	<div class="input-group">
														<input type="text" class="form-control" readonly="" name="vShopImage">					
														<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse <input type="file" name="vShopImage">
														</span>
														</span>
													</div>
	                                            <?php }?>
	                                        </div>
	                                        	  
	                                            
	                                    </div>
	
										<div class="form-group col-md-10" style="padding-left:15px;">
											<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
											<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
											
										</div>
									</fieldset>
								</form>
	                        </div>
	                    </div>
                     </div>
                     <div class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['data']->value['selectedTab']=='product'){?> in active<?php }?>" id="product">
                     	<input type="hidden" id="iStoreId" value='<?php echo $_smarty_tpl->tpl_vars['data']->value['iStoreId'];?>
'>
                     	<div class="panel-body">
							<div class="" style="float:right;margin:0 20px 10px 0;">
							    <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
products/create?iStoreId=<?php echo $_smarty_tpl->tpl_vars['data']->value['iStoreId'];?>
" class="btn btn-primary">Add Product</a>
							</div>
							<div class="col-lg-8">
							    <div class="panel panel-default">
							        <div class="panel-heading">
							            <i class="fa-fw"></i> Product Listing
							            <div class="pull-right">
							                
							            </div>
							        </div>
							        <!-- /.panel-heading -->
							        <div class="panel-body">
							            <div class="row">
							                <div class="col-lg-4">
							                    <div class="table-responsive">
							                        <table class="table table-hover" id="producttable">
							                            <thead>
							                                <tr>
							                                    <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
							                                    <th width="35%">Name of Product</th>
							                                    <th width="15%">Added Type</th>
							                                    <th width="15%">Date</th>
							                                    <th width="20%">Control</th>
							                                </tr>
							                            </thead>
							                        </table>
							                    </div>
							                </div>
							                <div class="col-lg-8">
							                    <div id="morris-bar-chart"></div>
							                </div>
							            </div>
							        </div>
							    </div>
							</div>
                     	</div>
                     </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});
	});

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>

<?php }} ?>