<?php /* Smarty version Smarty-3.1.11, created on 2014-08-29 19:02:27
         compiled from "application/views/templates/admin/slider/edit_slider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:137819110653fc5862a472c7-83027212%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55a04d80cf08b3011d9f188156e7de8a59f30793' => 
    array (
      0 => 'application/views/templates/admin/slider/edit_slider.tpl',
      1 => 1409313745,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '137819110653fc5862a472c7-83027212',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53fc5862ab84e7_84625912',
  'variables' => 
  array (
    'data' => 0,
    'eSliderType' => 0,
    'eStatuses' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53fc5862ab84e7_84625912')) {function content_53fc5862ab84e7_84625912($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
slider.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class=""></h1>
    </div>
</div>

<div class="row">
	<div class="btn-group" style="float:right;margin:0 20px 10px 0;">
        <a href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
slider/create" class="notify-create btn btn-primary">Add New Slider</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Slider
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body edit_personalization">
            	<div class="row">
            		<div class="col-lg-8">
	            		<form role="form" action="" method="POST" enctype="multipart/form-data">
				    <input type="hidden" name="data[iSliderId]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['iSliderId'];?>
">
				    <!--<label class="col-md-2">Slider Type</label>
				    <div class="form-group col-md-10">
					<select name="data[eSliderType]" class="form-control" required>
					<option value="">Select slider type</option>
					<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eSliderType']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
					    <option value="<?php echo $_smarty_tpl->tpl_vars['eSliderType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eSliderType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['slider']['eSliderType']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eSliderType']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
					<?php endfor; endif; ?>
					</select>
				    </div>-->
				    <div class="form-group col-md-10"style="padding-left:15px;">
					    <label class="col-md-2">Title</label>
					    <input type="text" class="form-control" name="data[vTitle]" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['vTitle'];?>
" required="">
				    </div>
				    
				    <div class="form-group col-md-10"style="padding-left:15px;">
					    <label class="col-md-2">Description</label>
					    
						<textarea class="form-control" name="data[tDescription]" required=""><?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['tDescription'];?>
</textarea>
				    </div>
				    
				    <div class="form-group col-md-10"style="padding-left:15px;">
				    	<label class="col-md-2">Slider Image</label>
				    	
						<?php if ($_smarty_tpl->tpl_vars['data']->value['slider']['vImage']!=''){?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_base_url'];?>
uploads/slider/<?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['iSliderId'];?>
/<?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['vImage'];?>
" width="150" height="150" style="padding-bottom: 10px;" ><br />
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="margin:0 0 10px 135px;">Delete</a>
						<?php }?>
							<div class="input-group">
							<input type="text" class="form-control" readonly="" name="vImage">					
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">
									Browse <input type="file" name="vImage">
								</span>
							</span>
							</div>
						<!-- <input type="file" name="vImage" required=""> -->
				    </div>
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="col-md-2">Status</label>
					    
							<select name="data[eStatus]" class="form-control" required>
								<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['i'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['i']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['name'] = 'i';
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['eStatuses']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['i']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['i']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['i']['total']);
?>
								    <option value="<?php echo $_smarty_tpl->tpl_vars['eStatuses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
" <?php if ($_smarty_tpl->tpl_vars['eStatuses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']]==$_smarty_tpl->tpl_vars['data']->value['slider']['eStatus']){?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['eStatuses']->value[$_smarty_tpl->getVariable('smarty')->value['section']['i']['index']];?>
</option>
								<?php endfor; endif; ?>
							</select>
				    </div>
					    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <input type="submit" class="btn btn-primary" value="Save changes" />
					    <button type="button" class="btn btn-default" onclick="returnme();" >Cancel</button>
				    </div>
				</form>
		            </div>
		        </div>
        	</div>
    	</div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Delete</h4>
	    </div>
	    <div class="modal-body">
		Are you sure to delete the Image?
	    </div>
	    <div class="modal-footer">
		<a class="btn btn-default" data-dismiss="modal">No</a>
		<a class="btn btn-default" style="float:right;" href="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
slider/deleteicon?id=<?php echo $_smarty_tpl->tpl_vars['data']->value['slider']['iSliderId'];?>
">Yes</a>
	    </div>
	</div>
    </div>
</div>
			    
			    

<script type="text/javascript">
	$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script><?php }} ?>