<?php /* Smarty version Smarty-3.1.11, created on 2014-08-28 23:45:52
         compiled from "application/views/templates/admin/user/view_user.tpl" */ ?>
<?php /*%%SmartyHeaderCode:211980022053f8ad985fc532-21742179%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '761c401d46abf940a865efcd40a9f1fe377c77f4' => 
    array (
      0 => 'application/views/templates/admin/user/view_user.tpl',
      1 => 1409244351,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '211980022053f8ad985fc532-21742179',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_53f8ad98624ef6_86586395',
  'variables' => 
  array (
    'data' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53f8ad98624ef6_86586395')) {function content_53f8ad98624ef6_86586395($_smarty_tpl) {?><script src="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_js_path'];?>
user.js"></script>
<form name="frmlist" id="frmlist" action="<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
user/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registered Users</h1>
    </div>
</div>
<?php if ($_smarty_tpl->tpl_vars['data']->value['message']!=''){?>
<div class="span12">
    <div class="alert alert-info">
        <?php echo $_smarty_tpl->tpl_vars['data']->value['message'];?>

    </div>
</div>
<?php }?>
<div class="row">
    <div class="" style="float:right;margin:0 20px 10px 0;"> 
                        <a href="" class="user-profile btn btn-info">User Profile</a>
                        <a href="" class="send-mail btn btn-success">Send Mail</a>
                       
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> User List
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="userlistId">
                                <thead>
                                <tr>
                                    <th width="1%"><input type="checkbox" id="check_all" name="check_all" value="1"></th>
                                    <th width="15%">First Name</th>
                                    <th width="20%">Last Name</th>
                                    <th width="10%">Email</th>
                                </tr>
                                </thead>
                            </table>  
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>           
</form>

<script>
    $(document).ready(function() {
        $('.user-profile').on('click',function(){
            var n = $('input[name="iId[]"]:checked').length;
            if(n==1){
                var _getUserId = $('input[name="iId[]"]:checked').val();
                $.ajax({
                type     : "POST",
                cache    : false,
                url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
user/view_users?iUserId="+_getUserId,
                success: function(data){
                    $.fancybox({
                        'width': 800,
                        'height':400,
                        'enableEscapeButton' : false,
                        'overlayShow' : true,
                        'overlayOpacity' : 0,
                        'hideOnOverlayClick' : false,
                        'content' : data
                    });
                    return false;
                },
                error: function(response){
                    alert('Error: Please try later.');
                    return false;
                }
            });
            return false;    
            }else{
                alert('You can not view multiple records at a time. Please select only 1.');return false;
            }
            /*
            */
    });
        $('.send-mail').on('click',function(){
            var n = $( "input:checked" ).length;
            if (n == 1) {
                var _getUserId = $('input[name="iId[]"]:checked').val();
                $.ajax({
                type     : "POST",
                cache    : false,
                url      : "<?php echo $_smarty_tpl->tpl_vars['data']->value['admin_url'];?>
user/send_mail?iUserId="+_getUserId,
                success: function(data) {
                    window.location.reload("{$data.admin_url}user");
                },
                error: function(response){
                    alert('Error: Please try later.');
                    return false;
                }
                });
                return false;
                
            }else{
                alert('You can not multiple send mail at a time. Please select only 1.');return false;
            }
        });    
    });
</script>

<?php }} ?>