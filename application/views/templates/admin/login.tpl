<!DOCTYPE html>
<html class="no-js">
    <head>
        <title>::: AnyWear :::</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
        <!-- Bootstrap -->
        <link href="{$data.admin_boostrap}css/login_bootstrap.min.css" rel="stylesheet">
        <meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!--<link href="{$data.admin_boostrap}css/bootstrap.css" rel="stylesheet" media="screen">-->
        <!--<link href="{$data.admin_boostrap}css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">-->
        
        <style>
			.modal-footer {   border-top: 0px; float: left; width: 100%; }
			.modal-content { float: left; width: 100%; }
		</style>
        
        
    </head>
    <body>
		<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
		  <div class="modal-dialog">
		  <div class="modal-content">
		      <div class="modal-header">
		          <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
		          <h1 class="text-center">Login</h1>
		      </div>
		      <div class="modal-body">
		      		{if {$data.message} neq ""}
		  				<div class="alert alert-danger">
		                    {$data.message}
		                </div>
	                {/if}
		          <form class="form col-md-12 center-block" id="frmsignin" role="form" action="{$data.admin_url}authentication" method="post">
		            <div class="form-group">
		              <input type="text" class="form-control input-lg" placeholder="Email" name="vEmail" id="vEmail">
		            </div>
		            <div class="form-group">
		              <input type="password" class="form-control input-lg" placeholder="Password" name="vPassword" id="vPassword">
		            </div>
		            <div class="form-group">
		              <button class="btn btn-primary btn-lg btn-block">Sign In</button>
		            </div>
		          </form>
		      </div>
		      <div class="modal-footer">
		          <div class="col-md-12">
		          	<!--<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>-->
				  </div>
		      </div>
		  </div>
		  </div>
		</div>
        <!--<a style="background:none; border:none; box-shadow:none;margin-top: 10px;width:97%;" class="brand-logo bottom-buffer" href="#"></a>
        <div class="container">
            <div class="row">
                {if $data.message neq ''}
                <div class="span4" style="margin:0px auto;float:none;">
                    <div class="alert alert-info">
                        {$data.message}
                    </div>
                </div>
                {/if}
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title form-signin-heading">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
            
                        <form  role="form" class="form-signin" id="frmsignin" action="{$data.admin_url}authentication" method="post">
                            
                            <div class="form-group">
                                <input type="text" class="input-block-level" placeholder="Email" maxlength="255" name="vEmail" id="vEmail">
                            </div>
                            <div class="form-group">
                                <input type="password" class="input-block-level" placeholder="Password" maxlength="255" name="vPassword" id="vPassword">
                            </div>
                            <div class="checkbox">
                                <label class="checkbox" style="width:69%; float:left;">

                                    <input type="checkbox" value="remember-me"> Remember me
                                </label>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>-->
        <script src="{$data.admin_boostrap}js/jquery-1.11.0.js"></script>
        <script src="{$data.admin_boostrap}js/login_bootstrap.min.js"></script>
    </body>
</html>



