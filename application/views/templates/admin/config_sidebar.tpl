<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse top-buffer2">
    <li {if $data.name eq 'update'} class="active" {/if}>
      
        <a href="{$data.admin_url}driver/update/{$data.iDriverId}">
    
        
            <i class="icon-chevron-right"></i>Driver</a>
    </li>
    <li {if $data.name eq 'company_info'} class="active" {/if}>
        <a href="{$data.admin_url}driver/company_info/{$data.iDriverId}"><i class="icon-chevron-right"></i>Company Information</a>
    </li>
    <li {if $data.name eq 'licence_info'} class="active" {/if}>
        <a href="{$data.admin_url}driver/licence_info/{$data.iDriverId}"><i class="icon-chevron-right"></i>Licence Information</a>
    </li>
    <li {if $data.name eq 'vehicle'} class="active" {/if}>
        <a href="{$data.admin_url}driver_vehicle/vehiclelist/{$data.iDriverId}"><i class="icon-chevron-right"></i>Vehicle Information</a>
    </li>
    <li {if $data.name eq 'subdriver'} class="active" {/if}>

        <a href="{$data.admin_url}subdriver/subdriverlist/{$data.iDriverId}"><i class="icon-chevron-right"></i>Sub Drivers</a>
    </li>
</ul>