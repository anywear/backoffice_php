<script src="{$data.admin_js_path}size.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
	<div class="" style="float:right;margin:0 20px 10px 0;">
    	<a href="{$data.admin_url}size/create" class="store-edit btn btn-primary">Add Size</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Size
                <div class="pull-right">
                    
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}size/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iSizeId" value='{$data.size.iSizeId}'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Name of Size</label>
										<input type="text" class="form-control size-style-dropdawn" id="vSizeTitle" name="size_detail[vSizeTitle]" value="{$data.size.vSizeTitle}" required>
										
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Size Type</label>
										<div class="input-prepend">
											<div class="btn-group size-style-dropdawn">
												<select class="form-control" data-toggle="dropdown" name="size_detail[eSizeType]" id="eSizeType">
					                                <option value=" ">-- Select Status --</option>
					                                {section name=i loop=$eSizeType}
					                                <option value="{$eSizeType[i]}" {if $eSizeType[i] eq $data.size['eSizeType']} selected="selected"{/if}>{$eSizeType[i]}</option>
					                                {/section}
					                            </select>
											</div>
										</div>
									</div>
  
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Sorting</label>
										<div class="btn-group size-style-dropdawn">
											<select id="iSorting" class="form-control" data-toggle="dropdown" name="size_detail[iSorting]" title="order no" lang="*">
											 	<option value=''>--Select Order--</option>
                                            {while ($data.totalRec) >= $data.initOrder}
                                            <option value="{$data.initOrder}" {if {$data.size['iSorting']} eq $data.initOrder}selected{/if}>{$data.initOrder++}</option>
                                            {/while}
											</select>
										</div>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Status</label>
											<div class="btn-group size-style-dropdawn">
												<select name="size_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												{section name=i loop=$eStatus}
												    <option value="{$eStatus[i]}" {if $eStatus[i] eq $data.size.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
												{/section}
												</select>
											</div>
									    </div>
									</div>
									<div class="form-group col-md-10" style="padding-left:0px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}
