<script src="{$data.admin_js_path}money_spend.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Price Data
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}money_spend/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iSpendId" value='{$data.size.iSpendId}'>
									<div class="form-group col-md-10" style="padding-left:15px;">
											<label class="control-label" for="typeahead">Name of Money Spend</label>
											<input type="text" class="form-control size-style-dropdawn" id="vTitle" name="money_detail[vTitle]" value="{$data.size.vTitle}" required>
										
									</div>
									 <div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Moeny Type</label>
										<div class="input-prepend">
											<div class="btn-group size-style-dropdawn">
												<select class="form-control" data-toggle="dropdown" name="money_detail[eSign]" id="eSign">
					                                <option value=" ">-- Select Status --</option>
					                                {section name=i loop=$eSign}
					                                <option value="{$eSign[i]}" {if $eSign[i] eq $data.size['eSign']} selected="selected"{/if}>{$eSign[i]}</option>
					                                {/section}
					                            </select>
											</div>
										</div>
									</div>
  									 <div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Sorting</label>
										<div class="btn-group size-style-dropdawn">
											<select id="iSorting" class="form-control" data-toggle="dropdown" name="money_detail[iSorting]" title="order no" lang="*">
											 	<option value=''>--Select Order--</option>
                                            {while ($data.totalRec) >= $data.initOrder}
                                            <option value="{$data.initOrder}" {if {$data.size['iSorting']} eq $data.initOrder}selected{/if}>{$data.initOrder++}</option>
                                            {/while}
											</select>
											
										</div>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Status</label>
									    <div class="btn-group size-style-dropdawn">
												<select name="money_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												{section name=i loop=$eStatus}
												    <option value="{$eStatus[i]}" {if $eStatus[i] eq $data.size.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
												{/section}
												</select>
										</div>
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}
