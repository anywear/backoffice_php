<script src="{$data.admin_js_path}money_spend.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Price Data
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}money_spend/{$data.function}" method="post" enctype="multipart/form-data">
                                <fieldset>
                                    <!-- <input type="hidden" name="iSizeId" value='{$data.money_detail.iSizeId}'> -->
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Name of Money Spend</label>
                                        <input type="text" class="form-control" id="vTitle" name="money_detail[vTitle]" value="{$data.money_detail.vTitle}" required>
                                    </div>
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Money Type</label>
                                        <div class="input-prepend">
                                            <div class="btn-group size-style-dropdawn">
                                                <select class="form-control" data-toggle="dropdown" name="money_detail[eSign]" id="eSign">
                                                    <option value=" ">-- Select Type --</option>
                                                    {section name=i loop=$eSign}
                                                    <option value="{$eSign[i]}" {if $eSign[i] eq $data.money_detail['eSign']} selected="selected"{/if}>{$eSign[i]}</option>
                                                    {/section}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Sorting</label>
                                        <div class="input-prepend">
                                            <div class="btn-group size-style-dropdawn">
                                                <select id="iSorting" class="form-control" data-toggle="dropdown" name="money_detail[iSorting]" title="order no" lang="*">
                                                    <option value=''>--Select Order--</option>
                                                    {while ($data.totalRec+1) >= $data.initOrder}
                                                    <option value="{$data.initOrder}" >{$data.initOrder++}</option>
                                                    {/while}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
                                        <button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
<script type="text/javascript">
var abc = 0;
    $(document).ready(function(){
        $('#stylevalue').on('change',function(){
            var _selStyleVal = $(this).val();
            var text_value = $("#vStyle").val();
            if(text_value.match(_selStyleVal)){
                return false;
            }
            
            var _newStr = text_value;
            if(text_value != '' && _selStyleVal != ''){
                _newStr = text_value+','+_selStyleVal;
            }else if(_selStyleVal != ''){
                _newStr = _selStyleVal;
            }
            if(_selStyleVal == 'All'){
                _newStr = $(this).attr('data-all');
            }
            $("#vStyle").val(_newStr);
        });             

        $('#wearvalue').on('change',function(){
            var _selStyleVal = $(this).val();
            var text_value = $("#vLookingFor").val();
            if(text_value.match(_selStyleVal)){
                return false;
            }
            var _newStr = text_value;
            if(text_value != '' && _selStyleVal != ''){
                _newStr = text_value+','+_selStyleVal;
            }else if(_selStyleVal != ''){
                _newStr = _selStyleVal;
            }
            if(_selStyleVal == 'All'){
                _newStr = $(this).attr('data-all');
            }
            $("#vLookingFor").val(_newStr);
        });

        $('#wantBuyValue').on('change',function(){
            var _selStyleVal = $(this).val();
            // alert(_selStyleVal);return false;
            var text_value = $("#vWantToBuy").val();
            if(text_value.match(_selStyleVal)){
                return false;
            }
            var _newStr = text_value;
            if(text_value != '' && _selStyleVal != ''){
                _newStr = text_value+','+_selStyleVal;
            }else if(_selStyleVal != ''){
                _newStr = _selStyleVal;
            }
            if(_selStyleVal == 'All'){
                _newStr = $(this).attr('data-all');
            }
            $("#vWantToBuy").val(_newStr);
        });

            
    });
</script>
{/literal}
