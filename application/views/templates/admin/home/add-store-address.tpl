<script src="{$data.admin_js_path}list_store.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Store Data
                <div class="pull-right">
                    <div class="btn-group">
                        <!--<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}home/{$data.function}?iStoreId={$data.store_detail.iStoreId}" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iStoreId" value='{$data.store_detail.iStoreId}'>
									<div class="form-group">
										<div class="store-label">
											<label class="control-label" for="typeahead">Name of Store</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vStoreName" name="store_detail[vStoreName]" value="{$data.store_detail.vStoreName}" required>
										</div>
									</div>
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Style</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group style-dropdawn">
												<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" data-all="Vintage,Modern,Slipee,Banana,Evening,Wow">				<option value="">Menu</option>
													<option value="Vintage">Vintage</option>
													<option value="Modern">Modern</option>
													<option value="Slipee">Slipee</option>
													<option value="Banana">Banana</option>
													<option value="Evening">Evening</option>
													<option value="Wow">Wow</option>
													<option class="divider"></option>
													<option value="All" >All</option>
												</select>
											</div> -->
											<input class="form-control addtextstyle" id="vStyle" name="store_detail[vStyle]" value="{$data.store_detail.vStyle}" type="text" required>
										</div>
									</div>
  
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Description</label>
										</div>
										<div class="controls">
											<textarea class="form-control textcss" rows="3" id="tDescription" name="store_detail[tDescription]" value="" required>{$data.store_detail.tDescription}</textarea>
										</div>
									</div>

									<div class="form-group">
									<div class="store-label">
										<label class="control-label" for="typeahead">Opening Hour</label>
									</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vOpeningHour" name="store_detail[vOpeningHour]" value="{$data.store_detail.vOpeningHour}"required>
										</div>
									</div>
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Phone Number</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vPhoneNumber" name="store_detail[vPhoneNumber]" value="{$data.store_detail.vPhoneNumber}" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Address</label>
										</div>
										<div class="controls">
											<textarea class="form-control textcss" rows="1" id="tAddress" name="store_detail[tAddress]" value="" required>{$data.store_detail.tAddress}</textarea>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Area</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vArea" name="store_detail[vArea]" value="{$data.store_detail.vArea}" required>
										</div>
									</div>
									
									
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Sizes</label>
										</div>
										<div class="controls check-label">
											{section name=i loop=$sizearray}
											<input type="checkbox" id="size-{$sizearray[i]}" name="vSize[]" value="{$sizearray[i]}"  {$size_checked[i]}>
											   <label for="size-{$sizearray[i]}">{$sizearray[i]}</label>											
											{/section}
										</div>
									</div>
									
									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Price Avr.</label>
										</div>
										<div class="controls check-label">
											{section name=i loop=$pricearray}
											<input type="checkbox" id="size-{$pricearray[i]}" name="vPriceAvr[]" value="{$pricearray[i]}"  {$price_checked[i]}>
											   <label for="size-{$pricearray[i]}">{$pricearray[i]}</label>											
											{/section}
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Looking for</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group ">
												<select class="form-control" id="wearvalue" data-toggle="dropdown" data-all="Work Wear,Weekend Wear,Evening Wear,Special Event Wear,Vintage Wear">	
													<option value="">Menu</option>
													<option value="Work Wear">Work Wear</option>
													<option value="Weekend Wear">Weekend Wear</option>
													<option value="Evening Wear">Evening Wear</option>
													<option value="Special Event Wear">Special Event Wear</option>
													<option value="Vintage Wear">Vintage Wear</option>
													<option class="divider"></option>
													<option value="All">All</option>
												</select>
											</div> -->
											<input class="form-control addtextlooking" id="vLookingFor" name="store_detail[vLookingFor]" value="{$data.store_detail.vLookingFor}" type="text" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Want to Buy</label>
										</div>
										<div class="input-prepend">
											<!-- <div class="btn-group style-dropdawn">
												<select class="form-control" id="wantBuyValue" data-toggle="dropdown" data-all="Clothing,Lingerie,Coats,Bags,Shoes">	
													<option value="">Menu</option>
													<option value="Clothing">Clothing</option>
													<option value="Lingerie">Lingerie</option>
													<option value="Coats">Coats</option>
													<option value="Bags">Bags</option>
													<option value="Shoes">Shoes</option>
													<option class="divider"></option>
													<option value="All">All</option>
												</select>
											</div> -->
											<input class="form-control addtextbuy" id="vWantToBuy" name="store_detail[vWantToBuy]" value="{$data.store_detail.vWantToBuy}" type="text" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">FB datasource</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vFBDataSource" name="store_detail[vFBDataSource]" value="{$data.store_detail.vFBDataSource}" required>
										</div>
									</div>

									<div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Instagram datasource</label>
										</div>
										<div class="controls">
											<input type="text" class="form-control textcss" id="vInstagramSource" name="store_detail[vInstagramSource]" value="{$data.store_detail.vInstagramSource}" required>
										</div>
									</div>

									<div class="form-group">
											<div class="store-label">
                                            <label class="control-label" for="typeahead">Main Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            {if $data.store_detail.vMainImage neq ''}
                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/MainImage/{$data.store_detail.vMainImage}" width="90%;" style="margin:0 0 0 2px;"/>
                                            {/if}
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vMainImage" name="vMainImage" data-icon="false"  value="{$data.store_detail.vMainImage}">
                                            </div>
                                    </div>

                                    <div class="form-group">
                                    		<div class="store-label">
                                            <label class="control-label" for="typeahead">Small Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            {if $data.store_detail.vSmallIamge neq ''}
                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/SmalImage/{$data.store_detail.vSmallIamge}" width="90%;" style="margin:0 0 0 2px;"/>
                                            {/if}
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vSmallIamge" name="vSmallIamge" data-icon="false"  value="{$data.store_detail.vSmallIamge}">
                                            </div>
                                    </div>

                                    <div class="form-group">
                                    		<div class="store-label">
                                            <label class="control-label" for="typeahead">Shop Image</label>
                                            </div>
                                            <div class="controls store-image">
                                            {if $data.store_detail.vShopImage neq ''}
                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/ShopImage/{$data.store_detail.vShopImage}" style="margin:0 0 0 2px;"/>
                                            {/if}
                                            </div>
                                            <div class="store-label">
                                            <label class="control-label" for="typeahead"></label>
                                            </div>
                                            <div>
                                            	<input type="file" class="filestyle" id="vShopImage" name="vShopImage" data-icon="false"  value="{$data.store_detail.vShopImage}">
                                            </div>
                                    </div>

									<div class="form-group" style="float:right;margin-right:10px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});
	});
</script>
{/literal}
