<script src="{$data.admin_js_path}store_city.js"></script>
<form name="frmlist" id="frmlist" action="{$data.admin_url}store/action_update" method="post">
<input type="hidden" name="action" id="action">
<input type="hidden" name="action" id="iCityId" value="{$data.iCityId}">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Store List</h1>
    </div>
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="{$data.admin_url}store/create" class="btn btn-primary">Add Store</a>
                        <a href="javascript:;" class="btn btn-primary store-edit">Edit Store</a>
                        <a href="javascript:;" class="btn btn-success on-sale">On Sale</a>
                        <!-- <a href="javascript:;" class="btn btn-info add-address">Add Address</a> -->
                        <a href="" class="btn btn-danger store-delete">Delete Store</a>
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Store DataTables Advanced
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" id="citytable">
                                <thead>
                                    <tr>
                                        <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
                                        <th width="5%"><span class="fa fa-star-o fa-fw"></span></th>
                                        <th width="35%">Name of Store</th>
                                        <th width="15%">Date</th>
                                        <th width="20%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
{literal}
<script type="text/javascript">
$(document).ready(function(){
    $('.store-edit').on('click',function(){
            var n = $('input[name="iId[]"]:checked').length;
            if (n == 1) {
                var _getStoreId = $('input[name="iId[]"]:checked').val();
                window.location.href ='{/literal}{$data.admin_url}{literal}store/update?iStoreId='+_getStoreId;
            }else{
                alert('You can not edit multiple records at a time. Please select only 1.');return false;
            }
        });

    $('.store-delete').on('click',function(){
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true)
        {
            $("#action").val("Delete");
                    $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><button type="button" class="btn bottom-buffer" onclick="submitform();">Delete</button><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn bottom-buffer" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                    return false;
        }
        return false;
    });

    $('.on-sale').on('click',function(){
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true)
        {
            $("#action").val("Yes");
            
                    $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Store On Sale</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to Store On Sale?</h3></div><div class="view_del_user"><button type="button" class="btn bottom-buffer" onclick="submitform();">Confirm</button><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn bottom-buffer" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                    return false;
        }
        return false;
    });

    $('.add-address').on('click',function(){
            var n = $('input[name="iId[]"]:checked').length;
            if (n == 1) {
                var _getStoreId = $('input[name="iId[]"]:checked').val();
                window.location.href ='{/literal}{$data.admin_url}{literal}store/store_add_address?iStoreId='+_getStoreId;
            }else{
                alert('You can not edit multiple records at a time. Please select only 1.');return false;
            }
        });

});
function submitform() {
    $("#frmlist").submit();
    }
</script>
{/literal}