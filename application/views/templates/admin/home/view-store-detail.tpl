<script src="{$data.admin_js_path}list_store.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Store List</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Store DataTables Advanced {if $data.getStoreDetail.eOnSale eq 'Yes'}
                <button type="button" class="btn btn-success" style="margin-left:30px;">On Sale</button>{/if}
                <div class="pull-right">
                    <!-- <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="span12">
                                <div class="block">
                                    <div class="block-content collapse in">
                                        <!--  -->
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Name of Store </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vStoreName}
                                            </div>
                                        </div>
                                         <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Style </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vStyle}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Description </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.tDescription}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Opening Hour </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vOpeningHour}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Phone Number </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vPhoneNumber}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Address </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.tAddress}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Area </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vArea}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Sizes </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vSize}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">PriceAvr </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vPriceAvr}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Looking for </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vLookingFor}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Want to Buy </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vWantToBuy}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">FB Datasource </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vFBDataSource}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Instagram Datasource </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getStoreDetail.vInstagramSource}
                                            </div>
                                        </div>

                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Main Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            {if $data.getStoreDetail.vMainImage neq ''}
                                                <img src="{$data.admin_base_url}uploads/store/{$data.getStoreDetail.iStoreId}/MainImage/{$data.getStoreDetail.vMainImage}" width="90%;" style="margin:0 0 0 0px;"/>
                                            {/if}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Small Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            {if $data.getStoreDetail.vSmallIamge neq ''}
                                                <img src="{$data.admin_base_url}uploads/store/{$data.getStoreDetail.iStoreId}/SmalImage/{$data.getStoreDetail.vSmallIamge}" width="90%;" style="margin:0 0 0 0px;" />
                                            {/if}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Shop Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            {if $data.getStoreDetail.vShopImage neq ''}
                                                <img src="{$data.admin_base_url}uploads/store/{$data.getStoreDetail.iStoreId}/ShopImage/{$data.getStoreDetail.vShopImage}" width="90%;" style="margin:0 0 0 0px;"/>
                                            {/if}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}