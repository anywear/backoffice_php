<script src="{$data.admin_js_path}list_store.js"></script>
<script src="{$data.admin_js_path}products.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Store Data
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
			<div class="panel-body">
				<!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="{if $data.selectedTab eq 'store'}active{/if}">
                    	<a href="#store" data-toggle="tab">Store</a>
                    </li>
                    <li class="{if $data.selectedTab eq 'product'}active{/if}">
                    	<a href="#product" data-toggle="tab">Products</a>
                    </li>
                    
                    </li>
                </ul>
                <div class="tab-content">
               <!-- <div class="row">-->
               		<div class="tab-pane fade in {if $data.selectedTab eq 'store'}active{/if}" id="store">
	                    <div class="col-lg-4" style="margin-top:10px;">
	                        <div class="table-responsive">
	                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}home/{$data.function}?iStoreId={$data.store_detail.iStoreId}" method="post" enctype="multipart/form-data">
									<fieldset>
										<input type="hidden" name="iStoreId" value='{$data.store_detail.iStoreId}'>
										<div class="form-group col-md-10" style="padding-left:15px;">
											<label for="typeahead">Name of Store</label>
											<input type="text" class="form-control" id="vStoreName" name="store_detail[vStoreName]" value="{$data.store_detail.vStoreName}" required>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Style</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" data-all="Vintage,Modern,Slipee,Banana,Evening,Wow">		{section name=i loop=$style}
						                                <option value="{$style[i].vStyle}" {if $style[i].vStyle eq $data.size_detail['vStyle']} selected="selected"{/if}>{$style[i].vStyle}</option>
						                                {/section}
													</select>
												</div>
												<input class="form-control textdropdown" id="vStyle" name="store_detail[vStyle]" value="{$data.store_detail.vStyle}" type="text" required>
											</div>
										</div>
	
										<div class="form-group col-md-10"style="padding-left:15px;">
	  										<label for="typeahead">Description</label>	
											<textarea class="form-control" rows="3" id="tDescription" name="store_detail[tDescription]" value="" required>{$data.store_detail.tDescription}</textarea>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Opening Hour</label>
											<input type="text" class="form-control" id="vOpeningHour" name="store_detail[vOpeningHour]" value="{$data.store_detail.vOpeningHour}"required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Phone Number</label>
											<input type="text" class="form-control" id="vPhoneNumber" name="store_detail[vPhoneNumber]" value="{$data.store_detail.vPhoneNumber}" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Address</label>	
											<textarea class="form-control" rows="1" id="tAddress" name="store_detail[tAddress]" value="" required>{$data.store_detail.tAddress}</textarea>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Area</label>
											<input type="text" class="form-control" id="vArea" name="store_detail[vArea]" value="{$data.store_detail.vArea}" required>
											
										</div>
										<!-- <div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Sizes</label>
											<div class="controls check-label">
												{section name=i loop=$store_size}
												<input type="checkbox" id="size-{$store_size[i].vSizeTitle}" name="vSize[]" value="{$store_size[i].vSizeTitle}" {$size_checked[i]}>
												   <label for="size-{$store_size[i].vSizeTitle}">{$store_size[i].vSizeTitle}</label>											
												{/section}
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Price Avr.</label>
											<div class="controls check-label">
												{section name=i loop=$eSign}
												<input type="checkbox" id="size-{$eSign[i].eSign}" name="vPriceAvr[]" value="{$eSign[i].eSign}" {$price_checked[i]}>
												   <label for="size-{$eSign[i].eSign}">{$eSign[i].eSign}</label>											
												{/section}
											</div>
										</div> -->
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Looking for</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" id="wearvalue" data-toggle="dropdown" data-all="Work Wear,Weekend Wear,Evening Wear,Special Event Wear,Vintage Wear">	
														{section name=i loop=$style_looking}
						                                <option value="{$style_looking[i].vStyleLookingName}" {if $style_looking[i].vStyleLookingName eq $data.size_detail['vStyle']} selected="selected"{/if}>{$style_looking[i].vStyleLookingName}</option>
						                                {/section}
													</select>
												</div>
												<input class="form-control textdropdown" id="vLookingFor" name="store_detail[vLookingFor]" value="{$data.store_detail.vLookingFor}" type="text" required>
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Want to Buy</label>
											<div class="input-prepend">
												<div class="btn-group style-dropdawn">
													<select class="form-control" id="wantBuyValue" data-toggle="dropdown" data-all="Clothing,Lingerie,Coats,Bags,Shoes">	
														{section name=i loop=$store_category}
						                                <option value="{$store_category[i].vStoreCategoryName}" {if $store_category[i].vStoreCategoryName eq $data.size_detail['vStyle']} selected="selected"{/if}>{$store_category[i].vStoreCategoryName}</option>
						                                {/section}
													</select>
												</div>
												<input class="form-control textdropdown" id="vWantToBuy" name="store_detail[vWantToBuy]" value="{$data.store_detail.vWantToBuy}" type="text" required>
											</div>
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Deeplink Support</label>
											<select name="store_detail[eDeeplinkSupport]" class="form-control" data-toggle="dropdown" required>
												{section name=i loop=$eDeeplinkSupport}
												    <option value="{$eDeeplinkSupport[i]}" {if $eDeeplinkSupport[i] eq $data.store_detail.eDeeplinkSupport}selected="selected"{/if}>{$eDeeplinkSupport[i]}</option>
												{/section}
											</select>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Host Domain</label>
											<input type="text" class="form-control" id="vHostDomain" name="store_detail[vHostDomain]" value="{$data.store_detail.vHostDomain}" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">FB datasource</label>
											<input type="text" class="form-control" id="vFBDataSource" name="store_detail[vFBDataSource]" value="{$data.store_detail.vFBDataSource}" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Instagram datasource</label>
											<input type="text" class="form-control" id="vInstagramSource" name="store_detail[vInstagramSource]" value="{$data.store_detail.vInstagramSource}" required>
											
										</div>
										<div class="form-group col-md-10"style="padding-left:15px;">
											<label for="typeahead">Main Image</label>
											<div class="store-image">
	                                            {if $data.store_detail.vMainImage neq ''}
	                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/MainImage/{$data.store_detail.vMainImage}" width="90%;" style="margin:0 0 0 2px;"/>
	                                            {else}
												<div class="input-group">
												<input type="text" class="form-control store-control" readonly="" name="vMainImage">					
												<span class="input-group-btn">
													<span class="btn btn-primary btn-file">
														Browse <input type="file" name="vMainImage">
													</span>
												</span>
												</div>
											{/if}
											 </div>
	                                    </div>
	                                    <div class="form-group col-md-10"style="padding-left:15px;">
	                                    	<label for="typeahead">Small Image</label>
	                                    	<div class="store-image">
	                                            {if $data.store_detail.vSmallIamge neq ''}
	                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/SmalImage/{$data.store_detail.vSmallIamge}" width="90%;" style="margin:0 0 0 2px;"/>
	                                            {else}
												<div class="input-group">
												<input type="text" class="form-control" readonly="" name="vSmallIamge">					
												<span class="input-group-btn">
													<span class="btn btn-primary btn-file">
														Browse <input type="file" name="vSmallIamge">
													</span>
												</span>
												</div>
											{/if}
											</div>
	                                    </div>
	                                    <div class="form-group col-md-10"style="padding-left:15px;">
	                                    	<label for="typeahead">Shop Image</label>
	                                    	<div class="store-image">
	                                            {if $data.store_detail.vShopImage neq ''}
	                                            	<img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/ShopImage/{$data.store_detail.vShopImage}" style="margin:0 0 0 2px;"/>
	                                            {else}
	                                            	<div class="input-group">
														<input type="text" class="form-control" readonly="" name="vShopImage">					
														<span class="input-group-btn">
														<span class="btn btn-primary btn-file">
															Browse <input type="file" name="vShopImage">
														</span>
														</span>
													</div>
	                                            {/if}
	                                        </div>
	                                        	  
	                                            
	                                    </div>
	
										<div class="form-group col-md-10" style="padding-left:15px;">
											<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
											<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
											
										</div>
									</fieldset>
								</form>
	                        </div>
	                    </div>
                     </div>
                     <div class="tab-pane fade {if $data.selectedTab eq 'product'} in active{/if}" id="product">
                     	<input type="hidden" id="iStoreId" value='{$data.iStoreId}'>
                     	<div class="panel-body">
							<div class="" style="float:right;margin:0 20px 10px 0;">
							    <a href="{$data.admin_url}products/create?iStoreId={$data.iStoreId}" class="btn btn-primary">Add Product</a>
							</div>
							<div class="col-lg-8">
							    <div class="panel panel-default">
							        <div class="panel-heading">
							            <i class="fa-fw"></i> Product Listing
							            <div class="pull-right">
							                
							            </div>
							        </div>
							        <!-- /.panel-heading -->
							        <div class="panel-body">
							            <div class="row">
							                <div class="col-lg-4">
							                    <div class="table-responsive">
							                        <table class="table table-hover" id="producttable">
							                            <thead>
							                                <tr>
							                                    <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
							                                    <th width="35%">Name of Product</th>
							                                    <th width="15%">Added Type</th>
							                                    <th width="15%">Date</th>
							                                    <th width="20%">Control</th>
							                                </tr>
							                            </thead>
							                        </table>
							                    </div>
							                </div>
							                <div class="col-lg-8">
							                    <div id="morris-bar-chart"></div>
							                </div>
							            </div>
							        </div>
							    </div>
							</div>
                     	</div>
                     </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
var abc = 0;
	$(document).ready(function(){
		$('#stylevalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vStyle").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vStyle").val(_newStr);
		});				

		$('#wearvalue').on('change',function(){
			var _selStyleVal = $(this).val();
			var text_value = $("#vLookingFor").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vLookingFor").val(_newStr);
		});

		$('#wantBuyValue').on('change',function(){
			var _selStyleVal = $(this).val();
			// alert(_selStyleVal);return false;
			var text_value = $("#vWantToBuy").val();
			if(text_value.match(_selStyleVal)){
				return false;
			}
			var _newStr = text_value;
			if(text_value != '' && _selStyleVal != ''){
				_newStr = text_value+','+_selStyleVal;
			}else if(_selStyleVal != ''){
				_newStr = _selStyleVal;
			}
			if(_selStyleVal == 'All'){
				_newStr = $(this).attr('data-all');
			}
			$("#vWantToBuy").val(_newStr);
		});
	});

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>
{/literal}
