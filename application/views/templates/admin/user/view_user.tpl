<script src="{$data.admin_js_path}user.js"></script>
<form name="frmlist" id="frmlist" action="{$data.admin_url}user/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registered Users</h1>
    </div>
</div>
{if $data.message neq ''}
<div class="span12">
    <div class="alert alert-info">
        {$data.message}
    </div>
</div>
{/if}
<div class="row">
    <div class="" style="float:right;margin:0 20px 10px 0;"> 
                        <a href="" class="user-profile btn btn-info">User Profile</a>
                        <a href="" class="send-mail btn btn-success">Send Mail</a>
                       
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> User List
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="userlistId">
                                <thead>
                                <tr>
                                    <th width="1%"><input type="checkbox" id="check_all" name="check_all" value="1"></th>
                                    <th width="15%">First Name</th>
                                    <th width="20%">Last Name</th>
                                    <th width="10%">Email</th>
                                </tr>
                                </thead>
                            </table>  
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>           
</form>
{literal}
<script>
    $(document).ready(function() {
        $('.user-profile').on('click',function(){
            var n = $('input[name="iId[]"]:checked').length;
            if(n==1){
                var _getUserId = $('input[name="iId[]"]:checked').val();
                $.ajax({
                type     : "POST",
                cache    : false,
                url      : "{/literal}{$data.admin_url}{literal}user/view_users?iUserId="+_getUserId,
                success: function(data){
                    $.fancybox({
                        'width': 800,
                        'height':400,
                        'enableEscapeButton' : false,
                        'overlayShow' : true,
                        'overlayOpacity' : 0,
                        'hideOnOverlayClick' : false,
                        'content' : data
                    });
                    return false;
                },
                error: function(response){
                    alert('Error: Please try later.');
                    return false;
                }
            });
            return false;    
            }else{
                alert('You can not view multiple records at a time. Please select only 1.');return false;
            }
            /*
            */
    });
        $('.send-mail').on('click',function(){
            var n = $( "input:checked" ).length;
            if (n == 1) {
                var _getUserId = $('input[name="iId[]"]:checked').val();
                $.ajax({
                type     : "POST",
                cache    : false,
                url      : "{/literal}{$data.admin_url}{literal}user/send_mail?iUserId="+_getUserId,
                success: function(data) {
                    window.location.reload("{$data.admin_url}user");
                },
                error: function(response){
                    alert('Error: Please try later.');
                    return false;
                }
                });
                return false;
                
            }else{
                alert('You can not multiple send mail at a time. Please select only 1.');return false;
            }
        });    
    });
</script>
{/literal}
