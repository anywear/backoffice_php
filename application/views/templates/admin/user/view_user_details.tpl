<div class="row" style="width:600px;">
    <div class="col-lg-12">
        <h3 class="user-profile-header">User Profile</h3>
    </div>
</div>
<div class="row">
<div class="row">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
            	<div class="form-group">
					<!-- <label class="control-label" for="typeahead">Profile Image :</label> -->
					<div class="user-image">
						<img src="{$data.admin_base_url}/uploads/user/{$data.getUserDetails.iUserId}/{$data.getUserDetails.vImage}" width="90%;" />
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">First Name </label>
					</div>
					<div>
						{$data.getUserDetails.vFirstName}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Last Name </label>
					</div>
					<div>
						{$data.getUserDetails.vLastName}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Email </label>
					</div>
					<div>
						{$data.getUserDetails.vEmail}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Password </label>
					</div>
					<div>
						****************
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Date of birth </label>
					</div>
					<div>
						{$data.getUserDetails.dDateOfBirth|date_format:"%m/%d/%Y"}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Type </label>
					</div>
					<div>
						{$data.getUserDetails.eType}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I Live in </label>
					</div>
					<div>
						{$data.getUserDetails.vLeaveIn}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">My Style </label>
					</div>
					<div>
						{$data.getUserDetails.vMyStyle}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">Size </label>
					</div>
					<div>
						{$data.getUserDetails.vSize}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">How Much usualy I Spend </label>
					</div>
					<div>
						{$data.getUserDetails.vSpendMoney}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I'm Looking for </label>
					</div>
					<div>
						{$data.getUserDetails.vLookingFor}
					</div>
				</div>
				<div class="form-group">
					<div class="user-label">
						<label class="control-label" for="typeahead">I Want to Buy </label>
					</div>
					<div>
						{$data.getUserDetails.vWantBuy}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

