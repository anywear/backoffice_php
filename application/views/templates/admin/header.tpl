<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="{$data.admin_url}"><img src="{$data.admin_boostrap}img/logo-anywear.png" alt=""></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-location-arrow fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages ">
                        <li class="scrollable-menu">
                            <!-- <select name="store_detail[eDeeplinkSupport]" class="form-control"  required> -->
                            {section name=i loop=$data.vCityName}
                                <!-- <option value="{$data.vCityName[i].iCityId}" {if $data.vCityName[i] eq $data.vCityName[i].vCityName}selected="selected"{/if}>{$data.vCityName[i].vCityName}</option> -->
                                <a href="{$data.admin_url}store/city?iCityId={$data.vCityName[i].iCityId}" class="text-center" ><strong>{$data.vCityName[i].vCityName}</strong></a>
                            {/section}
                            </select>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="{$data.admin_url}city">
                                <strong>Add City</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    {$data.userdata.anywear_admin_info.vFirstName} {$data.userdata.anywear_admin_info.vLastName} <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{$data.admin_url}admin_management"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{$data.admin_url}authentication/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>
         
            {include file="admin/left.tpl"}
</nav>

{literal}
<script type="text/javascript">
    function changecity(iCityId)
    {
        if(iCityId=="")
        {
            
        }
        else if(iCityId=="addnew")
        {
            
            var url=base_url+"home/create";        
            window.location=url;
        }
        else
        {
            var url=base_url+"home/city?";
            var pars="iCityId="+iCityId;
            // alert(pars);return false;
            window.location.href =url+pars;
            /*$.post(url+pars,function(data){                                                             
                // alert(data);return false;
                window.location.reload();
            });*/
        }
}
</script>
{/literal}