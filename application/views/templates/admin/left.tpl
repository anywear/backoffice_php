<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
        	<li>
                <a class="{if $data.menuAction eq 'user'}active{/if}" href="{$data.admin_url}user">Users</a>
            </li>
            <li>
                <a class="{if $data.menuAction eq 'slider'}active{/if}" href="{$data.admin_url}slider">App Home Slider</a>
            </li>
            <li class="{if $data.menuAction eq 'store' || $data.menuAction eq 'product_categories' || $data.menuAction eq 'product_brand' || $data.menuAction eq 'product_size' || $data.menuAction eq 'product_color' || $data.menuAction eq 'store_style_looking'}active{/if}">
            	<a href="#" class="">Stores<span class="fa arrow"></span></a>
            	<ul class="nav nav-second-level">
                    <!--<li>
                        <a class="{if $data.menuAction eq 'store_category'}active{/if}" href="{$data.admin_url}store_category">Category</a>
                    </li>-->
            		<li>
                        <a class="{if $data.menuAction eq 'store'}active{/if}" href="{$data.admin_url}store">Store</a>
                    </li>
                    <li>
                        <a class="{if $data.menuAction eq 'product_categories'}active{/if}" href="{$data.admin_url}product_categories">Category</a>
                    </li>
                    <li>
                        <a class="{if $data.menuAction eq 'product_brand'}active{/if}" href="{$data.admin_url}product_brand">Brand</a>
                    </li>
                    
                    <li>
                        <a class="{if $data.menuAction eq 'product_size'}active{/if}" href="{$data.admin_url}product_size">Sizes</a>
                    </li>
                    <li>
                        <a class="{if $data.menuAction eq 'product_color'}active{/if}" href="{$data.admin_url}product_color">Color</a>
                    </li>
                    <li>
				        <a class="{if $data.menuAction eq 'store_style_looking'}active{/if}" href="{$data.admin_url}style_looking">Style Looking For</a>
				    </li>
                </ul>
                <!--<a class="active" href="{$data.admin_url}home"><i class="fa fa-dashboard fa-fw"></i> Stores</a>-->
            </li>
            
            <li>
                <a class="{if $data.menuAction eq 'style'}active{/if}" href="{$data.admin_url}personalization">Personalization</a>
            </li>
            <li>
                <a class="{if $data.menuAction eq 'size_master'}active{/if}" href="{$data.admin_url}size">Size Master</a>
            </li>
            <li>
                <a class="{if $data.menuAction eq 'money_spend'}active{/if}" href="{$data.admin_url}money_spend">Money Spend On</a>
            </li>
            <li>
                <a class="{if $data.menuAction eq 'notification'}active{/if}" href="{$data.admin_url}notification">Notification</a>
            </li>
            <!--<li>
                <a class="" href="{$data.admin_url}style_looking"><i class="fa fa-sliders fa-fw"></i>Style Looking</a>
            </li>-->
            <!--<li>
                <a class="" href="{$data.admin_url}store_category"><i class="fa fa-sliders fa-fw"></i>Store Category</a>
            </li>-->
            <!--<li>
                <a class="" href="{$data.admin_url}store_size"><i class="fa fa-sliders fa-fw"></i>Size</a>
            </li>-->
        </ul>
    </div>
</div>