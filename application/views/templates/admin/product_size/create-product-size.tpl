<script src="{$data.admin_js_path}product_size.js"></script>
<div class="row">
    <div class="navbar">
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Size
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}product_size/{$data.function}" method="post" enctype="multipart/form-data">
				<fieldset>
									
                                    <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Select Size</label>
                                        <div class="btn-group size-style-dropdawn">
					    <select name="size[iParentId]" class="form-control" data-toggle="dropdown">
						<option value="">Select Size</option>
						{section name=i loop=$iParentId}
						<option value="{$iParentId[i].iProductSizeId}" {if $iParentId[i] eq $data.size.iParentId}selected="selected"{/if}>{$iParentId[i].vSizeName}</option>
						{/section}
					    </select>
                                        </div>
                                    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="control-label" for="typeahead">Name of Size</label>
					    <input type="text" class="form-control " id="vSizeName" name="size[vSizeName]" value="{$data.size_detail.vSizeName}" required>
				    </div>

				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					    <input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
