<script src="{$data.admin_js_path}products.js"></script>
<input type="hidden" id="iStoreId" value="{$data.iStoreId}" />
<div class="row">
<div class="navbar"></div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit product
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}products/{$data.function}?iProductId={$data.products_detail.iProductId}" method="post" enctype="multipart/form-data">
								<fieldset>
								    <input type="hidden" name="iStoreId" value='{$data.products_detail.iStoreId}'>
								    <input type="hidden" name="iProductId" value='{$data.products_detail.iProductId}'>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Brand</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="products[iBrandId]">		
										    <option>Select brand</option>
										    {section name=i loop=$data.iBrandId}
										    <option value="{$data.iBrandId[i].iBrandId}" {if $data.iBrandId[i].iBrandId eq $data.products_detail['iBrandId']} selected="selected"{/if}>{$data.iBrandId[i].vName}</option>
										    {/section}
										</select>
										</div>
									    </div>
									</div>
									
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Color</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductColorId][]" multiple>		
										    <!--<option>Select Color</option>-->
										    {section name=i loop=$data.color}
										    <option value="{$data.color[i].iProductColorId}" {if in_array($data.color[i].iProductColorId, $data.productColor[i].iProductColorId)} selected="selected"{/if}>{$data.color[i].vColorName}</option>
										    {/section}
										</select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Size</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductSizeId][]" multiple>		
										    <!--<option>Select Size</option>-->
										    {section name=i loop=$data.size}
										    <option value="{$data.size[i].iProductSizeId}" {if in_array($data.size[i].iProductSizeId eq $data.productSize['iProductSizeId'])} selected="selected"{/if}>{$data.size[i].vSizeName}</option>
										    {/section}
										</select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Product Category</label>
									    <div class="input-prepend">
										<div class="btn-group">
										<select class="form-control" row="1" data-toggle="dropdown" id="stylevalue" name="relation[iProductCatId][]" multiple>		
										    <!--<option>Select Product Category</option>-->
										    {section name=i loop=$data.product_cat}
										    <option value="{$data.product_cat[i].id}" {if $data.product_cat[i].id eq $data.products_detail['id']} selected="selected"{/if}>{$data.product_cat[i].vName}</option>
										    {/section}
										</select>
										</div>
									    </div>
									</div>
										
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Name</label>	
									    <input class="form-control " rows="1" id="vName" name="products[vName]" value="{$data.products_detail.vName}" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Brand Name</label>	
									    <input class="form-control " rows="1" id="vBrandedName" name="products[vBrandedName]" value="{$data.products_detail.vBrandedName}" required>
									</div>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Unbrand Name</label>	
									    <input class="form-control " rows="1" id="vUnbrandedName" name="products[vUnbrandedName]" value="{$data.products_detail.vUnbrandedName}" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Currency</label>	
									    <input class="form-control " rows="1" id="vCurrency" name="products[vCurrency]" value="{$data.products_detail.vCurrency}" required>
									</div>
									
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label  for="typeahead">Price</label>
									    <input type="text" class="form-control " id="fPrice" name="products[fPrice]" value="{$data.products_detail.fPrice}" required>
									</div>
									    
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Product Image</label>
									    {if $data.store_detail.vShopImage neq ''}
										    <img src="{$data.admin_base_url}uploads/store/{$data.store_detail.iStoreId}/ShopImage/{$data.store_detail.vShopImage}" style="margin:0 0 0 2px;"/>
									    {else}
										    <div class="input-group">
										    <input type="text" class="form-control" readonly="" name="vImage">					
										    <span class="input-group-btn">
											    <span class="btn btn-primary btn-file">
												    Browse <input type="file" name="vImage">
											    </span>
										    </span>
										    </div>
									    {/if}
									</div>
					
									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Stock</label>	
									    <input class="form-control " rows="1" id="iInStock" name="products[iInStock]" value="{$data.products_detail.iInStock}" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Locale</label>	
									    <input class="form-control " rows="1" id="vLocale" name="products[vLocale]" value="{$data.products_detail.vLocale}" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Description</label>	
									    <input class="form-control " rows="1" id="tDescription" name="products[tDescription]" value="{$data.products_detail.tDescription}" required>
									</div>

									<div class="form-group col-md-10"style="padding-left:15px;">
									    <label for="typeahead">Extract Date</label>	
									    <input class="form-control " rows="1" id="dExtractDate" name="products[dExtractDate]" value="{$data.products_detail.dExtractDate}" required>
									</div>

									<div class="form-group col-md-10" style="padding-left:15px;">
									    <label class="control-label" for="typeahead">Status</label>
										<div class="btn-group size-style-dropdawn">
										    <select name="products[eStatus]" class="form-control" data-toggle="dropdown" required>
										    {section name=i loop=$eStatus}
											<option value="{$eStatus[i]}" {if $eStatus[i] eq $data.products.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
										    {/section}
										    </select>
										</div>
									    </div>
									</div>

									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" id="cancel">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
									</div>
									
								</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">

	$(document).on('change', '.btn-file :file', function() {
	  var input = $(this),
	      numFiles = input.get(0).files ? input.get(0).files.length : 1,
	      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	  input.trigger('fileselect', [numFiles, label]);
	});

	$(document).ready( function() {
	    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
	        
	        var input = $(this).parents('.input-group').find(':text'),
	            log = numFiles > 1 ? numFiles + ' files selected' : label;
	        
	        if( input.length ) {
	            input.val(log);
	        } else {
	            if( log ) alert(log);
	        }
	        
	    });
	});
</script>
{/literal}
