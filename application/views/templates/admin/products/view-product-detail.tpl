<script src="{$data.admin_js_path}list_store.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Store List</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Store DataTables Advanced {if $data.getStoreDetail.eOnSale eq 'Yes'}
                <button type="button" class="btn btn-success" style="margin-left:30px;">On Sale</button>{/if}
                <div class="pull-right">
                    <!-- <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            Actions
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="" class="store-edit">Edit Store</a>
                            </li>
                            <li><a href="" class="on-sale">On Sale</a>
                            </li>
                            <li><a href="" class="add-address">Add Address</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="" class="store-delete">Delete Store</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="span12">
                                <div class="block">
                                    <div class="block-content collapse in">
                                        <!--  -->
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Name of Product </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.vName}
                                            </div>
                                        </div>
                                         <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Brand </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.vBrandedName}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Unbrand Name </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.vUnbrandedName}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Currency </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.vCurrency}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Price </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.fPrice}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Image </label>
                                            </div>
                                            <div class="view-store-image">
                                            {if $data.getProductDetail.vImage neq ''}
                                                <img src="{$data.admin_base_url}uploads/products/{$data.getProductDetail.iProductId}/{$data.getProductDetail.vImage}" width="90%;" style="margin:0 0 0 0px;"/>
                                            {/if}
                                            </div>
                                        </div>

                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Stock </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.iInStock}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Locale </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.vLocale}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Description </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.tDescription}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Extract Date </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.dExtractDate}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Status </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.eStatus}
                                            </div>
                                        </div>
                                        <div class="form-group all-view-store-details">
                                            <div class="view-store-label">
                                                <label class="control-label" for="typeahead">Data from </label>
                                            </div>
                                            <div class="store-details">
                                                {$data.getProductDetail.eDataFrom}
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}