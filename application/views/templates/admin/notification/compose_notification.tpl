<div class="row" style="width:550px;">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
				<form class="form-horizontal" id="frmadmin" action="{$data.admin_url}notification/{$data.function}" method="post" enctype="multipart/form-data">
					<fieldset>
					<legend>{$data.label} Compose</legend>
						<!--<input type="hidden" name="iAdminId" value='{$data.admin_detail.iAdminId}'>-->
						<div class="form-group">
							<div class="controls">
								<input type="text" style="width:85%;margin-left:15px;" class="admin-form" id="vTitle" name="data[vTitle]" value="{$data.admin_detail.vTitle}" placeholder="Title" required>
							</div>
						</div>

						<div class="form-group">
							<div class="controls">
								<textarea class="form-control" style="width:85%;margin-left:15px;" rows="3" id="tDescription" name="data[tDescription]" value="{$data.admin_detail.tDescription}" placeholder="One Fine body" required></textarea>
							</div>
						</div>
						<div class="form-group" style="margin-left:5px;">
							<button type="button" class="btn bottom-buffer" onclick="returnme();">Close</button>
							<button type="Submit" value="Send" id="btn-save" class="btn btn-primary" onclick="validate();">Send</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
{literal}
<script>
function validate(){
    if($( "#vFirstName" ).val() ==''){
    	$("#firstnameinput").html( "<p style='margin:5px 0 0 191px;'>Please Enter First Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#firstnameinput").hide();
    }
    if($( "#vLastName" ).val() ==''){
        $("#lastnameinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Last Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#lastnameinput").hide();}
    if($( "#vEmail" ).val() ==''){
        $("#emailinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Email!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#emailinput").hide();}
    if(IsEmail($( "#vEmail" ).val())==false){
        $("#properemail").html( "<p style='margin:5px 0 0 191px;>Please Enter Proper Email Address!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{$("#properemail").hide();}
    if($("#vPassword" ).val() ==''){
        $("#passwordinput").html( "<p style='margin:5px 0 0 191px;>Please Enter Password!</p>" );
        $("#myalert").modal('show');
        return false;
    }
    else{
    	$("#passwordinput").hide();
        $("#frmadmin").submit();
    }
}
function returnme(){
    $.fancybox.close();
}
</script>
{/literal}