<script src="{$data.admin_js_path}store_category.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Style
                <div class="pull-right">
                    <div class="btn-group">
                        
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}store_category/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iStoreCategoryId" value='{$data.size.iStoreCategoryId}'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label  for="typeahead">Name of Style</label>
										<input type="text" class="form-control " id="vStoreCategoryName" name="store_detail[vStoreCategoryName]" value="{$data.size.vStoreCategoryName}" required>
										
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Status</label>
										<div class="btn-group size-style-dropdawn">
												<select name="store_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												{section name=i loop=$eStatus}
												    <option value="{$eStatus[i]}" {if $eStatus[i] eq $data.size.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
												{/section}
												</select>
											</div>
									    
									</div>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}
