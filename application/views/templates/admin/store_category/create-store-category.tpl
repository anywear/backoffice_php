<script src="{$data.admin_js_path}store_category.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Store Category
                <div class="pull-right">
                    <div class="btn-group">
                        
                    </div>
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}store_category/{$data.function}" method="post" enctype="multipart/form-data">
								<fieldset>
									<!-- <input type="hidden" name="iSizeId" value='{$data.size_detail.iSizeId}'> -->
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label class="control-label" for="typeahead">Name of Store Category</label>
										
											<input type="text" class="form-control size-style-dropdawn" id="vStoreCategoryName" name="store_detail[vStoreCategoryName]" value="{$data.style_detail.vStoreCategoryName}" required>
										
									</div>
									<!-- <div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Size Type</label>
										</div>
										<div class="input-prepend">
											<div class="btn-group size-style-dropdawn">
												<select class="form-control" data-toggle="dropdown" name="size_detail[eSizeType]" id="eSizeType">
					                                <option value=" ">-- Select Type --</option>
					                                {section name=i loop=$eSizeType}
					                                <option value="{$eSizeType[i]}" {if $eSizeType[i] eq $data.size_detail['eSizeType']} selected="selected"{/if}>{$eSizeType[i]}</option>
					                                {/section}
					                            </select>
											</div>
										</div>
									</div> -->
  
									<!-- <div class="form-group">
										<div class="store-label">
										<label class="control-label" for="typeahead">Sorting</label>
										</div>
										<div class="controls">
											<div class="btn-group size-style-dropdawn">
											<select id="iSorting" class="form-control" data-toggle="dropdown" name="size_detail[iSorting]" title="order no" lang="*">
											 	<option value=''>--Select Order--</option>
                                                {while ($data.totalRec+1) >= $data.initOrder}
                                                <option value="{$data.initOrder}" >{$data.initOrder++}</option>
                                                {/while}
											</select>
											</div>
										</div>
									</div> -->
									<div class="form-group col-md-10" style="padding-left:15px;">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">

</script>
{/literal}
