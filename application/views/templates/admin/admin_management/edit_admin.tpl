<div class="row" style="width:450px;">
    <div class="span12">
        <div class="block">
            <div class="block-content collapse in">
				<form class="form-horizontal" id="frmadmin" action="{$data.admin_url}admin_management/{$data.function}" method="post" enctype="multipart/form-data">
					<fieldset>
					<legend>{$data.label} Edit Admin</legend>
						<input type="hidden" name="iAdminId" value='{$data.admin_detail.iAdminId}'>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">First Name</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vFirstName" name="admin_detail[vFirstName]" value="{$data.admin_detail.vFirstName}">
							</div>
							<span id="firstnameinput"></span>
						</div>

						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Last Name</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vLastName" name="admin_detail[vLastName]" value="{$data.admin_detail.vLastName}">
							</div>
							<span id="lastnameinput"></span>
						</div>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Email</label>
							</div>
							<div class="controls">
								<input type="text" class="admin-form" id="vEmail" name="admin_detail[vEmail]" value="{$data.admin_detail.vEmail}">
							</div>
							<span id="emailinput"></span>
							<span id="properemail"></span>
						</div>
						<div class="form-group">
							<div class="admin-label">
								<label class="control-label" for="typeahead">Password</label>
							</div>
							<div class="controls">
								<input type="password" class="admin-form" id="vPassword" name="admin_detail[vPassword]" value="" placeholder="••••••••••••••••••••••">
							</div>
							<span id="passwordinput"></span>
						</div>
						<div class="form-group" style="margin-left:5px;">
							<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
							<button type="button" id="btn-save" class="btn btn-primary" onclick="validate();">Save changes</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
{literal}
<script>
	function validate(){
    if($( "#vFirstName" ).val() ==''){
    	$("#firstnameinput").html( "<p style='margin:5px 0 0 161px;'>Please Enter First Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#firstnameinput").hide();
    }
    if($( "#vLastName" ).val() ==''){
        $("#lastnameinput").html( "<p style='margin:5px 0 0 161px;'>Please Enter Last Name!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#lastnameinput").hide();
    }
    if($( "#vEmail" ).val() ==''){
    	$("#emailinput").html( "<p style='margin:5px 0 0 161px;'>Please Enter Email!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#emailinput").hide();
    }
    if(IsEmail($( "#vEmail" ).val())==false){
        $("#properemail").html( "<p style='margin:5px 0 0 161px;'>Please Enter Proper Email Address!</p>" );
        $("#myalert").modal('show');
        return false;
    }else{
    	$("#properemail").hide();
    	$("#frmadmin").submit();
    }
}
</script>
{/literal}
