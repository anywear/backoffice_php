<script src="{$data.admin_js_path}product-brand.js"></script>
<div class="row">
    <div class="navbar">
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Brand
                <div class="pull-right">
                    <div class="btn-group"></div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}product_brand/update" method="post" enctype="multipart/form-data">
				<input type="hidden" name="data[iBrandId]" value="{$data.product_brand.iBrandId}">
				<fieldset>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Name</label>
					<input type="text" class="form-control size-style-dropdawn" id="vName" name="data[vName]" value="{$data.product_brand.vName}" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Synonyms</label>
					<input type="text" class="form-control size-style-dropdawn" id="tSynonyms" name="data[tSynonyms]" value="{$data.product_brand.tSynonyms}" required>
				    </div>
				    
				    <input type="hidden" name="data[eDataFrom]" value="manual">
					
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
