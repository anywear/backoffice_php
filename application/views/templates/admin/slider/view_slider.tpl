<script src="{$data.admin_js_path}slider.js"></script>
<form name="frmlist" id="frmlist" action="{$data.admin_url}slider/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">App Home Slider</h1>
    </div>
</div>
<div class="row">
    
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="{$data.admin_url}slider/create" class="notify-create btn btn-primary">Add New Slider</a>
                        <a href="" class="make-active btn btn-success">Make Active</a>
                        <a href="" class="make-inactive btn btn-info">Make Inactive</a>
                        <a href="" class="notify-delete btn btn-danger">Delete</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Slider Listing
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="sliderid">
                                <thead>
                                    <tr>
                                        <th width="3%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId" value="1"></th>
                                        <th width="25%">Title</th>
					<!--<th width="25%">Type</th>-->
					<th width="15%">Status</th>
					<th width="15%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
