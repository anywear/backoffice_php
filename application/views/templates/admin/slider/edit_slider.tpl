<script src="{$data.admin_js_path}slider.js"></script>
<div class="row">
    <div class="col-lg-12">
        <h1 class=""></h1>
    </div>
</div>

<div class="row">
	<div class="btn-group" style="float:right;margin:0 20px 10px 0;">
        <a href="{$data.admin_url}slider/create" class="notify-create btn btn-primary">Add New Slider</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Slider
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body edit_personalization">
            	<div class="row">
            		<div class="col-lg-8">
	            		<form role="form" action="" method="POST" enctype="multipart/form-data">
				    <input type="hidden" name="data[iSliderId]" value="{$data.slider.iSliderId}">
				    <!--<label class="col-md-2">Slider Type</label>
				    <div class="form-group col-md-10">
					<select name="data[eSliderType]" class="form-control" required>
					<option value="">Select slider type</option>
					{section name=i loop=$eSliderType}
					    <option value="{$eSliderType[i]}" {if $eSliderType[i] eq $data.slider.eSliderType}selected="selected"{/if}>{$eSliderType[i]}</option>
					{/section}
					</select>
				    </div>-->
				    <div class="form-group col-md-10"style="padding-left:15px;">
					    <label class="col-md-2">Title</label>
					    <input type="text" class="form-control" name="data[vTitle]" value="{$data.slider.vTitle}" required="">
				    </div>
				    
				    <div class="form-group col-md-10"style="padding-left:15px;">
					    <label class="col-md-2">Description</label>
					    
						<textarea class="form-control" name="data[tDescription]" required="">{$data.slider.tDescription}</textarea>
				    </div>
				    
				    <div class="form-group col-md-10"style="padding-left:15px;">
				    	<label class="col-md-2">Slider Image</label>
				    	
						{if $data.slider.vImage neq ''}
							<img src="{$data.admin_base_url}uploads/slider/{$data.slider.iSliderId}/{$data.slider.vImage}" width="150" height="150" style="padding-bottom: 10px;" ><br />
							<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="margin:0 0 10px 135px;">Delete</a>
						{/if}
							<div class="input-group">
							<input type="text" class="form-control" readonly="" name="vImage">					
							<span class="input-group-btn">
								<span class="btn btn-primary btn-file">
									Browse <input type="file" name="vImage">
								</span>
							</span>
							</div>
						<!-- <input type="file" name="vImage" required=""> -->
				    </div>
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <label class="col-md-2">Status</label>
					    
							<select name="data[eStatus]" class="form-control" required>
								{section name=i loop=$eStatuses}
								    <option value="{$eStatuses[i]}" {if $eStatuses[i] eq $data.slider.eStatus}selected="selected"{/if}>{$eStatuses[i]}</option>
								{/section}
							</select>
				    </div>
					    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <input type="submit" class="btn btn-primary" value="Save changes" />
					    <button type="button" class="btn btn-default" onclick="returnme();" >Cancel</button>
				    </div>
				</form>
		            </div>
		        </div>
        	</div>
    	</div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Delete</h4>
	    </div>
	    <div class="modal-body">
		Are you sure to delete the Image?
	    </div>
	    <div class="modal-footer">
		<a class="btn btn-default" data-dismiss="modal">No</a>
		<a class="btn btn-default" style="float:right;" href="{$data.admin_url}slider/deleteicon?id={$data.slider.iSliderId}">Yes</a>
	    </div>
	</div>
    </div>
</div>
			    
			    
{literal}
<script type="text/javascript">
	$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>{/literal}