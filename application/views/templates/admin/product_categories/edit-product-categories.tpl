<script src="{$data.admin_js_path}product-categories.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Product Categories
                <div class="pull-right">
                    <div class="btn-group"></div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}product_categories/update" method="post" enctype="multipart/form-data">
				<input type="hidden" name="data[id]" value="{$data.product_categories.id}">
				<fieldset>
				    
				     <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Product Category</label>
                                        <div class="btn-group size-style-dropdawn">
					    <select name="data[vParentId]" class="form-control" data-toggle="dropdown">
						<option value="">Parent Categories</option>
						{section name=i loop=$pranet_product_category}
						<option value="{$pranet_product_category[i].vProductCatId}" {if $pranet_product_category[i].vProductCatId eq $data.product_categories.vParentId}selected="selected"{/if}>{$pranet_product_category[i].vName}</option>
						{/section}
					    </select>
                                        </div>
                                    </div>
				     
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Product Category</label>
					<input type="text" class="form-control size-style-dropdawn" id="vProductCatId" name="data[vProductCatId]" value="{$data.product_categories.vProductCatId}" required>
				    </div>

				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Name</label>
					<input type="text" class="form-control size-style-dropdawn" id="vName" name="data[vName]" value="{$data.product_categories.vName}" required>
				    </div>
				    
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Short Name</label>
					<input type="text" class="form-control size-style-dropdawn" id="vShortName" name="data[vShortName]" value="{$data.product_categories.vShortName}" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Localized</label>
					<input type="text" class="form-control size-style-dropdawn" id="vLocalizedId" name="data[vLocalizedId]" value="{$data.product_categories.vLocalizedId}" required>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Has Heel Height Filter</label>
					<select name="data[eHasHeelHeightFilter]" class="form-control size-style-dropdawn" required>
					    <option value="true" {if $data.product_categories.eHasHeelHeightFilter eq true}selected{/if}>True</option>
					    <option value="false" {if $data.product_categories.eHasHeelHeightFilter eq false}selected{/if}>False</option>
					</select>
				    </div>
				    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Has Color Filter</label>
					<select name="data[eHasColorFilter]" class="form-control size-style-dropdawn" required>
					    <option value="true" {if $data.product_categories.eHasColorFilter eq true}selected{/if}>True</option>
					    <option value="false" {if $data.product_categories.eHasColorFilter eq false}selected{/if}>False</option>
					</select>
				    </div>
				    <input type="hidden" name="data[eDataFrom]" value="manual">
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
