<script src="{$data.admin_js_path}retailer.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Retailer
                <div class="pull-right">
                    <div class="btn-group"></div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
			    <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}retailer/update" method="post" enctype="multipart/form-data">
				<input type="hidden" name="data[iRetailerId]" value="{$data.retailer.iRetailerId}">
				<fieldset>
				    
				    <div class="form-group">
					<div class="store-label">
					    <label class="control-label" for="typeahead">Name</label>
					</div>
					<div class="controls">
					    <input type="text" class="form-control size-style-dropdawn" id="vName" name="data[vName]" value="{$data.retailer.vName}" required>
					</div>
				    </div>
				    
				    <div class="form-group">
					<div class="store-label">
					    <label class="control-label" for="typeahead">Data From</label>
					</div>
					<div class="controls">
					    <select name="data[eDeeplinkSupport]" class="form-control size-style-dropdawn" required>
					    {section name=i loop=$eDeeplinkSupport}
						<option value="{$eDeeplinkSupport[i]}" {if $eDeeplinkSupport[i] eq $data.retailer.eDeeplinkSupport}selected="selected"{/if}>{$eDeeplinkSupport[i]}</option>
					    {/section}
					    </select>
					</div>
				    </div>
				    
				    <div class="form-group">
					<div class="store-label">
					    <label class="control-label" for="typeahead">Host Domain</label>
					</div>
					<div class="controls">
					    <input type="text" class="form-control size-style-dropdawn" id="vHostDomain" name="data[vHostDomain]" value="{$data.retailer.vHostDomain}" required>
					</div>
				    </div>
				    
				    <div class="form-group">
					<div class="store-label">
					    <label class="control-label" for="typeahead">Data From</label>
					</div>
					<div class="controls">
					    <select name="data[eDataFrom]" class="form-control size-style-dropdawn" required>
					    {section name=i loop=$eDataFrom}
						<option value="{$eDataFrom[i]}" {if $eDataFrom[i] eq $data.retailer.eDataFrom}selected="selected"{/if}>{$eDataFrom[i]}</option>
					    {/section}
					    </select>
					</div>
				    </div>
					
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
