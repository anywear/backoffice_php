<script src="{$data.admin_js_path}product_color.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit Color
                <div class="pull-right">
                    <div class="btn-group">
                    </div>
                </div>
            </div>
	    <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}product_color/update" method="post" enctype="multipart/form-data">
				<fieldset>
				    <input type="hidden" name="iProductColorId" value='{$data.color.iProductColorId}'>
                                     <div class="form-group col-md-10" style="padding-left:15px;">
                                        <label class="control-label" for="typeahead">Select Color</label>
                                        <div class="btn-group size-style-dropdawn">
					    <select name="color[iParentId]" class="form-control" data-toggle="dropdown" required>
						<option value="">Select Color</option>
					    	{section name=i loop=$iParentId}
						<option value="{$iParentId[i].iProductColorId}" {if $iParentId[i].iProductColorId eq $data.color.iParentId}selected="selected"{/if}>{$iParentId[i].vColorName}</option>
						{/section}
					    </select>
                                        </div>
				    </div>
				     
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Name of Color</label>
					<input type="text" class="form-control Color-style-dropdawn" id="vColorName" name="color[vColorName]" value="{$data.color.vColorName}" required>
				    </div>
                                    
				    <div class="form-group col-md-10" style="padding-left:15px;">
					<label class="control-label" for="typeahead">Status</label>
					<div class="btn-group size-style-dropdawn">
					    <select name="color[eStatus]" class="form-control" data-toggle="dropdown" required>
					    {section name=i loop=$eStatus}
						<option value="{$eStatus[i]}" {if $eStatus[i] eq $data.color.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
					    {/section}
					    </select>
					</div>
				    </div>
				    <div class="form-group col-md-10" style="padding-left:15px;">
					    <button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
					    <input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
				    </div>
				</fieldset>
			    </form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
	</div>
    </div>
</div>
