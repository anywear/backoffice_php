<script src="{$data.admin_js_path}product_color.js"></script>
<form name="frmlist" id="frmlist" action="{$data.admin_url}product_color/action_update" method="post">
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Product Color List</h1>
    </div>
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="{$data.admin_url}product_color/create" class="btn btn-primary">Add Color</a>
                        <a href="" class="btn btn-success make-active">Make Active</a>
                        <a href="" class="btn btn-info make-inactive">Make Inactive</a>
                        <a href="" class="btn btn-danger notify-delete">Delete Color</a>
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Product Color DataTables
                <div class="pull-right">
                    
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                            <table class="table table-hover" id="Colortable">
                                <thead>
                                    <tr>
                                        <th width="5%"><input type="checkbox" id="check_all" name="check_all" class="getAdminId"></th>
                                        <th width="20%">Parent Color</th>
                                        <th width="20%">Name if Color</th>
                                        <th width="20%">Status</th>
                                        <th width="20%">Control</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        $('.notify-delete').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
        $("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
            }
        return false;
        });
    
    
    $(".make-active").click(function() {
        $("#action").val("Active");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    
    $(".make-inactive").click(function() {
        $("#action").val("Inactive");
        var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
        if(atLeastOneIsChecked == true){
        $("#frmlist").submit();
            return false;
        }
    });
    });
    
    function submitform() {
    $("#frmlist").submit();
    }
</script>
{/literal}