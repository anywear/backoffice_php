<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Personalization</h1>
    </div>
</div>

<div class="row" >
    <div class="btn-group" style="float:right;margin:0 20px 10px 0;">
        <a href="{$data.admin_url}personalization/add" class="btn btn-primary">Add Style</a>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Style List
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body img-list">
            	<div class="row">
            		<h4>Name: <small>{$data.getCat[0]['vCategoryName']}</small></h4>
            		{section name=i loop=$data.cat1PersonaData}
						<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			            	<a class="thumbnail" href="{$data.admin_url}personalization/edit?iPersonalId={$data.cat1PersonaData[i]['iPersonalId']}">
			                    <img class="img-responsive" src="{$data.base_url}uploads/personalization/{$data.cat1PersonaData[i]['iCategoryId']}/{$data.cat1PersonaData[i]['iPersonalId']}/250x250_{$data.cat1PersonaData[i]['vImage']}" alt="">
			                </a>
			            </div>
		            {/section}
		        </div>
		        
		        <hr />
		        
		        <div class="row">
            		<h4>Name: <small>{$data.getCat[1]['vCategoryName']}</small></h4>
            		{section name=i loop=$data.cat2PersonaData}
						<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			            	<a class="thumbnail" href="{$data.admin_url}personalization/edit?iPersonalId={$data.cat2PersonaData[i]['iPersonalId']}">
			                    <img class="img-responsive" src="{$data.base_url}uploads/personalization/{$data.cat2PersonaData[i]['iCategoryId']}/{$data.cat2PersonaData[i]['iPersonalId']}/250x250_{$data.cat2PersonaData[i]['vImage']}" alt="">
			                </a>
			            </div>
		            {/section}
		        </div>
        	</div>
    	</div>
    </div>
</div>        