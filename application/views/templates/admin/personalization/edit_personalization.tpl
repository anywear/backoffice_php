
<input type="hidden" name="action" id="action">
<div class="row">
    <div class="col-lg-12">
        <h1 class=""></h1>
    </div>
</div>

<div class="row">
<div class="btn-group" style="float:right;margin:0 20px 10px 0;">
                        <a href="{$data.admin_url}personalization/add" class="btn btn-primary">Add Style</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i> Style
                <div class="pull-right">
                    
                </div>
            </div>
            <div class="panel-body edit_personalization">
            	<div class="row">
            		<div class="col-lg-8">
	            		 <form role="form" action="" method="POST" enctype="multipart/form-data">
                         <input type="hidden" name="action" id="action">
                         <div class="form-group col-md-10" style="padding-left:15px;">
	            			<label class="">Category</label>
                            <!-- <div class="form-group col-md-10"> -->
	            		 	<select name="data[iCategoryId]" class="form-control add-per-cat" required>
                                    <option value="">Select category</option>
                                    {section name=i loop=$data.getCat}
                                    	<option value="{$data.getCat[i]['iCategoryId']}" {if $data.personaData['iCategoryId'] eq $data.getCat[i]['iCategoryId']}selected{/if}>{$data.getCat[i]['vCategoryName']}</option>
                                    {/section}
                                </select>
                            </div>
	                    
		            		<div class="img-list cat1">
	            				<div class="col-md-4 personal-image">
	            					<!-- <label class="col-md-2 personal-label"></label> -->
									<div class="col-xs-6 thumb">
						                <img class="img-responsive" src="{$data.base_url}uploads/personalization/{$data.personaData['iCategoryId']}/{$data.personaData['iPersonalId']}/104x354_{$data.personaData['vImage']}" alt="">
						            </div>
						            <div style="clear:both;"></div>
						            <div class="col-xs-5 thumb">
						            	<input type="hidden" name="data[iPersonalId]" value="{$data.personaData['iPersonalId']}" />
                                                        
						                <!-- <input type="file" class="forn_input" name="vImage" /> -->
                                        
						            </div>
                                </div>
                                <div class="form-group col-md-10" style="padding-left:15px;">
                                    <!-- <label class="">Add Photo</label> -->
                                    <!-- <div class="input-group ">
                                        <input id="upfile" type="text" class="form-control" readonly="" name="vImage" onchange="sub(this)">
                                        <span class="input-group-btn">
                                            <span class="btn btn-primary btn-file">
                                                Browse <input id="upfile" type="file" name="vImage" onchange="sub(this)">
                                            </span>
                                        </span>
                                    </div> -->
                                    <div class="form-group col-md-10">
                                        <div class="col-xs-5 thumb">
                                        <div id="yourBtn" class="btn btn-primary" onclick="getFile()">Add Photo</div>
                                        <div style='height: 0px;width: 0px; overflow:hidden;'><input id="upfile" type="file" class="forn_input" name="vImage" value="upload" onchange="sub(this)"/></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-10" style="padding-left:15px;">
                                    <!-- <label class="personal-label">Style Title</label> -->
                                    <input type="text" class="form-control upload_inputfile_text" name="data[vStyle]" value="{$data.personaData['vStyle']}" required/>
                                </div>
                                
                            </div>
					        
					        <div class="form-group col-md-10" style="padding-left:15px;">
					        	<input type="submit" value="Save changes" class="btn btn-primary"/>
                                <a data-toggle="modal" data-target="#myModal" class="btn btn-default" title="Delete">Delete</a>
					        	<button type="button" class="btn btn-default" onclick="returnme();">Cancel</button>
					        </div>
				         </form>
		            </div>
		        </div>
        	</div>
    	</div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Delete</h4>
        </div>
        <div class="modal-body">
        Are you sure to delete the Image?
        </div>
        <div class="modal-footer">
        <a class="btn btn-default" data-dismiss="modal">No</a>
        <a class="btn btn-default" style="float:right;" href="{$data.admin_url}personalization/action_update?id={$data.personaData.iPersonalId}">Yes</a>
        </div>
    </div>
    </div>
</div>
{literal}
	<script type="text/javascript">
    
		function returnme(){
			window.location.href = base_url+'personalization';
		}

        function getFile(){
   document.getElementById("upfile").click();
 }
 function sub(obj){
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("yourBtn").innerHTML = fileName[fileName.length-1];
    document.myForm.submit();
    event.preventDefault();
  }
 

$(document).ready( function() {
    /*$('.photo-delete').on('click',function(){
        $("#action").val("Delete");
            $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
        });*/
});
	</script>
{/literal}