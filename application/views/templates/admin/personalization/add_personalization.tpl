<div class="row">
    <div class="col-lg-12">
        <h1 class=""></h1>
    </div>
</div>

<div class="row">
<div class="btn-group" style="float:right;margin:0 20px 10px 0;">
                    	<a href="{$data.admin_url}personalization/add" class="btn btn-primary">Add Style</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Add Style
                <div class="pull-right">
                    <div class="btn-group">
                    	
                        
                    </div>
                </div>
            </div>
            <div class="panel-body edit_personalization">
            	<div class="row">
            		<div class="col-lg-8">
	            		 <form role="form" action="" method="POST" enctype="multipart/form-data">
	            		<div class="form-group col-md-10" style="padding-left:15px;">
	            			<label class="col-md-2">Category</label>
	            		 	
                            <select name="data[catName]" class="form-control" required>
                                <option value="">Select category</option>
                                {section name=i loop=$data.getCat}
                                	<option value="{$data.getCat[i]['iCategoryId']}">{$data.getCat[i]['vCategoryName']}</option>
                                {/section}
                            </select>
                        </div>
		            		<div class="img-list cat1">
		            			<div id="row_id_1">
		            				<div class="form-group col-md-10" style="padding-left:15px;">
			            				<label class="">Title</label>
							            <div class="form-group">
							            	<input type="text" class="form-control" name="data[per][vStyle][1]" value="" required/>
							            </div>
						            </div>
						            <div class="form-group col-md-10" style="padding-left:15px;">
			            				<label class="">Upload Photo</label>
					            		<div class="form-group">
							            	<div class="input-group ">
											<input type="text" class="form-control" readonly="" name="1">					
											<span class="input-group-btn">
												<span class="btn btn-primary btn-file">
													Browse <input type="file" name="1">
												</span>
											</span>
											</div>
							            </div>
									</div>				            
					            </div>
					        </div>
					        
					        <input type="hidden" value="1" id="total_row" />
					        <div class="form-group col-md-10">
					        	<a href="#" class="btn btn-default add_new" title="Add new"><span class="fa fa-plus-circle"></span></a>
					        	<a href="#" class="btn btn-default remove" title="Remove"><span class="fa fa-minus-circle"></span></a>
					        	<input type="submit" class="btn btn-primary" value="Save changes" />
					        	<button type="button" class="btn btn-default" onclick="returnme();" >Cancel</button>
					        </div>
				         </form>
		            </div>
		        </div>
        	</div>
    	</div>
    </div>
</div>

{literal}
<script type="text/javascript">
	$(document).ready(function(){
		$('.add_new').on('click',function(){
			//alert('hi'); return false;
			var _totRow = $('#total_row').val();
			var _rowHtml = '';
			
			var _newTot = parseInt(_totRow) + 1;
			
			_rowHtml += '<div id="row_id_'+_newTot+'">';
			_rowHtml += '<div class="form-group col-md-10"style="padding-left:15px;">';
			_rowHtml += '<label class="">Title</label>';
			_rowHtml += '<div class="form-group">';
			_rowHtml += '<input type="text" class="form-control" name="data[per][vStyle]['+_newTot+']" value="" required/>';
			_rowHtml += '</div>';
			_rowHtml += '</div>';
			_rowHtml += '<div class="form-group col-md-10"style="padding-left:15px;">';
	        _rowHtml += '<label class="">Upload Photo</label>';
			_rowHtml += '<div class="form-group">';

			_rowHtml += '<div class="input-group ">';
			_rowHtml += '<input type="text" class="form-control" readonly="" name="'+_newTot+'">';
			_rowHtml += '<span class="input-group-btn">';
			_rowHtml += '<span class="btn btn-primary btn-file">';
			_rowHtml += 'Browse <input type="file" name="'+_newTot+'">';
			_rowHtml += '</span>';
			_rowHtml += '</span>';
			// _rowHtml += '<input type="file" name="'+_newTot+'" required/>';
			
			_rowHtml += '</div>';
			_rowHtml += '</div>';
			_rowHtml += '</div>';
			
			$('.cat1').append(_rowHtml);
			$('#total_row').val(_newTot);
			return false;
		});
		
		$('.remove').on('click',function(){
			var _totRow = $('#total_row').val();
			
			if(_totRow > 1){
				var _newTot = parseInt(_totRow) - 1;
				$('#row_id_'+_totRow).remove();
				$('#total_row').val(_newTot);
			}
			
			
			return false;
		});
	});
	
	function returnme(){
		window.location.href = base_url+'personalization';
	}
	$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>
{/literal}