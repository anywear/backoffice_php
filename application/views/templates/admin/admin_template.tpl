<!DOCTYPE html>
<html class="no-js">
<head>
    <title>::: AnyWear :::</title>
    
    <link href="{$data.admin_boostrap}css/bootstrap.min.css" rel="stylesheet">
    <link href="{$data.admin_boostrap}css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="{$data.admin_boostrap}css/plugins/timeline.css" rel="stylesheet">
    <link href="{$data.admin_boostrap}css/sb-admin-2.css" rel="stylesheet">

  <link href="{$data.admin_boostrap}css/plugins/morris.css" rel="stylesheet">
    <link href="{$data.admin_boostrap}css/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css
    ">
    <script src="{$data.admin_boostrap}js/jquery-1.11.0.js"></script>
    <script src="{$data.admin_boostrap}js/bootstrap.min.js"></script>
    <script src="{$data.admin_boostrap}js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="{$data.admin_boostrap}js/sb-admin-2.js"></script>
    <script src="{$data.admin_boostrap}js/bootstrap-modal.js"></script>
    <script src="{$data.admin_js_path}common.js"></script>
    <script type="text/javascript" src="{$data.admin_boostrap}js/plugins/dataTables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="{$data.admin_boostrap}js/dataTables.editor.js"></script>
    <script type="text/javascript" src="{$data.admin_boostrap}js/dataTables.editor.bootstrap.js"></script>
    <link href="{$data.admin_boostrap}css/dataTables.bootstrap.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="{$data.admin_boostrap}js/plugins/dataTables/dataTables.bootstrap.js"></script>
    
    <link rel="stylesheet" type="text/css" href="{$data.base_url}/assets/library/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen"/>
    <script type="text/javascript" src="{$data.base_url}/assets/library/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
    


<script type="text/javascript">
var base_url = '{$data.admin_url}';
</script>

</head>

<body>
    <div id="wrapper">
        {include file="admin/header.tpl" }
        <div id="page-wrapper">
            {include file=$data.tpl_name}
        </div>
    </div>
    
     
</body>
</html>


