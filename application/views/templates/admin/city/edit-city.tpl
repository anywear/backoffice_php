<script src="{$data.admin_js_path}city.js"></script>
<div class="row">
    <div class="navbar">
        <!--<div class="navbar-inner">
            {$data.breadcrumb}
        </div>-->
    </div>
</div>
<div class="row">
<div class="" style="float:right;margin:0 20px 10px 0;">
                        <a href="{$data.admin_url}city/create" class="store-edit btn btn-primary">Add City</a>
                        
                    </div>
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa-fw"></i>Edit City
                <div class="pull-right">
                    
                </div>
            </div>
			<div class="panel-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="table-responsive">
                           <form class="form-horizontal" id="frmadmin" action="{$data.admin_url}city/update" method="post" enctype="multipart/form-data">
								<fieldset>
									<input type="hidden" name="iCityId" value='{$data.size.iCityId}'>
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label for="typeahead">Name of City</label>
										<input type="text" class="form-control" id="vCityName" name="size_detail[vCityName]" value="{$data.size.vCityName}" required>
									</div>
									
									<div class="form-group col-md-10" style="padding-left:15px;">
										<label  for="typeahead">Status</label>
										<div class="">
												<select name="size_detail[eStatus]" class="form-control" data-toggle="dropdown" required>
												{section name=i loop=$eStatus}
												    <option value="{$eStatus[i]}" {if $eStatus[i] eq $data.size.eStatus}selected="selected"{/if}>{$eStatus[i]}</option>
												{/section}
												</select>
											</div>
									    </div>
									</div>
									<div class="form-group col-md-10">
										<button type="button" class="btn bottom-buffer" onclick="returnme();">Cancel</button>
										<input type="submit" id="btn-save" class="btn btn-primary" value="Save changes" />
										
									</div>
								</fieldset>
							</form>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>
{literal}
<script type="text/javascript">
</script>
{/literal}
