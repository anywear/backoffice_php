<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class webservice_new_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    
    function add_retailer($data){
        $this->db->insert('store', $data);
        return $this->db->insert_id();
    }
    
    function update_product($iProductId,$data){
    	$this->db->update("products", $data, array('iProductId' => $iProductId));
	    return $this->db->affected_rows();
    }
    
    function get_retailer_data($vName){
        $this->db->select('vStoreName');
        $this->db->from('store');
        $this->db->where('vStoreName',$vName);
        return $this->db->get()->num_rows();
    }
    
    function get_brands_data($vName){
        $this->db->select('vName');
        $this->db->from('brands');
        $this->db->where('vName',$vName);
        return $this->db->get()->num_rows();
    }
    
    function add_brands($data){
        $this->db->insert('brands', $data);
        return $this->db->insert_id();
    }
    
    function get_categories_data($vName){
        $this->db->select('vName');
        $this->db->from('product_categories');
        $this->db->where('vName',$vName);
        return $this->db->get()->num_rows();
    }
    
    function add_categories($data){
        $this->db->insert('product_categories', $data);
        return $this->db->insert_id();
    }
    
    function getCatId($catId){
    	$this->db->select('id');
        $this->db->from('product_categories');
        $this->db->where('vProductCatId',$catId);
        return $this->db->get()->row_array();
    }
    
    function getStoreId($vStoreName){
    	$this->db->select('iStoreId');
        $this->db->from('store');
        $this->db->where('vStoreName',$vStoreName);
        return $this->db->get()->row_array();
    }
    
    function getBrandId($vName){
    	$this->db->select('iBrandId');
        $this->db->from('brands');
        $this->db->where('vName',$vName);
        return $this->db->get()->row_array();
    }
    
    function setProductData($data){
    	$this->db->insert('products', $data);
        return $this->db->insert_id();
    }
    
    function setProductCategoryData($data){
    	$this->db->insert('product_category_relation', $data);
        return $this->db->insert_id();
    }
    
    function saveAltImage($data){
    	$this->db->insert('product_gallery', $data);
        return $this->db->insert_id();
    }
    
    function updateImage($iProductId,$vImage){
    	$this->db->update("products", $vImage, array('iProductId' => $iProductId));
	    return $this->db->affected_rows();
    }
    
    function setColorData($data){
    	$this->db->insert('product_colors', $data);
        return $this->db->insert_id();
    }
    
    function check_colors($vColorName){
        $this->db->select('iProductColorId,vColorName');
        $this->db->from('product_colors');
        $this->db->where('vColorName',$vColorName);
        return $this->db->get()->row_array();
    }
    
    function setColorRelationData($data){
    	$this->db->insert('product_colors_relation', $data);
        return $this->db->insert_id();
    }
    
    function setSizeData($data){
    	$this->db->insert('product_sizes', $data);
        return $this->db->insert_id();
    }
    
    function check_sizes($vSizeName){
        $this->db->select('iProductSizeId,vSizeName');
        $this->db->from('product_sizes');
        $this->db->where('vSizeName',$vSizeName);
        return $this->db->get()->row_array();
    }
    
    function setSizeRelationData($data){
    	$this->db->insert('product_sizes_relation', $data);
        return $this->db->insert_id();
    }
    
    function check_product($vProductName){
        $this->db->select('iProductId,vName');
        $this->db->from('products');
        $this->db->where('vName',$vProductName);
        return $this->db->get()->row_array();
    }
}
?>


