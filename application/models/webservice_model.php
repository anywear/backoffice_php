<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class webservice_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function checklogin($vEmail,$vPassword)
    {
        $q = $this->db->get_where('user',array('vEmail' => $vEmail,'vPassword' => $vPassword));
        return $q->row_array();
    }
    
    function userRegister($data){
        $this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    
    function ChackEmail($vEmail){
        $this->db->select('vEmail');
        $this->db->from('user');
        $this->db->where('vEmail',$vEmail);
        return $this->db->get()->row_array();
    }
    
    function updateUserRegister($iUserId, $data)
    {
        $this->db->where('iUserId', $iUserId);
        $query = $this->db->update('user',$data); 
        return $query; 
    }
    
    function getSliderData(){
        $this->db->select('');
        $this->db->from('slider');
        //$this->db->where('eSliderType',$eSliderType);
        return $this->db->get()->result_array();
    }
    
    function getStyleCategories(){
        $this->db->select('');
        $this->db->from('category');
        return $this->db->get()->result_array();
    }
    
    function getStyleByCategoryId($iCategoryId){
        $this->db->select('');
        $this->db->from('personalization');
        $this->db->where('iCategoryId',$iCategoryId);
        return $this->db->get()->result_array();
    }
    
    function getGlobalSize($eSizeType){
        $this->db->select('');
        $this->db->from('global_size');
        $this->db->where('eSizeType',$eSizeType);
	$this->db->order_by('iSorting Asc');
        return $this->db->get()->result_array();
    }
    
    function getSpenOn($eSizeType){
        $this->db->select('');
        $this->db->from('money_spend');
    	$this->db->order_by('iSorting Asc');
        return $this->db->get()->result_array();
    }

    function getProductCategory($eSizeType){
        $this->db->select('');
        $this->db->from('product_categories');
        $this->db->order_by('vProductCatId Asc');
        return $this->db->get()->result_array();
    } 

    function getProductBrand($eSizeType){
        $this->db->select('');
        $this->db->from('brands');
        $this->db->order_by('iBrandId Asc');
        return $this->db->get()->result_array();
    } 

    function getProductSize($eSizeType){
        $this->db->select('');
        $this->db->from('product_sizes');
        $this->db->order_by('iProductSizeId Asc');
        return $this->db->get()->result_array();
    }

    function getProductColor($eSizeType){
        $this->db->select('');
        $this->db->from('product_colors');
        $this->db->order_by('iProductColorId Asc');
        return $this->db->get()->result_array();
    }

    function getStyleLooking($eSizeType){
        $this->db->select('');
        $this->db->from('style_looking');
        $this->db->order_by('iStyleLookingId Asc');
        return $this->db->get()->result_array();
    }

    function getStore($eSizeType){
        $this->db->select('');
        $this->db->from('store');
        $this->db->order_by('iStoreId Asc');
        return $this->db->get()->result_array();
    }
}
?>