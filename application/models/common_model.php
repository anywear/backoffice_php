<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

// USE FOR DELETE IMAGE
    function list_sysemaildata($EmailCode){  
        $this->db->select('');  
        $this->db->from('emailtemplate');
        $this->db->where('vEmailCode',$EmailCode);
        return $this->db->get();
    }   

// update the status of record (single or multiple)
    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

// USE FOR GET ALL CATEGORIE'S IMAGE
    function get_category_details($fieldId,$tableData){ 
        $this->db->select($tableData['image_field']);
        $this->db->from($tableData['tablename']);
        $this->db->where($tableData['update_field'],$fieldId);   
        $query = $this->db->get();
        return $query->row_array();
    }

// USE FOR DELETE RECORDS
    function delete_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

// USE FOR DELETE IMAGE
    function delete_image($tableData){ 
        $data[$tableData['image_field']] = '';
        $this->db->where($tableData['update_field'], $tableData['field_id']);
        return $this->db->update($tableData['tablename'], $data);        
    }


    // USE FOR DELETE IMAGE
    function get_admin_details($iAdminId){ 
        $this->db->select('iAdminId,vFirstName,vLastName,vEmail');
        $this->db->from('administrators');
        $this->db->where('iAdminId',$iAdminId);
        $query = $this->db->get();
        return $query->row_array();
    }

     function get_sub_driver_category_details($fieldId,$tableData){ 
        $this->db->select($tableData['image_field']);
        $this->db->from($tableData['tablename']);
        $this->db->where($tableData['update_field'],$fieldId);   
        $query = $this->db->get();
        return $query->row_array();
    }

    //get all country list
    function get_country(){
        $this->db->select('iCountryId,vCountry');
        $this->db->from('country');
        $query = $this->db->get();
        return $query->result_array();
    }

    //get all country list
    function getcity(){
        $this->db->select('vCityName,iCityId');
        $this->db->from('city');
        $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_city($iCityId){
        $this->db->select('iCityId,vCity');
        $this->db->from('city');
        $this->db->where('iCityId',$iCityId);
        $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->row_array();    
    }

    //get all country list
    function getslider(){
        $this->db->select('iBannerId,vBannerImage,vBannerTitle,tDescription');
        $this->db->from('banner');
        $this->db->where('eStatus','Active');
        $this->db->order_by('iOrderNo','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }    

    //get all country list
    function getpopularpaces(){
        $this->db->select('iPopularPlaceId,vPlaceTitle,vPlaceImage,fLatitude,fLongitude,taddress');
        $this->db->from('popular_place');
        $this->db->where('eStatus','Active');
        $this->db->where('fLatitude !=','');
        $this->db->where('fLongitude !=','');
        $this->db->where('taddress !=','');
        $this->db->order_by('iPopularPlaceId','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_country_code($iCountryId){
        $this->db->select('vCountryMobileCode');
        $this->db->from('country');
        $this->db->where('iCountryId', $iCountryId);      
        $query = $this->db->get()->row_array();
        return $query['vCountryMobileCode'];    
    }

    // Add Contact Us
    function add_contact_us($data){
        $query=$this->db->insert('contact_us', $data);
        return $this->db->insert_id();
    }

    function getpages($vPagecode){
        $this->db->select('iSPageId,vPageName,tContent_en,eStatus');
        $this->db->from('static_pages');
        $this->db->where('vPageTitle',$vPagecode);
        $this->db->where('eStatus','Active');
        return $this->db->get()->row_array();
    
    }

    function get_driver_details($iDriverId){
        $this->db->select('iDriverId,vFirstName,vLastName,vEmail,vProfileImage,iParentDriverId');
        $this->db->from('driver');
        $this->db->where('iDriverId',$iDriverId);
        $query = $this->db->get();
        return $query->row_array();
    }

      function get_client_details($iClientId){
        $this->db->select('iClientId,vFirstName,vLastName,vEmail,vProfileImage');
        $this->db->from('client');
        $this->db->where('iClientId',$iClientId);
        $query = $this->db->get();
        return $query->row_array();
    }

    //get all Faq category list
    function get_faqcategory(){
        $this->db->select('iFaqCategoryId,vFaqCategoryName,vImage');
        $this->db->from('faq_category');
        $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_faqquestion($num_limit,$rec_limit,$pagingflag,$iFaqCategoryId){
        $this->db->select('f.iFaqId,f.vQuestion,f.vAnswer,fc.vFaqCategoryName,f.iFaqCategoryId');
        $this->db->from('faq as f');
        $this->db->join('faq_category as fc','fc.iFaqCategoryId=f.iFaqCategoryId');
        $this->db->where('f.iFaqCategoryId',$iFaqCategoryId);
        $this->db->where('f.eStatus','Active');
        if($pagingflag !=''){
            $this->db->limit($rec_limit,$num_limit);    
        }
        $query = $this->db->get();
        return $query->result_array();    
    }

    function yourdriver($num_limit,$rec_limit,$pagingflag){
        $this->db->select('iDriverId,vFirstName,vLastName,vProfileImage,tDescrption,iParentDriverId');
        $this->db->from('driver');
        $this->db->where('eStatus','Active');
        $this->db->order_by('iDriverId','desc');
        if($pagingflag !=''){
            $this->db->limit($rec_limit,$num_limit);    
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    function yourteam($num_limit,$rec_limit,$pagingflag){
        $this->db->select('iTeamMemberId,vFirstName,vLastName,vProfileImage,tDescrption');
        $this->db->from('your_team');
        $this->db->where('eStatus','Active');
        $this->db->order_by('iTeamMemberId','desc');
        if($pagingflag !=''){
            $this->db->limit($rec_limit,$num_limit);    
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function popularPlaceDetail($iPopularPlaceId){
        $this->db->select('');
        $this->db->from('popular_place');
        $this->db->where('iPopularPlaceId',$iPopularPlaceId);
        return $this->db->get()->row_array();
    }
}
?>