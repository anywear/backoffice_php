<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class slider_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_slider_listing(){
        $this->db->select('');
        $this->db->from('slider');
        $this->db->order_by('iSliderId desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_slider($data){
        $query=$this->db->insert('slider', $data);
        return $this->db->insert_id();
    }
    
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }
    
    function get_slider_details($iSliderId){
        $this->db->select('');
        $this->db->from('slider');
        $this->db->where('iSliderId', $iSliderId);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function update($iSliderId, $data)
    {
        $this->db->where('iSliderId', $iSliderId);
        $query = $this->db->update('slider',$data); 
        return $query; 
    }
    
    function delete_image($iSliderId)
    {
        $data['vImage'] = '';
        $this->db->where('iSliderId', $iSliderId);
        return $this->db->update('slider', $data);
    }
    
    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }
}
?>