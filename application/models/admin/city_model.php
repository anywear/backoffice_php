<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class city_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    
    function get_list(){
        $this->db->select('');
        $this->db->from('city');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_city(){
        $this->db->select('');
        $this->db->from("city");
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_city($data){
        $this->db->insert('city', $data);
        return $this->db->insert_id();
    }

    function get_city_details($iCityId){
        $this->db->select('');
        $this->db->from('city');
        $this->db->where('iCityId', $iCityId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iCityId', $ids);
        $this->db->delete('city');    
    }

    function edit_store($data){
        $this->db->update("city", $data, array('iCityId' => $data['iCityId']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iCityId, $data)
    {
        $this->db->where('iCityId', $iCityId);
        $query = $this->db->update('city',$data); 
        return $query; 
    }
}
?>

