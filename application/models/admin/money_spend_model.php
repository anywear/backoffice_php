<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class money_spend_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function count_all(){
        $this->db->select("COUNT('iSpendId') AS tot");
        $this->db->from('money_spend');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('money_spend');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_money_spend(){
        $this->db->select('');
        $this->db->from("money_spend");
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_money_spend($data){
        // echo "<pre>";print_r($data);exit;
        $this->db->insert('money_spend', $data);
        return $this->db->insert_id();
    }

    function get_size_details($iSpendId){
        $this->db->select('');
        $this->db->from('money_spend');
        $this->db->where('iSpendId', $iSpendId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function edit_store($data){
        $this->db->update("money_spend", $data, array('iSpendId' => $data['iSpendId']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iSpendId, $data)
    {
        // echo "<pre>";print_r($data);exit;
        $this->db->where('iSpendId', $iSpendId);
        $query = $this->db->update('money_spend',$data); 
        return $query; 
    }
}
?>

