<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class size_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function count_all(){
        $this->db->select("COUNT('iSizeId') AS tot");
        $this->db->from('global_size');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('global_size');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_size(){
        $this->db->select('');
        $this->db->from("global_size");
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_size($data){
        $this->db->insert('global_size', $data);
        return $this->db->insert_id();
    }

    function get_size_details($iSizeId){
        $this->db->select('');
        $this->db->from('global_size');
        $this->db->where('iSizeId', $iSizeId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iSizeId', $ids);
        $this->db->delete('global_size');    
    }


    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iSizeId, $data)
    {
        $this->db->where('iSizeId', $iSizeId);
        $query = $this->db->update('global_size',$data); 
        return $query; 
    }
}
?>

