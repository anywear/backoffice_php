<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class style_looking_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('style_looking');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_style_looking(){
        $this->db->select('');
        $this->db->from("style_looking");
        $this->db->order_by('iStyleLookingId desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_style_looking($data){
        $this->db->insert('style_looking', $data);
        return $this->db->insert_id();
    }

    function get_style_looking_details($iSizeId){
        $this->db->select('');
        $this->db->from('style_looking');
        $this->db->where('iStyleLookingId', $iSizeId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iStyleLookingId', $ids);
        $this->db->delete('style_looking');    
    }

    function edit_store($data){
        $this->db->update("style_looking", $data, array('iStyleLookingId' => $data['iStyleLookingId']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iStyleLookingId, $data)
    {
        $this->db->where('iStyleLookingId', $iStyleLookingId);
        $query = $this->db->update('style_looking',$data); 
        return $query; 
    }
}
?>

