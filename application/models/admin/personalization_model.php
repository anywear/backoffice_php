<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class personalization_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_personalization($iCategoryId){
        $this->db->select('');
        $this->db->from('personalization');
        $this->db->order_by('iPersonalId desc');
        $this->db->where('iCategoryId',$iCategoryId);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_personalization_by_id($iPersonalId){
        $this->db->select('');
        $this->db->from('personalization');
        $this->db->order_by('iPersonalId desc');
        $this->db->where('iPersonalId',$iPersonalId);
        $query = $this->db->get();
        return $query->row_array();
    }
    
    function save_cat($data){
    	// if cat ID exist, update record
    	foreach ($data as $key => $value){
    		if($value['iCategoryId']){
	    		$this->db->update("category", $value, array('iCategoryId' => $value['iCategoryId']));
	        	$res[] = $this->db->affected_rows();
	    	}else{
	    		$this->db->insert('category', $value);
	        	$res[] = $this->db->insert_id();
	    	}
    	}
    	return $res;
    }
	
    function get_category(){
        $this->db->select('');
        $this->db->from('category');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function save_personalize($data){
		$this->db->insert('personalization', $data);
		return $this->db->insert_id();
	}
    
     function updata_image_personalize($data){
    	// if cat ID exist, update record
    	$this->db->update("personalization", $data, array('iPersonalId' => $data['iPersonalId']));
    	return $data['iPersonalId'];
    }
    
    function update_personalize($data){
		$this->db->update("personalization", $data, array('iPersonalId' => $data['iPersonalId']));
    	return $data['iPersonalId'];
	}

    function get_personalization_details($iPersonalId){
        $this->db->select('');
        $this->db->from('personalization');
        $this->db->where('iPersonalId', $iPersonalId);
        $query = $this->db->get();
        return $query->row_array();
    }

    function delete_image($iPersonalId)
    {
        $data['vImage'] = '';
        $this->db->where('iPersonalId', $iPersonalId);
        return $this->db->update('personalization', $data);
    }

     function delete_record($ids){
        $this->db->where('iPersonalId', $ids);
        return $this->db->delete('personalization');    
    }

}
?>