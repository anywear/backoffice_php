<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_categories_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function count_all(){
        $this->db->select("COUNT('id') AS tot");
        $this->db->from('product_categories');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_list(){
        $this->db->select('id,vName,vShortName,vSizeTitle,vLocalizedId,eDataFrom');
        $this->db->from('product_categories');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_product_categories_listing(){
        $this->db->select('');
        $this->db->from("product_categories");
        $this->db->order_by('id desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_product_categories($data){
        $this->db->insert('product_categories', $data);
        return $this->db->insert_id();
    }

    function get_product_categories_details($id){
        $this->db->select('');
        $this->db->from('product_categories');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('id', $ids);
        $this->db->delete('product_categories');    
    }

    function edit_store($data){
        $this->db->update("product_categories", $data, array('id' => $data['id']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('product_categories',$data); 
        return $query; 
    }
    
    function get_parent_product_category($vParentId='clothes-shoes-and-jewelry', $old_cat="",$icatIdNot="clothes-shoes-and-jewelry",$loop=1,$vProductCatId){
        global $par_arr_new;
        $sql_query = "select vProductCatId,vName,id, vParentId from product_categories  where vParentId='$vParentId'";
        $db_tech_prs = $this->db->query($sql_query)->result_array();
        $cnt = count($db_tech_prs);
        if($cnt>0){
            for($i=0 ; $i<$cnt ; $i++){
                $par_arr_new[] = array('vProductCatId'=> $db_tech_prs[$i]['vProductCatId'],'vParentId'=> $db_tech_prs[$i]['vParentId'],'vName' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vName']);
                $this->get_parent_product_category($db_tech_prs[$i]['vProductCatId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$vProductCatId);
            }
            $old_cat = "";
        }
        return $par_arr_new;
    }
}
?>

