<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_management_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}

	function get_admin_details($iAdminId){
		$this->db->select('');
		$this->db->from('administrators');
		$this->db->where('iAdminId', $iAdminId);		
		$query = $this->db->get();
		return $query->row_array();
	}

	function edit_admin($data){
		$this->db->update("administrators", $data, array('iAdminId' => $data['iAdminId']));
		return $this->db->affected_rows();   
	}

	function add_admin($data){
		$this->db->insert('administrators', $data);
		return $this->db->insert_id();
	}
	
	function get_all_admins() {
		$this->db->select('');
		$this->db->from('administrators');
		$this->db->order_by('iAdminId desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	 function admin_exists($vEmail){
		$this->db->select('');
		$this->db->from('administrators');
		$this->db->where('vEmail', $vEmail);	
		$query = $this->db->get();
		return $query->num_rows();
	}

	function delete_admin_details($iAdminId,$table){
        $this->db->where('iAdminId', $iAdminId);
        $this->db->delete($table); 
        return $this->db->affected_rows();
    }

    function delete_admin_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }
}
?>