<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class retailer_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }


    function get_list(){
        $this->db->select('');
        $this->db->from('retailers');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_retailer_listing(){
        $this->db->select('');
        $this->db->from("retailers");
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_retailer($data){
        $this->db->insert('retailers', $data);
        return $this->db->insert_id();
    }

    function get_retailer_details($iRetailerId){
        $this->db->select('');
        $this->db->from('retailers');
        $this->db->where('iRetailerId', $iRetailerId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_retailers_record($iRetailerIds,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $iRetailerIds);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($iRetailerIds){
        $this->db->where('iRetailerId', $iRetailerIds);
        $this->db->delete('retailers');    
    }

    function edit_store($data){
        $this->db->update("retailers", $data, array('iRetailerId' => $data['iRetailerId']));
        return $this->db->affected_rows();
    }

    function get_update_all($iRetailerIds,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $iRetailerIds);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iRetailerId, $data)
    {
        $this->db->where('iRetailerId', $iRetailerId);
        $query = $this->db->update('retailers',$data); 
        return $query; 
    }

}
?>

