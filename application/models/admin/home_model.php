<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('administrator');
        $query = $this->db->get();
        return $query->result_array();
    }

    function change_password($data){
        $this->db->update("administrator", $data, array('iAdminId' => $data['iAdminId']));
        return $this->db->affected_rows();   
    }

    function get_all_store(){
        $this->db->select('');
        $this->db->from("store");
        $this->db->order_by('iStoreId DESC');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    function set_favorite($data){
    	
    	$this->db->select('iStoreId,eFav');
        $this->db->from("store");
        $this->db->where('iStoreId',$data['iStoreId']);
        $res=$this->db->get()->row_array();
        
        if($res['eFav'] == 'No'){
        	$data['eFav'] = 'Yes';
        }else{
        	$data['eFav'] = 'No';
    	}
    	
    	$this->db->update("store", $data, array('iStoreId' => $data['iStoreId']));
        return $data['eFav']; 
    }

    function set_onSale($data){
        $this->db->select('iStoreId,eOnSale');
        $this->db->from("store");
        $this->db->where('iStoreId',$data['iStoreId']);
        $res=$this->db->get()->row_array();
        
        if($res['eOnSale'] == 'No'){
            $data['eOnSale'] = 'Yes';
        }else{
            $data['eOnSale'] = 'No';
        }
        
        $this->db->update("store", $data, array('iStoreId' => $data['iStoreId']));
        return $data['eOnSale']; 
    }
    function add_store($data){
        $this->db->insert('store', $data);
        return $this->db->insert_id();
    }

    function add_shop_image($iStoreId,$data){
        $arr=array('iStoreId' =>$iStoreId,'vShopImage'=>$data);
        $this->db->insert('store_shop_images',$arr);
    }
    function get_store_details($iStoreId){
        $this->db->select('');
        $this->db->from('store');
        $this->db->where('iStoreId',$iStoreId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_store_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iStoreId', $ids);
        $this->db->delete('store');    
    }

    function edit_store($data){
        $this->db->update("store", $data, array('iStoreId' => $data['iStoreId']));
        return $this->db->affected_rows();
    }

    function set_Store_onSale($Data)
    {
        $iStoreId =$Data['iStoreId'];
        $onsale['eOnSale'] = $Data['eOnSale'];
       $this->db->where_in('iStoreId', $iStoreId);
        $this->db->update("store", $onsale);
        return $this->db->affected_rows(); 
    }

    function get_all_style(){
        $this->db->select('iPersonalId,vStyle');
        $this->db->from('personalization');
        // $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->result_array();        
    }

    function get_all_style_looking(){
        $this->db->select('iStyleLookingId,vStyleLookingName');
        $this->db->from('style_looking');
        $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_store_size(){
        $this->db->select('iStoreSizeId,vSizeTitle');
        $this->db->from('store_size');
        $this->db->where('eStatus','Active');
        $this->db->order_by('iSorting');
        $query = $this->db->get();
        return $query->result_array();   
    }

    function get_all_store_category(){
        $this->db->select('iStoreCategoryId,vStoreCategoryName');
        $this->db->from('store_category');
        $this->db->where('eStatus','Active');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_money_spend(){
        $this->db->select('iSpendId,eSign');
        $this->db->from('money_spend');
        $this->db->where('eStatus','Active');
        $this->db->order_by('iSorting');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_store_city($iCityId){
        $this->db->select('iCityId,vCityName');
        $this->db->from('city');
        $this->db->where('iCityId',$iCityId);
        $query=$this->db->get();
        return $query->row_array();
    }

    function get_all_store_with_city($vCityName){
        $this->db->select('');
        $this->db->from("store");
        $this->db->where('vArea',$vCityName);
        $query=$this->db->get();
        return $query->result_array();
        
    }
}
?>

