<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class user_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function check_auth($vEmail,$vPassword){
        $this->db->select('');
        $this->db->from('administrator');
        $this->db->where('vEmail', $vEmail);
        $this->db->where('vPassword', $vPassword);
        $query = $this->db->get();
        return $query->row_array();
    }

    function count_user(){
        $this->db->select('u.iUserId,u.vFirstName,u.vLastName,u.vEmail');
        $this->db->from('user as u');
        $this->db->order_by('iUserId desc');
        return $this->db->count_all_results();
    }

    function get_user(){
        $this->db->select('');
        $this->db->from('user');
        $this->db->order_by('iUserId desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function today_signup_user(){
        $today=date("Y-m-d");
        $this->db->select('iUserId,vFirstName,vLastName,vEmail,eStatus,dRegisterDate');
        $this->db->from('user');
        $this->db->where('dRegisterDate', $today);      
        $this->db->order_by('iUserId desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function add_user($data){
        $query=$this->db->insert('user', $data);
        return $this->db->insert_id();
    }
    
    function edit_user($data){
        $this->db->update("user", $data, array('iUserId' => $data['iUserId']));
        return $this->db->affected_rows();   
    }

    function get_user_details($iUserId){ 
        $this->db->select('');
        $this->db->from('user');
        $this->db->where('iUserId', $iUserId);      
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_country(){
        $this->db->select('iCountryId,vCountry');
        $this->db->from('country');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_country_code($iCountryId){
        $this->db->select('iCountryId,vCountry');
        $this->db->from('country');
        $this->db->where('iCountryId', $iCountryId);      
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_states_code($iStateId){
        $this->db->select('iStateId,iCountryId,vState');
        $this->db->from('state');
        $this->db->where('iStateId',$iStateId);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_allstates(){
        $this->db->select('iStateId,vState');
        $this->db->from('state');
        $this->db->where('eStatus', 'Active');      
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_allstates_bycountry($iCountryId){
        $this->db->select('iStateId,vState,iCountryId');
        $this->db->from('state');
        $this->db->where('iCountryId', $iCountryId);
        $this->db->where('eStatus', 'Active');      
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function delete_image($iUserId){ 
        $data['vProfileImage'] = '';
        $this->db->where('iUserId', $iUserId);
        return $this->db->update('user', $data);
    }
    
    function getuser_by_mail($mail){
        $this->db->select('');
        $this->db->from('user');
        $this->db->where('vEmail',$mail);      
        $query = $this->db->get();
        return $query->result_array();        
    }

    function get_cities($iCountryId,$iStateId){
        $this->db->select('');
        $this->db->from('city');
        $this->db->where('iCountryId',$iCountryId);      
        $this->db->where('iStateId',$iStateId); 
        $query = $this->db->get();
        return $query->result_array(); 
    }
    
    function get_user_fbid($iUserId){
        $this->db->select('vFBId');
        $this->db->from('user');
        $this->db->where('iUserId',$iUserId);      
        $query = $this->db->get();
        return $query->row_array();        
    }
    
    function get_user_gallery($vFBId){
        $this->db->select('');
        $this->db->from('user_gallery');
        $this->db->where('vFBId',$vFBId);
        $query = $this->db->get();
        return $query->result_array();
    }

    function add_AllImages($data){
        $query=$this->db->insert('image_gallery', $data);
        return $this->db->insert_id();      
    }
    function get_AllImages_gallery($vFBId){
        $this->db->select('');
        $this->db->from('image_gallery');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_user_email($iUserId){ 
        $this->db->select('');
        $this->db->from('user');
        $this->db->where('iUserId', $iUserId);      
        $query = $this->db->get();
        return $query->row_array();
    }
}
?>