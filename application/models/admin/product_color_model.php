<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_color_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function count_all(){
        $this->db->select("COUNT('iProductColorId') AS tot");
        $this->db->from('store_size');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('store_size');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_color(){
        $this->db->select('');
        $this->db->from("product_colors");
        $this->db->order_by('iProductColorId desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    function get_parent_color_id($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iProductColorId){
        global $par_arr_new;
        $sql_query = "select iProductColorId, vColorName, iParentId from product_colors  where iParentId='$iParentId' and eStatus='Active'";
        $db_tech_prs = $this->db->query($sql_query)->result_array();
        $cnt = count($db_tech_prs);
        if($cnt>0){
            for($i=0 ; $i<$cnt ; $i++){
                $par_arr_new[] = array('iProductColorId'=> $db_tech_prs[$i]['iProductColorId'],'iParentId'=> $db_tech_prs[$i]['iParentId'],'vColorName' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vColorName']);
                $this->get_parent_color_id($db_tech_prs[$i]['iProductColorId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iProductColorId);
            }
            $old_cat = "";
        }
        return $par_arr_new;
    }

    function add_color($data){
        $this->db->insert('product_colors', $data);
        return $this->db->insert_id();
    }

    function get_color_details($iProductColorId){
        $this->db->select('');
        $this->db->from('product_colors');
        $this->db->where('iProductColorId', $iProductColorId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iProductColorId', $ids);
        $this->db->delete('product_colors');    
    }

    function get_peoduct_color_details($iProductColorId){
        $this->db->select('iProductColorId,iParentId,vColorName,eStatus');
        $this->db->from('product_colors');
        $this->db->where('iProductColorId',$iProductColorId);   
        $query = $this->db->get();
        return $query->row_array();
    }

     function getchild($iCategoryId){
        $this->db->select('iProductColorId,iParentId,vColorName,eStatus');
        $this->db->from('product_colors');
        $this->db->where('iProductColorId',$iProductColorId);   
        $query = $this->db->get();
        return $query->row_array();   
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iProductColorId, $data)
    {
        $this->db->where('iProductColorId', $iProductColorId);
        $query = $this->db->update('product_colors',$data); 
        return $query; 
    }
}
?>

