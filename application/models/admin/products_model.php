<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class products_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_all_product($iStoreId){
        $this->db->select('iProductId,iStoreId,vName,dAddedDate,eStatus,eDataFrom');
        $this->db->from("products");
        $this->db->where('iStoreId',$iStoreId);
        $query=$this->db->get();
        return $query->result_array();
        
    }

    function add_product($data){
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }
    
    function add_color_relation($data){
        $this->db->insert('product_colors_relation', $data);
        return $this->db->insert_id();
    }
    
    function add_size_relation($data){
        $this->db->insert('product_sizes_relation', $data);
        return $this->db->insert_id();
    }
    
    function add_category_relation($data){
        $this->db->insert('product_category_relation', $data);
        return $this->db->insert_id();
    }

    function edit_products($data){
        $this->db->update("products", $data, array('iProductId' => $data['iProductId']));
        return $this->db->affected_rows();
    }

    function get_all_brand_id(){
        $this->db->select('iBrandId,vName');
        $this->db->from('brands');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_all_color(){
        $this->db->select('iProductColorId,vColorName');
        $this->db->from('product_colors');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_all_size(){
        $this->db->select('iProductSizeId,vSizeName');
        $this->db->from('product_sizes');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_all_product_cat(){
        $this->db->select('id,vName');
        $this->db->from('product_categories');
        $query = $this->db->get();
        return $query->result_array();
    }

    
    
    function save_cat($data){
    	// if cat ID exist, update record
    	foreach ($data as $key => $value){
    		if($value['iCategoryId']){
	    		$this->db->update("category", $value, array('iCategoryId' => $value['iCategoryId']));
	        	$res[] = $this->db->affected_rows();
	    	}else{
	    		$this->db->insert('category', $value);
	        	$res[] = $this->db->insert_id();
	    	}
    	}
    	return $res;
    }
	
    function get_category(){
        $this->db->select('');
        $this->db->from('category');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function save_personalize($data){
		$this->db->insert('products', $data);
		return $this->db->insert_id();
	}
    
     function updata_image_personalize($data){
    	// if cat ID exist, update record
    	$this->db->update("products", $data, array('iProductId' => $data['iProductId']));
    	return $data['iProductId'];
    }
    
    function update_personalize($data){
		$this->db->update("iProductId", $data, array('iProductId' => $data['iProductId']));
    	return $data['iProductId'];
	}

    function get_product_details($iProductId){
        $this->db->select('');
        $this->db->from('products');
        $this->db->where('iProductId', $iProductId);
        $query = $this->db->get();
        return $query->row_array();
    }

    function delete_image($iProductId)
    {
        $data['vImage'] = '';
        $this->db->where('iProductId', $iProductId);
        return $this->db->update('products', $data);
    }

     function delete_record($ids){
        $this->db->where('iProductId', $ids);
        return $this->db->delete('products');    
    }

    function get_all_product_color_by_id($iProductId){
        $this->db->select('');
        $this->db->from('product_colors_relation');
        $this->db->where('iProductId', $iProductId);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_product_size_by_id($iProductId){
        $this->db->select('');
        $this->db->from('product_sizes_relation');
        $this->db->where('iProductId', $iProductId);
        $query = $this->db->get();
        return $query->result_array();
    }

    function edit_color_relation($data){
        $this->db->update("product_colors_relation", $data, array('iProductId' => $data['iProductId']));
        return $data['iProductId'];
    }

    function edit_size_relation($data){
        $this->db->update("product_sizes_relation", $data, array('iProductId' => $data['iProductId']));
        return $data['iProductId'];
    }

    function edit_category_relation($data){
        $this->db->update("product_category_relation", $data, array('iProductId' => $data['iProductId']));
        return $data['iProductId'];
    }

}
?>