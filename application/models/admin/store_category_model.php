<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class store_category_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('store_category');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_store_category_looking(){
        $this->db->select('');
        $this->db->from("store_category");
        $query=$this->db->get();
        return $query->result_array();
    }
    
    
    function add_style_looking($data){
        $this->db->insert('store_category', $data);
        return $this->db->insert_id();
    }

    function get_store_category_details($iStoreCategoryId){
        $this->db->select('');
        $this->db->from('store_category');
        $this->db->where('iStoreCategoryId', $iStoreCategoryId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iStoreCategoryId', $ids);
        $this->db->delete('store_category');    
    }

    function edit_store($data){
        $this->db->update("store_category", $data, array('iStoreCategoryId' => $data['iStoreCategoryId']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iStoreCategoryId, $data)
    {
        $this->db->where('iStoreCategoryId', $iStoreCategoryId);
        $query = $this->db->update('store_category',$data); 
        return $query; 
    }
}
?>

