<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class notification_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_notification_listing(){
        $this->db->select('');
        $this->db->from('notification');
        $this->db->order_by('iNotificationId desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    function add_notify($data){
        $query=$this->db->insert('notification', $data);
        return $this->db->insert_id();
    }
    
    function delete_notification_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function get_all_user_email(){
        $this->db->select('iUserId,vEmail');
        $this->db->from('user');
        $this->db->order_by('iUserId desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    function get_notification($ids){
        $this->db->select('iNotificationId,vTitle,tDescription');
        $this->db->from('notification');
        $this->db->where('iNotificationId', $ids);
        $query = $this->db->get();
        return $query->row_array();
    }
}
?>