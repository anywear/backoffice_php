<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_size_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function count_all(){
        $this->db->select("COUNT('iStoreSizeId') AS tot");
        $this->db->from('store_size');
        $query = $this->db->get();
        return $query->row_array();
    }

    function get_list(){
        $this->db->select('');
        $this->db->from('store_size');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_size(){
        $this->db->select('');
        $this->db->from("product_sizes");
        $this->db->order_by('iProductSizeId desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    function get_parent_size_id($iParentId=0, $old_cat="",$icatIdNot="0",$loop=1,$iProductSizeId){
        global $par_arr_new;
        $sql_query = "select iProductSizeId, vSizeName, iParentId from product_sizes  where iParentId='$iParentId' and eStatus='Active'";
        $db_tech_prs = $this->db->query($sql_query)->result_array();
        $cnt = count($db_tech_prs);
        if($cnt>0){
            for($i=0 ; $i<$cnt ; $i++){
                $par_arr_new[] = array('iProductSizeId'=> $db_tech_prs[$i]['iProductSizeId'],'iParentId'=> $db_tech_prs[$i]['iParentId'],'vSizeName' =>  $old_cat."--|".$loop."|&nbsp;&nbsp;".$db_tech_prs[$i]['vSizeName']);
                $this->get_parent_size_id($db_tech_prs[$i]['iProductSizeId'], $old_cat."&nbsp;&nbsp;&nbsp;&nbsp;",$icatIdNot,$loop+1,$iProductSizeId);
            }
            $old_cat = "";
        }
        
        return $par_arr_new;
    }

    function add_size($data){
        $this->db->insert('product_sizes', $data);
        return $this->db->insert_id();
    }

    function get_size_details($iProductSizeId){
        $this->db->select('');
        $this->db->from('product_sizes');
        $this->db->where('iProductSizeId', $iProductSizeId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_slider_record($ids,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($ids){
        $this->db->where('iProductSizeId', $ids);
        $this->db->delete('product_sizes');    
    }

    function edit_store($data){
        $this->db->update("store_size", $data, array('iStoreSizeId' => $data['iStoreSizeId']));
        return $this->db->affected_rows();
    }

    function get_update_all($ids,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $ids);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iProductSizeId, $data)
    {
        $this->db->where('iProductSizeId', $iProductSizeId);
        $query = $this->db->update('product_sizes',$data); 
        return $query; 
    }

    /*function get_parent_size_id(){
        $this->db->select('iProductSizeId');
        $this->db->from('product_sizes');
        $query = $this->db->get();
        return $query->result_array();
    }*/


    function get_peoduct_size_details($iProductSizeId){
        $this->db->select('iProductSizeId,iParentId,vSizeName,eStatus');
        $this->db->from('product_sizes');
        $this->db->where('iProductSizeId',$iProductSizeId);   
        $query = $this->db->get();
        return $query->row_array();
    }

    function getchild($iCategoryId){
        $this->db->select('iProductSizeId,iParentId,vSizeName,eStatus');
        $this->db->from('product_sizes');
        $this->db->where('iProductSizeId',$iProductSizeId);   
        $query = $this->db->get();
        return $query->row_array();   
    }
}
?>

