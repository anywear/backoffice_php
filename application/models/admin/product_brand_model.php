<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class product_brand_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }


    function get_list(){
        $this->db->select('');
        $this->db->from('brands');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_product_brand_listing(){
        $this->db->select('');
        $this->db->from("brands");
        $this->db->order_by('iBrandId desc');
        $query=$this->db->get();
        return $query->result_array();
    }
    
    function add_product_brand($data){
        $this->db->insert('brands', $data);
        return $this->db->insert_id();
    }

    function get_product_brand_details($iBrandId){
        $this->db->select('');
        $this->db->from('brands');
        $this->db->where('iBrandId', $iBrandId);
        $query = $this->db->get();
        return $query->row_array();
    }
    function delete_brand_record($iBrandIds,$tableData)
    {
        $this->db->where_in($tableData['update_field'], $iBrandIds);
        $this->db->delete($tableData['tablename']); 
    }

    function delete_record($iBrandIds){
        $this->db->where('iBrandId', $iBrandIds);
        $this->db->delete('brands');    
    }

    function edit_store($data){
        $this->db->update("brands", $data, array('iBrandId' => $data['iBrandId']));
        return $this->db->affected_rows();
    }

    function get_update_all($iBrandIds,$action,$tableData){
        $data = array('eStatus'=>$action);
        $this->db->where_in($tableData['update_field'], $iBrandIds);
        $this->db->update($tableData['tablename'], $data); 
        return $this->db->affected_rows();   
    }

    function update($iBrandId, $data)
    {
        $this->db->where('iBrandId', $iBrandId);
        $query = $this->db->update('brands',$data); 
        return $query; 
    }

}
?>
