
function returnme()
{
    window.location.href = base_url+'product_size';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"product_size/all_product_size_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iProductSizeId",
                "type": "checkbox"
            },
            {
                "label": "Parent Id",
                "name": "iParentId"
            },
            {
                "label": "Size Name",
                "name": "vSizeName"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#sizetable').dataTable( {
        "sAjaxSource": base_url+"product_size/all_product_size_listing",
    "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iProductSizeId","bSortable": false},
            { "mData": "iParentId" },
            { "mData": "vSizeName"},
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );