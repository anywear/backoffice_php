
function returnme()
{
	window.location.href = base_url+'admin_management';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"admin_management/get_admin_listing",
        "domTable": "#admintable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iAdminId",
                "type": "checkbox"
            },
            {
                "label": "First Name:",
                "name": "vFirstName"
            },
            {
                "label": "Last Name:",
                "name": "vLastName"
            },
            {
                "label": "Email:",
                "name": "vEmail"
            }
        ]
    } );
 
    $('#admintable').dataTable( {
        "sAjaxSource": base_url+"admin_management/get_admin_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iAdminId","bSortable": false},
            { "mData": "vFirstName" },
            { "mData": "vLastName" },
            { "mData": "vEmail"},
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );