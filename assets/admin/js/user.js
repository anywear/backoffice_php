$(document).ready(function(){
    $("#btn-save").click(function() {
        if($( "#vFirstName" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter First Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#vLastName" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Last Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        /*if($( "#vProfileImage1" ).val() ==''){
            $(".modal-body").html( "<p>Please Upload an Profile Image!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        if($( "#iCountryId" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Country!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#iStateId" ).val() ==''){
            $(".modal-body").html( "<p>Please Select State!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#vCity" ).val() ==''){
            $(".modal-body").html( "<p>Please Select City!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        /*if($("#iMobileNo" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Mobile Number!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        /*if($( "#vEmail" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Email!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if(IsEmail($( "#vEmail" ).val())==false){
            $(".modal-body").html( "<p>Please Enter Proper Email Address!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/

        var EmailText = $("#vEmail").val();
        if ($.trim(EmailText).length == 0) {
            $(".modal-body").html( "<p>Please Enter Email!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if (validateEmail(EmailText)) {
            //alert('Valid Email Address');
            return true;
        }else {
            $(".modal-body").html( "<p>Invalid Email! Please Enter Proper Email Address!</p>" );
            $("#myalert").modal('show');
            return false;
        }

        if($("#vPassword" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Password!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        /*if($( "#vCreditcardNo" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Creditcard No!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        if($( "#iMonth" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Month!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#iYear" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Year!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        /*if($( "#iCvvNo" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Cvv!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        if($( "#vPostalCode" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Postal Code!</p>" );
            $("#myalert").modal('show');
            return false;
        }
       /* if($( "#vPromotionCode" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Promotion Code.</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        if($( "#eStatus" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Status!</p>" );
            $("#myalert").modal('show');
            return false;
        }
    });
});

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
       return true;
    }else {
        return false;
    }
}

$(document).ready(function(){   
    $( "#iCountryId" ).change(function() {
        iCountryId = $('#iCountryId').val(); 
        url = base_url+"user/get_all_states/"+iCountryId;
        $.post(url,
            function(data) {
                var lang_data = $.parseJSON(data);
                $('#iStateId').html(lang_data);                    
        });
        secondurl = base_url+"driver/Countrycode/"+iCountryId;
        $.post(secondurl,
        function(data) {
            $('#vCountryMobile').val(data);
            $('#vCountryMobileCode').val(data);                                        
        });
    });

    $("#iStateId").change(function() {
        iStateId = $('#iStateId').val(); 
        iCountryId = $('#iCountryId').val(); 
        url = base_url+"user/get_all_cities?iStateId="+iStateId+'&iCountryId='+iCountryId;
        $.post(url,
            function(data) {
                var lang_data = $.parseJSON(data);
                $('#iCityId').html(lang_data);                    
        });
    });
});


function returnme(){
	window.location.href = base_url+'user';
}

function CheckValidFile(val,name)
{
    var a = val.substring(val.lastIndexOf('.') + 1).toLowerCase();  
    if(a == 'gif' || a == 'GIF' || a == 'jpg'  ||a == 'JPG' ||a == 'jpeg' ||a == 'JPEG' ||a == 'png' ||a == 'BMP' ||a == 'bmp' ){
        var size = document.getElementById('vProfileImage').files[0].size;
        if(size >= 5380334)
        {
            $(".modal-body").html( "<p>Please Upload the Image Having Size Upto 5MB!</p>" );
            $("#myalert").modal('show');  
            document.getElementById(name).value = "";
            return false; 
        }
        $("#vProfileImage1").val(val);
        return true;
    }
    else{
        $(".modal-body").html( "<p>Extenstion Is Not Valid. Please Upload Only (gif,jpg,jpeg,bmp,png) Files!!!</p>" );
        $("#myalert").modal('show');  
        document.getElementById(name).value = "";
        return false;     
    }
    
}
function checkprise(events){
    var unicodes=events.charCode? events.charCode :events.keyCode;
    if (unicodes!=8)
    { 
            if( ((unicodes>47 && unicodes<58) || unicodes == 43 || unicodes == 46 )){
                return true;
            }else{
        return false;
        }
    }
}


 // use a global for the submit and return data rendering in the examples
 
$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"user/get_user_listing",
        "domTable": "#userlistId",
        "fields": [ {
                "label": "User Item:",
                "name": "iUserId",
                "type": "checkbox"
            },
            {
                "label": "First Name:",
                "name": "vFirstName"
            }, 
            {
                "label": "Last Name:",
                "name": "vLastName"
            }, 
            {
                "label": "Email:",
                "name": "vEmail"
            }
	    ]
    } );
 
    $('#userlistId').dataTable( {
        "sAjaxSource": base_url+"user/get_user_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iUserId","bSortable": false},
            { "mData": "vFirstName" },
            { "mData": "vLastName" },
            { "mData": "vEmail" }
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );


 

