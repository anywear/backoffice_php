$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"slider/get_slider_listing",
        "domTable": "#sliderid",
        "fields": [ {
                "label": "User Item:",
                "name": "iSliderId",
                "type": "checkbox"
            },
            {
                "label": "Title:",
                "name": "vTitle"
            },
//	    {
//                "label": "Type:",
//                "name": "eSliderType"
//            },
	    {
                "label": "Status:",
                "name": "eStatus"
            },
	    {
                "label": "Control:",
                "name": "editlink"
            }
	    ]
    } );
 
    $('#sliderid').dataTable( {
        "sAjaxSource": base_url+"slider/get_slider_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iSliderId","bSortable": false},
            { "mData": "vTitle" },
	    //{ "mData": "eSliderType" },
	    { "mData": "eStatus"},
	    { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    });
    
    $('.notify-delete').on('click',function(){
            var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
            if(atLeastOneIsChecked == true)
            {
		$("#action").val("Delete");
                $('<div class="modal1 hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button><div class="error_poptit">Delete Record</div></div><div class="eor_poptxt"><h3 id="myModalLabel">Are you sure , You wanted to delete this record ?</h3></div><div class="view_del_user"><a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal" style="margin-left: 43px;" onclick="submitform();">Delete</a><div class="delete_user" style="margin-left:10px" onclick="close_model();"><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></div></div></div>').modal();
                return false;
            }
        return false;
    });
    
    $(".make-active").click(function() {
	$("#action").val("Active");
	var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
	    if(atLeastOneIsChecked == true){
		$("#frmlist").submit();
		return false;
	    }
	});
    
    $(".make-inactive").click(function() {
	$("#action").val("Inactive");
	var atLeastOneIsChecked = $('input[name="iId[]"]:checked').length > 0;
	    if(atLeastOneIsChecked == true){
		$("#frmlist").submit();
		return false;
	    }
	});
     
});

function returnme(){
    window.location.href = base_url+'slider';
}

function submitform() {
    $("#frmlist").submit();
}
 

