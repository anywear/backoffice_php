
function returnme()
{
    window.location.href = base_url+'product_brand';
}


$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"product_brand/all_product_brand_listing",
        "domTable": "#productbrand",
        "fields": [ {
                "label": "Item ID:",
                "name": "iBrandId",
                "type": "checkbox"
            },
            {
                "label": "Name",
                "name": "vName"
            },,
            /*{
                "label": "Synonyms",
                "name": "tSynonyms"
            },*/
            {
                "label": "Data From",
                "name": "eDataFrom"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#productbrand').dataTable( {
        "sAjaxSource": base_url+"product_brand/all_product_brand_listing",
        "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iBrandId","bSortable": false},
            { "mData": "vName"},
            //{ "mData": "tSynonyms"},
            { "mData": "eDataFrom"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );