
function returnme()
{
	window.location.href = base_url+'style_looking';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"style_looking/all_style_looking_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iStyleLookingId",
                "type": "checkbox"
            },
            {
                "label": "Style Looking",
                "name": "vStyleLookingName"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#styletable').dataTable( {
        "sAjaxSource": base_url+"style_looking/all_style_looking_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iStyleLookingId","bSortable": false},
            { "mData": "vStyleLookingName" },
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );