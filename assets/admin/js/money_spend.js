
function returnme()
{
	window.location.href = base_url+'money_spend';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"money_spend/all_money_spend_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iSpendId",
                "type": "checkbox"
            },
            {
                "label": "Size",
                "name": "vTitle"
            },
            {
                "label": "SizeType",
                "name": "eSign"
            },
            {
                "label": "Sorting",
                "name": "iSorting"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#sizetable').dataTable( {
        "sAjaxSource": base_url+"money_spend/all_money_spend_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iSpendId","bSortable": false},
            { "mData": "vTitle" },
            { "mData": "eSign" },
            { "mData": "iSorting"},
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );