$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"notification/get_notification_listing",
        "domTable": "#notificationid",
        "fields": [ /*{
                "label": "User Item:",
                "name": "iNotificationId",
                "type": "checkbox"
            },*/
            {
                "label": "id:",
                "name": "iNotificationId"
            },
            {
                "label": "Notification:",
                "name": "tDescription"
            },
            {
                "label": "Date:",
                "name": "dCurrentDate"
            }
	    ]
    } );
 
    $('#notificationid').dataTable( {
        "sAjaxSource": base_url+"notification/get_notification_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            //{ "mData": "iNotificationId"},
            { "mData": "iNotificationId"},
            { "mData": "tDescription"},
            { "mData": "dCurrentDate"}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );


 

