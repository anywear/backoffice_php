$(document).ready(function(){       
$( "#iCountryId" ).change(function() {
        iCountryId = $('#iCountryId').val(); 
        url = base_url+"user/get_all_states/"+iCountryId;
          $.post(url,
                function(data) {
                    var lang_data = $.parseJSON(data);
                    $('#iStateId').html(lang_data);                    
                });
    });
});



function check_user_email() {

        if($( "#vFirstName" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter First Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#vLastName" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Last Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#iRoleId" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Role!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#vEmail" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Email!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if(IsEmail($( "#vEmail" ).val())==false){
            $(".modal-body").html( "<p>Please Enter Proper Email Address!</p>" );
        	$("#myalert").modal('show');
            return false;
        }
	    if($( "#vUsername" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter User Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#vPassword" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Password!</p>" );
            $("#myalert").modal('show');
            return false;
        }
	    if($( "#vProfileImage" ).val() ==''){
            $(".modal-body").html( "<p>Please Upload an Profile Image!</p>" );
            $("#myalert").modal('show');
            return false;
        }
	    if($( "#iCountryId" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Country!</p>" );
            $("#myalert").modal('show');
            return false;
        }
	    if($( "#vCity" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter City!</p>" );
            $("#myalert").modal('show');
            return false;
        }
	    if($( "#vPhone" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Contact No!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#vAddress" ).val() ==''){
            $(".modal-body").html( "<p>Please Enter Address!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($( "#eStatus" ).val() ==''){
            $(".modal-body").html( "<p>Please Select Status!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        var formdata=$("#save_myprofile").serialize();
        var url=base_url+'myprofile/check_username?';
        
        $("#loader").html('<img src="'+base_image+'ajax-loader.gif">');        
        $.post(url+formdata,function(data){
            $("#btn-save").attr("onclick","");

            if (data=='exitst') {
                
                $(".modal-body").html( "<p>Email Address Already Exists, Please Try Another One!!</p>" );
                $("#myalert").modal('show');
                $("#loader").html('');
                $("#btn-save").attr("onclick","return check_user_email();");
                return false;
            }else{
                $("#save_myprofile").submit();
            }
        });
}
function returnme()
{
	window.location.href = base_url+'home';
}
function imageName(val1){    
    $("#vProfileImage1").val(val1);
}
function CheckValidFile(val,name)
{
    var a = val.substring(val.lastIndexOf('.') + 1).toLowerCase();  
    if(a == 'gif' || a == 'GIF' || a == 'jpg'  ||a == 'JPG' ||a == 'jpeg' ||a == 'JPEG' ||a == 'png' ||a == 'BMP' ||a == 'bmp' )
    return true;
    $(".modal-body").html( "<p>Extenstion is not valid . Please upload only (gif,jpg,jpeg,bmp) Files!!!</p>" );
    $("#myalert").modal('show');  
    document.getElementById(name).value = "";
    return false; 
}
function checkprise(events){
    var unicodes=events.charCode? events.charCode :events.keyCode;
    if (unicodes!=8)
    { 
            if( ((unicodes>47 && unicodes<58) || unicodes == 43 || unicodes == 46 )){
                return true;
            }else{
        return false;
        }
    }
}