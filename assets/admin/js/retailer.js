
function returnme()
{
    window.location.href = base_url+'retailer';
}


$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"retailer/all_retailer_listing",
        "domTable": "#retailerid",
        "fields": [ {
                "label": "Item ID:",
                "name": "iRetailerId",
                "type": "checkbox"
            },
            {
                "label": "Name",
                "name": "vName"
            },
            {
                "label": "Deep link Support",
                "name": "eDeeplinkSupport"
            },
            {
                "label": "Host Domain",
                "name": "vHostDomain"
            },
            {
                "label": "Data From",
                "name": "eDataFrom"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#retailerid').dataTable( {
        "sAjaxSource": base_url+"retailer/all_retailer_listing",
        "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iRetailerId","bSortable": false},
            { "mData": "vName"},
            { "mData": "eDeeplinkSupport"},
            { "mData": "vHostDomain"},
            { "mData": "eDataFrom"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );