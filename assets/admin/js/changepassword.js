$(document).ready(function(){
    $("#btn-save").click(function(){
        var pwd=$("#vPassword").val();
        var confirmpwd=$("#confirmpwd").val();
    
        if(pwd.trim() == ''){
            $(".modal-body").html( "<p>Please Enter Password!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if(confirmpwd.trim() == ''){
            $(".modal-body").html( "<p>Please Enter Confirm Password!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        else if(pwd.trim() != confirmpwd.trim()){
            $(".modal-body").html( "<p>New password and confirmation password do not match.!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        document.changepwd.submit();
    }); 
});

function returnme(){
	window.location.href = base_url+'home';
}