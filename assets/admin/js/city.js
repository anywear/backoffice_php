
function returnme()
{
	window.location.href = base_url+'city';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"city/all_city_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iCityId",
                "type": "checkbox"
            },
            {
                "label": "City",
                "name": "vCityName"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#citytable').dataTable( {
        "sAjaxSource": base_url+"city/all_city_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iCityId","bSortable": false},
            { "mData": "vCityName"},
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );