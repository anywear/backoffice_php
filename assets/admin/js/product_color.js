
function returnme()
{
    window.location.href = base_url+'product_color';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"product_color/all_product_color_listing",
        "domTable": "#Colortable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iProductColorId",
                "type": "checkbox"
            },
            {
                "label": "Parent Id",
                "name": "iParentId"
            },
            {
                "label": "Color Name",
                "name": "vColorName"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#Colortable').dataTable( {
        "sAjaxSource": base_url+"product_color/all_product_color_listing",
    "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iProductColorId","bSortable": false},
            { "mData": "iParentId" },
            { "mData": "vColorName"},
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );