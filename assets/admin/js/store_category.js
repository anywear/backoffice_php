
function returnme()
{
	window.location.href = base_url+'store_category';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"store_category/all_store_category_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iStoreCategoryId",
                "type": "checkbox"
            },
            {
                "label": "Style Looking",
                "name": "vStoreCategoryName"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#storetable').dataTable( {
        "sAjaxSource": base_url+"store_category/all_store_category_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iStoreCategoryId","bSortable": false},
            { "mData": "vStoreCategoryName" },
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );