// use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    var _iStoreId = $('#iStoreId').val();
    
    $('#cancel').on('click',function(){
    	window.location.href = base_url+'store/update?tab=product&iStoreId='+_iStoreId;
    });
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"products/all_product_listing?iStoreId="+_iStoreId,
        "domTable": "#producttable",
        "fields": [ {
                "label": "Product ID:",
                "name": "iProductId",
                "type": "checkbox"
            },
            {
                "label": "Name:",
                "name": "vName"
            },
            {
                "label": "Added Type:",
                "name": "eDataFrom"
            },
            {
                "label": "Date:",
                "name": "dAddedDate"
            },{
                "label": "Edit:",
                "name": "editlink"
            }
        ]
    } );
 
    $('#producttable').dataTable( {
        "sAjaxSource": base_url+"products/all_product_listing?iStoreId="+_iStoreId,
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iProductId","bSortable": false},
            { "mData": "vName" },
            { "mData": "eDataFrom" },
            { "mData": "dAddedDate"},
            { "mData": "editlink","bSortable": false }
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );

    
});



