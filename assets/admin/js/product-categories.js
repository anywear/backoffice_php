
function returnme()
{
    window.location.href = base_url+'product_categories';
}


$(document).ready(function() {
    var editor;
    
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"product_categories/all_product_categories_listing",
        "domTable": "#productcategories",
        "fields": [ {
                "label": "Item ID:",
                "name": "id",
                "type": "checkbox"
            },
            //{
            //    "label": "Product Categories",
            //    "name": "vProductCatId"
            //},
            {
                "label": "Name",
                "name": "vName"
            },
            {
                "label": "Short Name",
                "name": "vShortName"
            },
            {
                "label": "Localized",
                "name": "vLocalizedId"
            },
            //{
            //    "label": "Parent",
            //    "name": "vParentId"
            //},
            //{
            //    "label": "Has Heel Height Filter",
            //    "name": "eHasHeelHeightFilter"
            //},
            //{
            //    "label": "Has Color Filter",
            //    "name": "eHasColorFilter"
            //},
            {
                "label": "Data From",
                "name": "eDataFrom"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#productcategories').dataTable( {
        "sAjaxSource": base_url+"product_categories/all_product_categories_listing",
    "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "id","bSortable": false},
          //{ "mData": "vProductCatId" },
            { "mData": "vName"},
            { "mData": "vShortName"},
            { "mData": "vLocalizedId"},
            //{ "mData": "vParentId"},
            //{ "mData": "eHasHeelHeightFilter" },
            //{ "mData": "eHasColorFilter"},
            { "mData": "eDataFrom"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );