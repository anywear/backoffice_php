
function returnme()
{
    window.location.href = base_url+'home';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    var _iCityId = $('#iCityId').val();
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"home/all_store_listing?iCityId="+_iCityId,
        "domTable": "#storetable",
        "fields": [ {
                "label": "Store ID:",
                "name": "iStoreId",
                "type": "checkbox"
            },
            {
                "label": "Favouites:",
                "name": "eFav"
            },
            {
                "label": "Store Name:",
                "name": "vStoreName"
            },
            {
                "label": "Date:",
                "name": "dCurrentdate"
            },{
                "label": "Edit:",
                "name": "editlink"
            }
        ]
    } );

    
 
    $('#citytable').dataTable( {
        "sAjaxSource": base_url+"home/all_store_listing?iCityId="+_iCityId,
    "aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iStoreId","bSortable": false},
            { "mData": "eFav" },
            { "mData": "vStoreName" },
            { "mData": "dCurrentdate"},
            { "mData": "editlink","bSortable": false }
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );

    
});


