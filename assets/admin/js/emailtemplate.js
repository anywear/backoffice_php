$(document).ready(function(){
    $("#btn-save").click(function() {
        if($("#vEmailTitle").val() ==''){
            $(".modal-body").html( "<p>Please Enter Email Title!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#vEmailCode").val() ==''){
            $(".modal-body").html( "<p>Please Enter Email Code!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#vFromName").val() ==''){
            $(".modal-body").html( "<p>Please Enter From Name!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        
	   if($("#vFromEmail").val() ==''){
            $(".modal-body").html( "<p>Please Enter From Email!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if(IsEmail($( "#vFromEmail" ).val())==false){
            $(".modal-body").html( "<p>Please Enter Proper Email Address!</p>" );
            $("#myalert").modal('show');
            return false;
        }
	   if($("#vEmailSubject").val() ==''){
            $(".modal-body").html( "<p>Please Enter Email Subject!</p>" );
            $("#myalert").modal('show');
            return false;
        }

        /*if($("#tEmailMessage").val() ==''){
            $(".modal-body").html( "<p>Please Enter Email Message!</p>" );
            $("#myalert").modal('show');
            return false;
        }*/
        
	   if($("#vEmailFooter").val() ==''){
            $(".modal-body").html( "<p>Please Enter Email Footer!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#eSendType").val() =='')
        {
            $(".modal-body").html( "<p>Please Select Send Type!</p>" );
            $("#myalert").modal('show');
            return false;
        }
        if($("#eStatus").val() =='')
        {
            $(".modal-body").html( "<p>Please Select Status!</p>" );
            $("#myalert").modal('show');
            return false;
        }
    });
});

function returnme()
{
	window.location.href = base_url+'emailtemplate';
}


var editor; // use a global for the submit and return data rendering in the examples
 
$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"emailtemplate/get_emailtemplate_listing",
        "domTable": "#emailtemplatelistId",
        "fields": [ {
                "label": "Country ID:",
                "name": "id",
                "type": "checkbox"
            }, {
                "label": "Email Title:",
                "name": "emailtitle"
            },{
                "label": "From Email:",
                "name": "fromemail"
            }, {
                "label": "Email Code:",
                "name": "emailcode"
            },{
                "label": "Status:",
                "name": "status"
            },{
                "label": "Edit:",
                "name": "editlink"
            }
        ]
    } );
 
    $('#emailtemplatelistId').dataTable( {
        "sAjaxSource": base_url+"emailtemplate/get_emailtemplate_listing",
	"aaSorting": [[5,'desc']],
        "aoColumns": [
            { "mData": "id","bSortable": false},
            { "mData": "emailtitle" },
            { "mData": "fromemail" },
            { "mData": "emailcode" },
            { "mData": "status"},
            { "mData": "editlink","bSortable": false }
            
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );
