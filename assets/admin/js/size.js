
function returnme()
{
	window.location.href = base_url+'size';
}

 // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    var editor;
    editor = new $.fn.dataTable.Editor( {
        "ajaxUrl": base_url+"size/all_size_listing",
        "domTable": "#sizetable",
        "fields": [ {
                "label": "Item ID:",
                "name": "iSizeId",
                "type": "checkbox"
            },
            {
                "label": "Size",
                "name": "vSizeTitle"
            },
            {
                "label": "SizeType",
                "name": "eSizeType"
            },
            {
                "label": "Sorting",
                "name": "iSorting"
            },
            {
                "label": "Status",
                "name": "eStatus"
            },
            {
                "label": "Control",
                "name": "editlink"
            }
        ]
    } );
 
    $('#sizetable').dataTable( {
        "sAjaxSource": base_url+"size/all_size_listing",
	"aaSorting": [[0,'desc']],
        "aoColumns": [
            { "mData": "iSizeId","bSortable": false},
            { "mData": "vSizeTitle" },
            { "mData": "eSizeType" },
            { "mData": "iSorting"},
            { "mData": "eStatus"},
            { "mData": "editlink","bSortable": false}
        ],
        "oTableTools": {
            "sRowSelect": "multi",
            "aButtons": [
                { "sExtends": "editor_create", "editor": editor },
                { "sExtends": "editor_edit",   "editor": editor },
                { "sExtends": "editor_remove", "editor": editor }
            ]
        }
    } );
} );