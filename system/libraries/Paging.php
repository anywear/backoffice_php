<?php
class Paging
{
	var  $page_limit;	//set page limit 
	var  $rec_limit;	//set rec limit per page
	var  $totrec;		//tot record for paging
	var  $totpages;		//total pages to display
	var  $start;
	var  $page_string;
	var  $var_limit;


	/**
	 *@ intiliaze paging class & vars
	 *@ no return values
	 */

	function __construct($tot_rec,$start,$functoCall='',$iCampaignCategoryId,$rec_limit)
	{ 
        //intiailaze variables here & methods
		$this->setStart($start);				//set start
		$this->setTotalRecord($tot_rec);		//set total record
		$this->setRecordLimit($rec_limit);				//set record limit
		$this->setPageLimit();					//set total page limit
		$this->setTotalPages($tot_rec);			//set total pages
		$this->JSFunc = $functoCall;
		$this->iCampaignCategoryId = $iCampaignCategoryId;
	}

	/**
	 *@ private set start from
	 *@ return start value
	 */

	private function setStart($start){
		if($start == "0")
			return $this->start = 0;
		else 
			return $this->start = $start;
	}

	/**
	 *@ private set total record
	 *@ return total record
	 */

	private function setTotalRecord($tot_rec){
		if($tot_rec == "")
			return $this->totRec = 0;
		else 
			return $this->totRec = $tot_rec;
	}

	/**
	 *@ private set total record limit per page
	 *@ return total record limit per page
	 */

	private function setRecordLimit($rec_limit){
		global 	$FRONT_REC_LIMIT_ALL;                 
		return $this->rec_limit = $rec_limit;
	}

	/**
	 *@ private set totapages of listing
	 *@ return total pages
	 */

	private function setTotalPages($tot_rec){
	    
		return $this->totPages = ceil($this->totRec/$this->rec_limit);
	}

	/**
	 *@ private set pagelimit 
	 *@ return page limit
	 */

	private function setPageLimit(){
		global $FPLIMIT;
        $FPLIMIT = ($FPLIMIT !='')?$FPLIMIT:5;
		return $this->page_limit = $FPLIMIT;
	}


	/**
	 *@ public display paging 
	 *@ return paging string
	 */

	public function displayPaging()
	{
        global $smarty;
		$page_string = "";
		$page_limit = $this->page_limit;
		$tot_pages = $this->totPages;
        
        $loop_limit = (($page_limit > $tot_pages) ? $tot_pages : $page_limit) ;
        $start_loop = floor($this->start/$page_limit);
      	
		if($start_loop != ($this->start/$page_limit))
			$start_loop = $start_loop * $page_limit+1;
		else
			$start_loop = ($start_loop-1) * $page_limit+1;
        
		$page_string.="<div class='pagination page_msg'>";
		$page_string.="<ul>";          

		if($start_loop > $page_limit)
		{
            $prev_loop = $start_loop - 1;
			$page_string.="<li><a onclick='javascript:".$this->JSFunc."(\"".$this->iCampaignCategoryId."\",\"".$prev_loop."\");' title='Previous'>Previous</a>&nbsp;</li>";
	    }
        
		for($loop=1 ; $loop<=$loop_limit ; $loop++)
		{	
			if($start_loop > $tot_pages) break;
			if($start_loop == $this->start)
				$clas = "active";
			else
				$clas = "bott-link-btn";
				
			$page_string.="<li class='$clas'><a onclick='javascript:".$this->JSFunc."(\"".$this->iCampaignCategoryId."\",\"".$start_loop."\");' title=\"".$start_loop."\"  id=pageing".$start_loop." class='$clas' value=\"".$start_loop."\">$start_loop</a>&nbsp;</li>";
            $start_loop++;	
		}
      
		if($start_loop<=$tot_pages)
			$page_string.="<li ><a  onclick='javascript:".$this->JSFunc."(\"".$this->iCampaignCategoryId."\",\"".$start_loop."\");'; title='Next'>Next</a></li>";
			$page_string.="</ul>";
		    $page_string.="</div>";
        
        return $page_string;
	}

	/**
	 *@ public display paging message
	 *@ return paging message
	 */
	public function setMessage($msg)
	{
		global $smarty;
		$rec_limit = $this->rec_limit;
		$num_limit = ($this->start-1)*$rec_limit;
		$startrec = $num_limit;
		$lastrec = $startrec + $rec_limit;
		$startrec = $startrec + 1;
		if($lastrec > $this->totRec)
			$lastrec = $this->totRec;
		
		if($this->totRec > 0 ){
			return $recmsg = 'Showing'." ".$startrec." - ".$lastrec." ".strtolower($msg)." of ".$this->totRec;
		}else{
			return $recmsg= 'No'." ".strtolower($msg)." Found.";
		}
	}
}
?>
